package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.SandwichSelection;
import com.cs.bakeryco.model.Items;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class SandwichBoxItemsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Items> orderList = new ArrayList<>();
    String language;
    private DataBaseHelper myDbHelper;
    Activity parentActivity;
    int boxSize, pos = 0;
    ProgressDialog dialog;

    public SandwichBoxItemsAdapter(Context context, ArrayList<Items> groups, int boxSize, String language, Activity parentActivity) {
        this.context = context;
        this.orderList = groups;
        this.boxSize = boxSize;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }

    public int getCount() {
        return SandwichSelection.boxSize;
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView itemImage, sideLine, bottomLine;
        TextView gridnumber;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.pastries_box_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.itemImage = (ImageView) convertView.findViewById(R.id.item_image);
            holder.sideLine = (ImageView) convertView.findViewById(R.id.side_line);
            holder.bottomLine = (ImageView) convertView.findViewById(R.id.bottom_line);
            holder.gridnumber = (TextView) convertView.findViewById(R.id.grid_number);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        int pos = SandwichSelection.boxSize/2;
        if(position == (pos-1) || (position+1) == SandwichSelection.boxSize){
            holder.sideLine.setVisibility(View.INVISIBLE);
        }
        else{
            holder.sideLine.setVisibility(View.VISIBLE);
        }
        int pos1 = SandwichSelection.boxSize/2;
        if(position>=pos1){
            holder.bottomLine.setVisibility(View.INVISIBLE);
        }
        else{
            holder.bottomLine.setVisibility(View.VISIBLE);
        }
        try {
            if(orderList.size()>=position){
                Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getImages()).placeholder(R.drawable.ic_launcher).into(holder.itemImage);
                holder.itemImage.setVisibility(View.VISIBLE);
                holder.gridnumber.setVisibility(View.GONE);
            }
            else{
                holder.itemImage.setVisibility(View.GONE);
                holder.gridnumber.setVisibility(View.VISIBLE);
                pos = position + 1;
                holder.gridnumber.setText(""+pos);
            }
        } catch (Exception e) {
            e.printStackTrace();
            holder.itemImage.setVisibility(View.GONE);
            holder.gridnumber.setVisibility(View.VISIBLE);
            pos = position + 1;
            holder.gridnumber.setText(""+pos);
        }
//        try {
////            dialog = ProgressDialog.show(parentActivity, "", "");
////            Glide.with(context)
////                    .load(Constants.IMAGE_URL+orderList.get(position).getImages())
////                    .listener(new RequestListener<String, GlideDrawable>() {
////                        @Override
////                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
////                            try {
////                                dialog.dismiss();
////                            } catch (Exception e1) {
////                                e1.printStackTrace();
////                            }
////                            return true;
////                        }
////
////                        @Override
////                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
////                            try {
////                                dialog.dismiss();
////                            } catch (Exception e) {
////                                e.printStackTrace();
////                            }
////                            return true;
////                        }
////                    })
////                    .into(holder.itemImage);
//            Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getImages()).into(holder.itemImage);
//        } catch (Exception e) {
//            e.printStackTrace();
//            holder.gridnumber.setText(""+position);
//        }

        return convertView;
    }

}
