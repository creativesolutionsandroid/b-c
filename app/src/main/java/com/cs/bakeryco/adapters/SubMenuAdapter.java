package com.cs.bakeryco.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.model.MainCategories;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class SubMenuAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<MainCategories> orderList = new ArrayList<>();
    String language;
    int value;

    public SubMenuAdapter(Context context, ArrayList<MainCategories> groups, int value, String language) {
        this.context = context;
        this.orderList = groups;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.value = value;
    }


    public int getCount() {
        return orderList.get(value).getSubCategories().size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title;
        ImageView itemImage;
        LinearLayout grid_layout;
//        ArrayList<String> list = new ArrayList<>();
//        ArrayList<String> left_list = new ArrayList<>();
//        ArrayList<String> right_list = new ArrayList<>();
        //ImageButton plus;
//        LinearLayout additionalsLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.subcat_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.item_image);
            holder.grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(orderList.get(value).getSubCategories().get(position).getSubCat());
            Log.e("TAG","title" + orderList.get(value).getSubCategories().get(position).getSubCat());
        }else if(language.equalsIgnoreCase("Ar")){
            holder.title.setText(orderList.get(value).getSubCategories().get(position).getSubCatAr());
        }

        Glide.with(context).load(Constants.IMAGE_URL+orderList.get(value).getSubCategories().get(position).getImages()).into(holder.itemImage);

        try {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            PastriesSelection.orderPrice.setText("0.00 SR");
            PastriesSelection.orderQuantity.setText("0");
            PastriesSelection.mcount_basket.setText("0");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

}
