package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.model.SubItems;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class PastriesBoxItemsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<SubItems> orderList = new ArrayList<>();
    String language;
    private DataBaseHelper myDbHelper;
    Activity parentActivity;
    int boxSize, pos, pos1;
    ProgressDialog dialog;
    int value;
    int value1;

    public PastriesBoxItemsAdapter(Context context, ArrayList<SubItems> groups, int value, int value1, int boxSize, String language, Activity parentActivity) {
        this.context = context;
        this.orderList = groups;
        this.value = value;
        this.value1 = value1;
        this.boxSize = boxSize;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }

    public int getCount() {
//        Log.i("TAG","boxSize "+PastriesSelection.boxSize);
//        return orderList.get(value).getChildItems().get(value1).getSubItems().size();
        return PastriesSelection.boxSize;
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView itemImage, sideLine, bottomLine;
        TextView gridnumber;
        RelativeLayout itemLayout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.pastries_box_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.itemImage = (ImageView) convertView.findViewById(R.id.item_image);
            holder.sideLine = (ImageView) convertView.findViewById(R.id.side_line);
            holder.bottomLine = (ImageView) convertView.findViewById(R.id.bottom_line);
            holder.gridnumber = (TextView) convertView.findViewById(R.id.grid_number);
            holder.itemLayout = (RelativeLayout) convertView.findViewById(R.id.itemLayout);
//            convertView.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 10));
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        pos = boxSize/2;

        if(position == (pos-1) || (position+1) == boxSize){
            holder.sideLine.setVisibility(View.INVISIBLE);
        }
        else{
            holder.sideLine.setVisibility(View.VISIBLE);
        }

        if(PastriesSelection.rowSize == 1){
            final float scale = context.getResources().getDisplayMetrics().density;
            int px = (int) (80 * scale + 0.5f);
            holder.itemLayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, px));
        }
        else if(PastriesSelection.rowSize == 2){
            final float scale = context.getResources().getDisplayMetrics().density;
            int px = (int) (40 * scale + 0.5f);
            holder.itemLayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, px));
        }
        else if(PastriesSelection.rowSize == 3){
            final float scale = context.getResources().getDisplayMetrics().density;
            int px = (int) (28 * scale + 0.5f);
            holder.itemLayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, px));
        }
        else if(PastriesSelection.rowSize == 4){
            final float scale = context.getResources().getDisplayMetrics().density;
            int px = (int) (20 * scale + 0.5f);
            holder.itemLayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, px));
        }
        else if(PastriesSelection.rowSize == 5){
            final float scale = context.getResources().getDisplayMetrics().density;
            int px = (int) (16 * scale + 0.5f);
            holder.itemLayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, px));
        }
        else{
            final float scale = context.getResources().getDisplayMetrics().density;
            int px = (int) (15 * scale + 0.5f);
            holder.itemLayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, px));
        }

        try {
            if(orderList.size()>=position){
                Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getSubItemimage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_launcher).into(holder.itemImage);
                holder.itemImage.setVisibility(View.VISIBLE);
                holder.gridnumber.setVisibility(View.GONE);
            }
            else{
                holder.itemImage.setVisibility(View.GONE);
                holder.gridnumber.setVisibility(View.VISIBLE);
                pos = position + 1;
                holder.gridnumber.setText(""+pos);
            }
        } catch (Exception e) {
            e.printStackTrace();
            holder.itemImage.setVisibility(View.GONE);
            holder.gridnumber.setVisibility(View.VISIBLE);
            pos = position + 1;
            holder.gridnumber.setText(""+pos);
        }
//        try {
////            dialog = ProgressDialog.show(parentActivity, "", "");
////            Glide.with(context)
////                    .load(Constants.IMAGE_URL+orderList.get(position).getImages())
////                    .listener(new RequestListener<String, GlideDrawable>() {
////                        @Override
////                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
////                            try {
////                                dialog.dismiss();
////                            } catch (Exception e1) {
////                                e1.printStackTrace();
////                            }
////                            return true;
////                        }
////
////                        @Override
////                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
////                            try {
////                                dialog.dismiss();
////                            } catch (Exception e) {
////                                e.printStackTrace();
////                            }
////                            return true;
////                        }
////                    })
////                    .into(holder.itemImage);
////            Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getImages()).into(holder.itemImage);
//        } catch (Exception e) {
//            e.printStackTrace();
//            holder.gridnumber.setText(""+position);
//        }

        return convertView;
    }

}
