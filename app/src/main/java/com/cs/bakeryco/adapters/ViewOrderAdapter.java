package com.cs.bakeryco.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.model.ViewOrder;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 7/10/2017.
 */

public class ViewOrderAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<ViewOrder> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty, price, pos1;
    String language;
    Float priceTxt= Float.valueOf(0);

    public ViewOrderAdapter(Context context, ArrayList<ViewOrder> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title, price, qty, qty1, itemDesc;
        ImageView itemType, plusBtn, minusBtn, itemComment, itemIcon;
        //ImageButton plus;
        LinearLayout additionalsLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.view_row, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.view_row_arabic, null);
            }

            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.price = (TextView) convertView.findViewById(R.id.item_price);
            holder.qty = (TextView) convertView.findViewById(R.id.item_desc);
//            holder.qty1 = (TextView) convertView.findViewById(R.id.item_qty1);
//            holder.plusBtn = (ImageView) convertView.findViewById(R.id.plus_btn);
//            holder.minusBtn = (ImageView) convertView.findViewById(R.id.minus_btn);
//
//            holder.itemComment = (ImageView) convertView.findViewById(R.id.edit_comment);
//            holder.itemType = (ImageView) convertView.findViewById(R.id.item_type);
//            holder.itemDesc = (TextView) convertView.findViewById(R.id.item_desc);
//            holder.additionalsLayout = (LinearLayout) convertView.findViewById(R.id.additionals_layout);
            holder.itemIcon = (ImageView) convertView.findViewById(R.id.item_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if(orderList.get(position).getAdditionalName().equals("")){
//            holder.additionalsName.setVisibility(View.GONE);
//        }else {
//            holder.additionalsName.setVisibility(View.VISIBLE);
//            holder.additionalsName.setText(orderList.get(position).getAdditionalName());
//        }
//        holder.itemType.setVisibility(View.INVISIBLE);
//        holder.plusBtn.setVisibility(View.INVISIBLE);
//        holder.minusBtn.setVisibility(View.INVISIBLE);
//        holder.qty1.setVisibility(View.INVISIBLE);

//        if(language.equalsIgnoreCase("En")){
//            String[] parts = orderList.get(position).getAdditionalsStr().split(",");
//            String[] parts1 = orderList.get(position).getAdditionalsPrice().split(",");
//
////            holder.additionalsLayout.removeAllViews();
//            for(int i=0; i< parts.length;i++){
//
//                if(!parts[i].equalsIgnoreCase("")) {
//                    View v = inflater.inflate(R.layout.checkout_additionals, null);
//                    TextView addtionalsName = (TextView) v.findViewById(R.id.additionals_name);
//                    TextView addtionalsPrice = (TextView) v.findViewById(R.id.additional_price);
//                    addtionalsName.setText(parts[i]);
//                    int priceTxt = Integer.parseInt(parts1[i]) * Integer.parseInt(orderList.get(position).getQuantity());
//                    addtionalsPrice.setText("" + priceTxt);
//                    holder.additionalsLayout.addView(v);
//                }
//            }
//        }else if(language.equalsIgnoreCase("Ar")){
//            String[] parts = orderList.get(position).getAdditionalsStrAr().split(",");
//            String[] parts1 = orderList.get(position).getAdditionalsPrice().split(",");
//
//            holder.additionalsLayout.removeAllViews();
//            for(int i=0; i< parts.length;i++){
//                if(!parts[i].equalsIgnoreCase("")) {
//                    View v = inflater.inflate(R.layout.checkout_additionals_arabic, null);
//                    TextView addtionalsName = (TextView) v.findViewById(R.id.additionals_name);
//                    TextView addtionalsPrice = (TextView) v.findViewById(R.id.additional_price);
//                    addtionalsName.setText(parts[i]);
//                    int priceTxt = Integer.parseInt(parts1[i]) * Integer.parseInt(orderList.get(position).getQuantity());
//                    addtionalsPrice.setText("" + priceTxt);
//                    holder.additionalsLayout.addView(v);
//                }
//            }
//        }



//        if(orderList.get(position).getItemType().equalsIgnoreCase("1")){
//            holder.itemComment.setImageResource(R.drawable.small_item);
//        }else if(orderList.get(position).getItemType().equalsIgnoreCase("2")){
//            holder.itemComment.setImageResource(R.drawable.medium_item);
//        }else if(orderList.get(position).getItemType().equalsIgnoreCase("3")){
//            holder.itemComment.setImageResource(R.drawable.large_item);
//        }else if(orderList.get(position).getItemType().equalsIgnoreCase("4")){
//            holder.itemComment.setImageResource(R.drawable.extra_large_item);
//        }else if(orderList.get(position).getItemType().equalsIgnoreCase("5")){
//            holder.itemComment.setImageResource(R.drawable.pcs4_item);
//        }else if(orderList.get(position).getItemType().equalsIgnoreCase("6")){
//            holder.itemComment.setImageResource(R.drawable.pcs8_item);
//        }

        if(language.equalsIgnoreCase("En")){
//            holder.itemDesc.setText(orderList.get(position).getItemDesc());
            holder.title.setText(orderList.get(position).getItemName());
        }else if(language.equalsIgnoreCase("Ar")){
//            holder.itemDesc.setText(orderList.get(position).getItemDescAr());
            holder.title.setText(orderList.get(position).getItemNameAr());
        }

        Log.e("TAG","QTY " +orderList.get(position).getQuantity());

        holder.qty.setText(orderList.get(position).getQuantity());
//        holder.qty1.setText(orderList.get(position).getQuantity());
        double number,number1;
        priceTxt = Float.parseFloat(orderList.get(position).getPrice()) * Integer.parseInt(orderList.get(position).getQuantity());
        number=priceTxt;
        number1= Double.parseDouble(orderList.get(position).getPrice());
        DecimalFormat decim = new DecimalFormat("0.00");
        holder.price.setText("" +decim.format(number1));
        Log.e("TAG","Image "+orderList.get(position).getItemImage());
        Glide.with(context).load("http://csadms.com/bncservices/images/" + orderList.get(position).getItemImage()).into(holder.itemIcon);


		/*ArrayList<HashMap<String, String>> List = SchoolAlertMain.DBcontroller.getAllAlerts();
		//System.out.println("adapter"+alertsArrayList.get(position).getTime());
		if(List.contains(alertsArrayList.get(position).getAlertId())){
			holder.alertList.setBackgroundColor(Color.GRAY);
		}*/

        return convertView;
    }
}