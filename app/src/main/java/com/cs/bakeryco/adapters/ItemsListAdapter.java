//package com.cs.bakeryco.adapters;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.view.animation.Animation;
//import android.widget.BaseExpandableListAdapter;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.cs.bakeryco.Constants;
//import com.cs.bakeryco.DataBaseHelper;
//import com.cs.bakeryco.R;
//import com.cs.bakeryco.activities.CakesInnerActivity;
//import com.cs.bakeryco.activities.CategoriesListActivity;
//import com.cs.bakeryco.activities.CateringActivity;
//import com.cs.bakeryco.activities.CommentsActivity;
//import com.cs.bakeryco.fragments.OrderFragment;
//import com.cs.bakeryco.model.Items;
//import com.cs.bakeryco.model.Order;
//import com.cs.bakeryco.model.SubCategories;
//import com.readystatesoftware.viewbadger.BadgeView;
//
//import org.apache.commons.lang3.text.WordUtils;
//
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//public class ItemsListAdapter extends BaseExpandableListAdapter {
//
//    private Context context;
//    private ArrayList<SubCategories> groups;
////    private ArrayList<Nutrition> nutritionList;
//
//    private DataBaseHelper myDbHelper;
//    BadgeView badge;
//    AlertDialog alertDialog;
//    private int orderQuantity;
//    float orderPrice;
//    String prevId;
//    String language;
//
//
//    public ItemsListAdapter(Context context, ArrayList<SubCategories> groups, String language) {
//        this.context = context;
//        this.groups = groups;
//        this.language=language;
//        myDbHelper = new DataBaseHelper(context);
//
//    }
//
//    @Override
//    public Object getChild(int groupPosition, int childPosition) {
//        ArrayList<Items> chList = groups.get(groupPosition).getChildItems();
//
//        return chList.get(childPosition);
//    }
//
//    @Override
//    public long getChildId(int groupPosition, int childPosition) {
//        return childPosition;
//    }
//
//    @Override
//    public View getChildView(int groupPosition, final int childPosition,
//                             boolean isLastChild, View convertView, ViewGroup parent) {
//
//        final Items child = (Items) getChild(groupPosition, childPosition);
//        LayoutInflater infalInflater = (LayoutInflater) context
//                .getSystemService(context.LAYOUT_INFLATER_SERVICE);
//        if (convertView == null) {
////            LayoutInflater infalInflater = (LayoutInflater) context
////                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
//           if (language.equalsIgnoreCase("En")) {
//               convertView = infalInflater.inflate(R.layout.child_item, null);
//           }else if (language.equalsIgnoreCase("Ar")){
//               convertView = infalInflater.inflate(R.layout.child_item_arabic, null);
//           }
//        }
//        View v1 = null;
//        final ArrayList<Order> orderList = new ArrayList<>();
//        if (language.equalsIgnoreCase("En")) {
//             v1 = infalInflater.inflate(R.layout.product_info_dropdown, null);
//        }else if (language.equalsIgnoreCase("Ar")){
//             v1 = infalInflater.inflate(R.layout.product_info_dropdown_arabic, null);
//        }
//        TextView tv = (TextView) convertView.findViewById(R.id.subcat_name);
//        final TextView childBadgeQty = (TextView) convertView.findViewById(R.id.child_badge_qty);
//        final RelativeLayout infoLayout = (RelativeLayout) v1.findViewById(R.id.info_layout);
//        LinearLayout itemLayout = (LinearLayout) convertView.findViewById(R.id.item_layout);
//        final LinearLayout childLayout = (LinearLayout) convertView.findViewById(R.id.child_layout);
////        final TextView childQty = (TextView) convertView.findViewById(R.id.child_item_qty);
//        final TextView price = (TextView) v1.findViewById(R.id.product_price);
////        final RelativeLayout qty_layout = (RelativeLayout) convertView.findViewById(R.id.qty_layout);
//        final TextView foodPrice = (TextView) convertView.findViewById(R.id.item_price_food);
//        final TextView dummyPrice = (TextView) convertView.findViewById(R.id.dummy_price);
//        TextView mdescrib= (TextView) convertView.findViewById(R.id.describ_name);
//
//        final LinearLayout commentayout = (LinearLayout) v1.findViewById(R.id.comment_layout);
//        ImageView plusBtn = (ImageView) v1.findViewById(R.id.plus_btn);
//        ImageView minusBtn = (ImageView) v1.findViewById(R.id.minus_btn);
//        final TextView count = (TextView) v1.findViewById(R.id.product_count);
//        final ImageView itemIcon = (ImageView) convertView.findViewById(R.id.item_icon);
//        childLayout.removeAllViews();
////        infoLayout.setVisibility(View.GONE);
//
//
//
////        Picasso.with(context).load("http://85.194.94.241/bncservices/images/" + child.getImages()).placeholder(R.drawable.child_image).into(itemIcon);
//        Glide.with(context).load("http://csadms.com/bncservices/images/"+child.getImages()).into(itemIcon);
//        Log.e("","" +child.getImages());
//
//        itemIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
//                LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();
//
//                    View dialogView = inflater.inflate(R.layout.bigimage_popup, null);
//                    dialogBuilder.setView(dialogView);
//                // ...Irrelevant code for customizing the buttons and title
////                if(language.equalsIgnoreCase("En")){
////                    layout = R.layout.bigimage_popup;
////                }else if(language.equalsIgnoreCase("Ar")){
////                    layout = R.layout.bigimage_popup;
////                }
//
//                ImageView img = (ImageView) dialogView.findViewById(R.id.big_image);
//
//                Glide.with(context).load(Constants.IMAGE_URL+child.getImages()).into(img);
//
//
//
//                alertDialog = dialogBuilder.create();
//                alertDialog.show();
//
//                //Grab the window of the dialog, and change the width
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                Window window = alertDialog.getWindow();
//                lp.copyFrom(window.getAttributes());
//                //This makes the dialog take up the full width
//                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                window.setAttributes(lp);
//            }
//        });
//
//        Log.i("Name TAG", "" + child.getItemName());
//        if (language.equalsIgnoreCase("En")) {
//            tv.setText(WordUtils.capitalizeFully(child.getItemName()));
//            mdescrib.setText(WordUtils.capitalizeFully(child.getDescription()));
//        }else if (language.equalsIgnoreCase("Ar")){
//            tv.setText(WordUtils.capitalizeFully(child.getItemNameAr()));
//            mdescrib.setText(WordUtils.capitalizeFully(child.getDescriptionAr()));
//        }
//        if(!child.getCatId().equals("2")) {
//            dummyPrice.setVisibility(View.VISIBLE);
//            foodPrice.setVisibility(View.VISIBLE);
//            if(child.getCatId().equalsIgnoreCase("8")) {
//                dummyPrice.setText( "SR/kg");
//            }else {
//                dummyPrice.setText("SR/pc");
//            }
//            foodPrice.setText("" + Float.parseFloat(child.getPriceList().get(0).getPrice()));
//        }else {
//            foodPrice.setVisibility(View.GONE);
//            dummyPrice.setVisibility(View.INVISIBLE);
//        }
//        final View finalV = v1;
//        itemLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                prevId = child.getItemId();
////                orderList.clear();
////                orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
//                if (child.getCatId().equals("2")) {
//
//                    Intent i = new Intent(context, CakesInnerActivity.class);
//                    i.putExtra("item_name", child.getItemName());
//                    i.putExtra("catId", child.getCatId());
//                    i.putExtra("subCatId", child.getSubCatId());
//                    i.putExtra("itemId", child.getItemId());
//                    i.putExtra("item_nameAr", child.getItemNameAr());
//                    i.putExtra("item_descAr",child.getDescriptionAr());
//                    i.putExtra("item_desc", child.getDescription());
//                    i.putExtra("price_list", child.getPriceList());
//                    i.putExtra("itemImage",child.getImages());
//                    context.startActivity(i);
//
//                } else {
//                        int getSavedCount = myDbHelper.getItemOrderCount(child.getItemId());
//                        float getSavedPrice = Float.parseFloat(child.getPriceList().get(0).getPrice()) * getSavedCount;
//                        Log.i("TAG", "saved count" + getSavedCount);
//
//                        if (getSavedCount == 0) {
//
////                            if(child.getCatId().equalsIgnoreCase("2")){
////                                price.setText(Float.parseFloat(child.getPriceList().get(0).getPrice())+"");
////                            }else
////                            if(child.getCatId().equalsIgnoreCase("8")){
////                                price.setText(Float.parseFloat(child.getPriceList().get(0).getPrice()) + " / kg");
////                            }else {
////                                price.setText(Float.parseFloat(child.getPriceList().get(0).getPrice()) + " / pc");
////                            }
//                            if(child.getCatId().equals("4")){
//                                float macaronPrice = Float.parseFloat(child.getPriceList().get(0).getPrice()) * 5;
//                                float mprice= Float.parseFloat(child.getPriceList().get(0).getPrice());
//                                count.setText("5");
//                                price.setText(macaronPrice+"");
//                                childBadgeQty.setVisibility(View.VISIBLE);
//                                childBadgeQty.setText("5");
//                                orderQuantity = 5;
//                                orderPrice = Float.parseFloat(child.getPriceList().get(0).getPrice()) * 5;
//                                HashMap<String, String> values = new HashMap<>();
//                                values.put("itemId", child.getItemId());
//                                values.put("itemTypeId", child.getPriceList().get(0).getSize());
//                                values.put("subCategoryId", child.getSubCatId());
//                                values.put("additionals", "");
//                                values.put("qty", "5");
//                                values.put("price", String.valueOf(mprice));
//                                values.put("additionalsPrice", "");
//                                values.put("additionalsTypeId", "");
//                                values.put("totalAmount", Float.toString(macaronPrice));
//                                values.put("comment", "");
//                                values.put("status", "1");
//                                values.put("creationDate", "14/07/2015");
//                                values.put("modifiedDate", "14/07/2015");
//                                values.put("categoryId", child.getCatId());
//                                values.put("itemName", child.getItemName());
//                                values.put("itemNameAr", child.getItemNameAr());
//                                values.put("image",child.getImages());
//                                Log.e("TAG","price items1"+mprice);
//                                Log.e("TAG","Qty items"+5);
//                                myDbHelper.insertOrder(values);
//                            }else {
//                                count.setText("1");
//                                price.setText(Float.parseFloat(child.getPriceList().get(0).getPrice()) + "");
//                                childBadgeQty.setVisibility(View.VISIBLE);
//                                childBadgeQty.setText("1");
//                                orderQuantity = 1;
//                                orderPrice = Float.parseFloat(child.getPriceList().get(0).getPrice());
//                                HashMap<String, String> values = new HashMap<>();
//                                values.put("itemId", child.getItemId());
//                                values.put("itemTypeId", child.getPriceList().get(0).getSize());
//                                values.put("subCategoryId", child.getSubCatId());
//                                values.put("additionals", "");
//                                values.put("qty", "1");
//                                values.put("price", child.getPriceList().get(0).getPrice());
//                                values.put("additionalsPrice", "");
//                                values.put("additionalsTypeId", "");
//                                values.put("totalAmount", child.getPriceList().get(0).getPrice());
//                                values.put("comment", "");
//                                values.put("status", "1");
//                                values.put("creationDate", "14/07/2015");
//                                values.put("modifiedDate", "14/07/2015");
//                                values.put("categoryId", child.getCatId());
//                                values.put("itemName", child.getItemName());
//                                values.put("itemNameAr", child.getItemNameAr());
//                                values.put("image",child.getImages());
//                                Log.e("TAG","price items2"+child.getPriceList().get(0).getPrice());
//                                Log.e("TAG","Qty items2"+1);
//                                myDbHelper.insertOrder(values);
//                            }
//                            try {
//                                double number;
//                                number=myDbHelper.getTotalOrderPrice();
//                                DecimalFormat decim = new DecimalFormat("0.00");
//                                CategoriesListActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                                CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//
//                            try {
//                                double number;
//                                number=myDbHelper.getTotalOrderPrice();
//                                DecimalFormat decim = new DecimalFormat("0.00");
//                                CateringActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                                CateringActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                double number;
//                                number=myDbHelper.getTotalOrderPrice();
//                                DecimalFormat decim = new DecimalFormat("0.00");
//                                OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
//                                OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//
//
////                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Material_Light_Dialog));
////
//////                            if(language.equalsIgnoreCase("En")) {
////                                // set title
////                                alertDialogBuilder.setTitle("BAKERY & Co.");
////
////                                // set dialog message
////                                alertDialogBuilder
////                                        .setMessage("You must add at least 6 macarons to proceed to checkout.\n Do you want to add 6 items?")
////                                        .setCancelable(false)
////                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
////                                            public void onClick(DialogInterface dialog, int id) {
////                                                dialog.dismiss();
////                                            }
////                                        })
////                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
////                                    @Override
////                                    public void onClick(DialogInterface dialog, int which) {
////                                        dialog.dismiss();
////                                    }
////                                });
//////                            }else if(language.equalsIgnoreCase("Ar")){
//////                                // set title
//////                                alertDialogBuilder.setTitle("اوريجانو");
//////
//////                                // set dialog message
//////                                alertDialogBuilder
//////                                        .setMessage(result)
//////                                        .setCancelable(false)
//////                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//////                                            public void onClick(DialogInterface dialog, int id) {
//////                                                dialog.dismiss();
//////                                            }
//////                                        });
//////                            }
////
////
////                            // create alert dialog
////                            AlertDialog alertDialog = alertDialogBuilder.create();
////
////                            // show it
////                            alertDialog.show();
//
//                        } else {
//                            orderQuantity = getSavedCount;
//                            orderPrice = Float.parseFloat(child.getPriceList().get(0).getPrice()) * orderQuantity;
//                            count.setText("" + getSavedCount);
////                            if(child.getCatId().equalsIgnoreCase("2")){
//                                price.setText("" + getSavedPrice + " SR");
////                            }else
////                            if(child.getCatId().equalsIgnoreCase("8")){
////                                price.setText("" + getSavedPrice + " / kg");
////                            }else {
////                                price.setText("" + getSavedPrice + " / pc");
////                            }
//
//                            try {
//                                double number;
//                                number=myDbHelper.getTotalOrderPrice();
//                                DecimalFormat decim = new DecimalFormat("0.00");
//                                CategoriesListActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                                CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//
//                            try {
//                                double number;
//                                number=myDbHelper.getTotalOrderPrice();
//                                DecimalFormat decim = new DecimalFormat("0.00");
//                                CateringActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                                CateringActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//
//                            try {
//                                double number;
//                                number=myDbHelper.getTotalOrderPrice();
//                                DecimalFormat decim = new DecimalFormat("0.00");
//                                OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
//                                OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        childLayout.removeAllViews();
//                        childLayout.addView(finalV);
//
//                }
//
//
////                notifyDataSetChanged();
//            }
//        });
//
//        plusBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                prevId = child.getItemId();
////                orderList.clear();
////                orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
//                if( myDbHelper.getItemOrderCount(child.getItemId()) >0) {
//                    if(child.getCatId().equals("4")){
//                        orderQuantity = orderQuantity + 5;
//                        orderPrice = orderPrice + (Float.parseFloat(child.getPriceList().get(0).getPrice())*5);
//                    }else {
//                        orderQuantity = orderQuantity + 1;
//                        orderPrice = orderPrice + Float.parseFloat(child.getPriceList().get(0).getPrice());
//
//                    }
//                    count.setText("" + orderQuantity);
////                    if(child.getCatId().equalsIgnoreCase("2")){
//                        price.setText(""+ orderPrice);
////                    }else
////                    if(child.getCatId().equalsIgnoreCase("8")){
////                        price.setText("" + orderPrice + " / kg");
////                    }else {
////                        price.setText("" + orderPrice + " / pc");
////                    }
//                    myDbHelper.updateOrder(String.valueOf(orderQuantity), String.valueOf(orderPrice), child.getItemId(), child.getPriceList().get(0).getSize());
//                    try {
//                        double number;
//                        number=myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim = new DecimalFormat("0.00");
//                        CateringActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                        CateringActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    try {
//                        double number;
//                        number=myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim = new DecimalFormat("0.00");
//                        CategoriesListActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                        CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    try {
//                        double number;
//                        number=myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim = new DecimalFormat("0.00");
//                        OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
//                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    int cnt = myDbHelper.getItemOrderCount(child.getItemId());
//                    if (cnt != 0) {
//                        childBadgeQty.setVisibility(View.VISIBLE);
//                        childBadgeQty.setText("" + cnt);
//                    } else {
//                        childBadgeQty.setVisibility(View.GONE);
//                    }
//                }else{
//
//                    if(child.getCatId().equals("4")){
//                        float macaronPrice = Float.parseFloat(child.getPriceList().get(0).getPrice()) * 5;
//                        count.setText("5");
//                        price.setText(macaronPrice+"");
//                        childBadgeQty.setVisibility(View.VISIBLE);
//                        childBadgeQty.setText("5");
//                        orderQuantity = 5;
//                        orderPrice = Float.parseFloat(child.getPriceList().get(0).getPrice()) * 5;
//                        HashMap<String, String> values = new HashMap<>();
//                        values.put("itemId", child.getItemId());
//                        values.put("itemTypeId", child.getPriceList().get(0).getSize());
//                        values.put("subCategoryId", child.getSubCatId());
//                        values.put("additionals", "");
//                        values.put("qty", "5");
//                        values.put("price", child.getPriceList().get(0).getPrice());
//                        values.put("additionalsPrice", "");
//                        values.put("additionalsTypeId", "");
//                        values.put("totalAmount", Float.toString(macaronPrice));
//                        values.put("comment", "");
//                        values.put("status", "1");
//                        values.put("creationDate", "14/07/2015");
//                        values.put("modifiedDate", "14/07/2015");
//                        values.put("categoryId", child.getCatId());
//                        values.put("itemName", child.getItemName());
//                        values.put("itemNameAr", child.getItemNameAr());
//                        values.put("image",child.getImages());
//                        Log.e("TAG","price items3"+child.getPriceList().get(0).getPrice());
//                        Log.e("TAG","Qty items3"+5);
//                        myDbHelper.insertOrder(values);
//                    }else {
//
//                        count.setText("1");
//                        childBadgeQty.setVisibility(View.VISIBLE);
//                        childBadgeQty.setText("1");
//                        orderQuantity = 1;
//                        orderPrice = Float.parseFloat(child.getPriceList().get(0).getPrice());
//
////                    if(child.getCatId().equalsIgnoreCase("2")){
//                        price.setText("" + orderPrice + " SR");
////                    }else
////                    if(child.getCatId().equalsIgnoreCase("8")){
////                        price.setText("" + orderPrice + " / kg");
////                    }else {
////                        price.setText("" + orderPrice + " / pc");
////                    }
//                        HashMap<String, String> values = new HashMap<>();
//                        values.put("itemId", child.getItemId());
//                        values.put("itemTypeId", child.getPriceList().get(0).getSize());
//                        values.put("subCategoryId", child.getSubCatId());
//                        values.put("additionals", "");
//                        values.put("qty", "1");
//                        values.put("price", child.getPriceList().get(0).getPrice());
//                        values.put("additionalsPrice", "");
//                        values.put("additionalsTypeId", "");
//                        values.put("totalAmount", child.getPriceList().get(0).getPrice());
//                        values.put("comment", "");
//                        values.put("status", "1");
//                        values.put("creationDate", "14/07/2015");
//                        values.put("modifiedDate", "14/07/2015");
//                        values.put("categoryId", child.getCatId());
//                        values.put("itemName", child.getItemName());
//                        values.put("itemNameAr", child.getItemNameAr());
//                        values.put("image",child.getImages());
//                        Log.e("TAG","price items4"+child.getPriceList().get(0).getPrice());
//                        Log.e("TAG","Qty items4"+1);
//                        myDbHelper.insertOrder(values);
//                    }
//                    try {
//                        double number;
//                        number=myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim = new DecimalFormat("0.00");
//                        CategoriesListActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                        CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    try {
//                        double number;
//                        number=myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim = new DecimalFormat("0.00");
//                        CateringActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                        CateringActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    try {
//                        double number;
//                        number=myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim = new DecimalFormat("0.00");
//                        OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
//                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
////                    ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
//
//                }
////                notifyDataSetChanged();
//            }
//        });
//
//        minusBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                prevId = child.getItemId();
//                if(orderQuantity > 0) {
////                    orderList.clear();
////                    orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
//                    if( myDbHelper.getItemOrderCount(child.getItemId()) >0) {
//                        if(child.getCatId().equalsIgnoreCase("4")){
//                            orderQuantity = orderQuantity - 5;
//                            orderPrice = orderPrice - (Float.parseFloat(child.getPriceList().get(0).getPrice())*5);
//                        }else {
//                            orderQuantity = orderQuantity - 1;
//                            orderPrice = orderPrice - Float.parseFloat(child.getPriceList().get(0).getPrice());
//                        }
//
//
////                        if(child.getCatId().equalsIgnoreCase("2")){
//                            price.setText(""+ orderPrice +" SR");
////                        }else
////                        if(child.getCatId().equalsIgnoreCase("8")){
////                            price.setText("" + orderPrice + " / kg");
////                        }else {
////                            price.setText("" + orderPrice + " / pc");
////                        }
//                        count.setText("" + orderQuantity);
//                        myDbHelper.updateOrder(String.valueOf(orderQuantity), String.valueOf(orderPrice), child.getItemId(), child.getPriceList().get(0).getSize());
//
//                        try {
//                            double number;
//                            number=myDbHelper.getTotalOrderPrice();
//                            DecimalFormat decim = new DecimalFormat("0.00");
//                            CategoriesListActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                            CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                        } catch (NullPointerException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            double number;
//                            number=myDbHelper.getTotalOrderPrice();
//                            DecimalFormat decim = new DecimalFormat("0.00");
//                            CateringActivity.orderPrice.setText("" + decim.format(number) + " SR");
//                            CateringActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
////                            CateringActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                        } catch (NullPointerException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            double number;
//                            number=myDbHelper.getTotalOrderPrice();
//                            DecimalFormat decim = new DecimalFormat("0.00");
//                            OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
//                            OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                        } catch (NullPointerException e) {
//                            e.printStackTrace();
//                        }
//                        int cnt = myDbHelper.getItemOrderCount(child.getItemId());
//                        if (cnt != 0) {
//                            childBadgeQty.setVisibility(View.VISIBLE);
//                            childBadgeQty.setText("" + cnt);
//                        } else {
//                            childBadgeQty.setVisibility(View.GONE);
//                        }
//                        if (orderQuantity == 0) {
//                            notifyDataSetChanged();
//                            myDbHelper.deleteItemFromOrder(child.getItemId(), child.getPriceList().get(0).getSize());
//                        }
////                        notifyDataSetChanged();
//                    }
//                }
//            }
//        });
//
//        int cnt = myDbHelper.getItemOrderCount(child.getItemId());
////        childQty.setText(""+cnt);
//        if(cnt !=0){
//            childBadgeQty.setVisibility(View.VISIBLE);
//            childBadgeQty.setText(""+cnt);
//            orderQuantity = cnt;
//        }else {
//            childBadgeQty.setVisibility(View.GONE);
//        }
//
//        commentayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(orderQuantity > 0) {
//                    orderList.clear();
//                    orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
//                    if(orderList.size() >0) {
//
//
//                        Intent intent = new Intent(context, CommentsActivity.class);
//                        if(language.equalsIgnoreCase("En")){
//                            intent.putExtra("title", child.getItemName());
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            intent.putExtra("title", child.getItemNameAr());
//                        }
//
//                        intent.putExtra("itemImage", child.getImage());
//                        intent.putExtra("itemId", child.getItemId());
//                        intent.putExtra("orderId", orderList.get(0).getOrderId());
//                        intent.putExtra("comment", orderList.get(0).getComment());
//                        intent.putExtra("size", orderList.get(0).getItemTypeId());
//                        intent.putExtra("screen", "food");
//                        context.startActivity(intent);
//
//                    }
//                }else{
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//
//                    // set title
//                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("Please add quantity")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                }
//            }
//        });
//
//
////        if(child.getItemId().equals(prevId)){
////            childLayout.removeAllViews();
////            childLayout.addView(v1);
////            orderList.clear();
////            orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
////            nutritionList = myDbHelper.getNutritionInfo(child.getCatId(), child.getItemId());
////        }
//
//        return convertView;
//    }
//
//    @Override
//    public int getChildrenCount(int groupPosition) {
//        ArrayList<Items> chList = groups.get(groupPosition).getChildItems();
//        return chList.size();
//    }
//
//    @Override
//    public Object getGroup(int groupPosition) {
//        return groups.get(groupPosition);
//    }
//
//    @Override
//    public int getGroupCount() {
//        return groups.size();
//    }
//
//    @Override
//    public long getGroupId(int groupPosition) {
//        return groupPosition;
//    }
//
//    @Override
//    public View getGroupView(int groupPosition, boolean isExpanded,
//                             View convertView, ViewGroup parent) {
//        SubCategories group = (SubCategories) getGroup(groupPosition);
//        if (convertView == null) {
//            LayoutInflater inf = (LayoutInflater) context
//                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
//            if (language.equalsIgnoreCase("En")) {
//                convertView = inf.inflate(R.layout.group_item, null);
//            }else if (language.equalsIgnoreCase("Ar")){
//                convertView = inf.inflate(R.layout.group_item_arabic, null);
//            }
//        }
//
//        RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.grp_layout);
//        TextView tv = (TextView) convertView.findViewById(R.id.group_name);
//        TextView groupItemQty  = (TextView) convertView.findViewById(R.id.group_item_qty);
//        ImageView iv = (ImageView) convertView.findViewById(R.id.group_indicator);
//        ImageView groupIcon = (ImageView) convertView.findViewById(R.id.group_icon);
//
//        Glide.with(context).load(Constants.IMAGE_URL+group.getImages()).into(groupIcon);
//
//        if (language.equalsIgnoreCase("En")) {
//            tv.setText(WordUtils.capitalizeFully(group.getSubCat()));
//        }else if (language.equalsIgnoreCase("Ar")){
//            tv.setText(WordUtils.capitalizeFully(group.getSubCatAr()));
//        }
//
////        context.startActivity(new Intent(context, DonutSelection.class));
//
//        if (isExpanded) {
//            iv.setImageResource(R.drawable.down_arrow);
//        } else {
//            if (language.equalsIgnoreCase("En")) {
//                iv.setImageResource(R.drawable.next_arrow);
//            }else if (language.equalsIgnoreCase("Ar")){
//                iv.setImageResource(R.drawable.left_arrow);
//            }
//        }
//
//        int cnt = myDbHelper.getSubcatOrderCount(group.getSubCatId());
//        if(cnt !=0) {
//            groupItemQty.setVisibility(View.VISIBLE);
//            groupItemQty.setText(""+cnt);
//        }else{
//            groupItemQty.setVisibility(View.GONE);
//        }
//
//        return convertView;
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return true;
//    }
//
//    @Override
//    public boolean isChildSelectable(int groupPosition, int childPosition) {
//        return true;
//    }
//
//
//    Animation.AnimationListener animL = new Animation.AnimationListener() {
//
//        @Override
//        public void onAnimationStart(Animation animation) {
//        }
//
//        @Override
//        public void onAnimationRepeat(Animation animation) {
//        }
//
//        @Override
//        public void onAnimationEnd(Animation animation) {
//            //this is just a method call you can create to delete the animated view or hide it until you need it again.
////            clearAnimation();
//        }
//    };
//
//}