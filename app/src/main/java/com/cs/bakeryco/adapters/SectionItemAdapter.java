package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CategoriesListActivity;
import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.Section;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.bakeryco.activities.PastriesSelection.itemQty;
import static com.cs.bakeryco.activities.PastriesSelection.itemfinalQty;
import static com.cs.bakeryco.activities.PastriesSelection.itemfinalprice;

/**
 * Created by CS on 1/24/2018.
 */

public class SectionItemAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Section> orderList = new ArrayList<>();
    ArrayList<Order> orderDB = new ArrayList<>();
    String language;
    Activity parentActivity;
    private DataBaseHelper myDbHelper;
    int value;
    public static int Qty = 0;
    public static float orderPrice = 0;
    String itemid;

    public SectionItemAdapter(Context context, ArrayList<Section> groups, int value, String language,
                              Activity parentActivity) {
        this.context = context;
        this.orderList = groups;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.value = value;
        myDbHelper = new DataBaseHelper(context);
    }

    @Override
    public int getCount() {
        return orderList.get(value).getChildItems().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView title, itemQty, price;
        ImageView itemImage, minus, plus;
        LinearLayout grid_layout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.pastries_selection_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemQty = (TextView) convertView.findViewById(R.id.donut_qty);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.donut_image);
            holder.minus = (ImageView) convertView.findViewById(R.id.donut_minus);
            holder.plus = (ImageView) convertView.findViewById(R.id.donut_plus);
            holder.grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);
            holder.price = (TextView) convertView.findViewById(R.id.item_price);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.price.setVisibility(View.VISIBLE);

//        orderPrice = Float.parseFloat(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());

//        final int Qty1 = myDbHelper.getItemOrderCount(orderList.get(value).getChildItems().get(position).getItemId());
//        holder.itemQty.setText(""+Qty1);
        if (PastriesSelection.itemQty != null) {

            int count = 0;
            for (int i = 0; i < itemQty.size(); i++) {
                if (itemQty.get(i).equals(orderList.get(value).getChildItems().get(position).getItemId())) {
                    count = count + 1;
                }

            }
            if (count > 0) {

                holder.itemQty.setText("" + count);
            } else {
                holder.itemQty.setText("0");
            }
        } else {
            holder.itemQty.setText("0");
        }

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                int getSavedCount = myDbHelper.getItemOrderCount(orderList.get(value).getChildItems().get(position).getItemId());
//                int getSavedPrice = Integer.parseInt(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice()) * getSavedCount;
//                Log.i("TAG", "saved count" + getSavedCount);
//
//                int getsubcatcount = myDbHelper.getSubcatOrderCount(orderList.get(value).getChildItems().get(position).getSubCatId());
//
//                if (getSavedCount == 0) {
//
//
//                    holder.itemQty.setText("1");
//                    HashMap<String, String> values = new HashMap<>();
//                    values.put("itemId", orderList.get(value).getChildItems().get(position).getItemId());
//                    values.put("itemTypeId", orderList.get(value).getChildItems().get(position).getPriceList().get(0).getSize());
//                    values.put("subCategoryId", orderList.get(value).getChildItems().get(position).getSubCatId());
//                    values.put("additionals", "");
//                    values.put("qty", "1");
//                    values.put("price", orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());
//                    values.put("additionalsPrice", "");
//                    values.put("additionalsTypeId", "");
//                    values.put("totalAmount", orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());
//                    values.put("comment", "");
//                    values.put("status", "1");
//                    values.put("creationDate", "14/07/2015");
//                    values.put("modifiedDate", "14/07/2015");
//                    values.put("categoryId", "");
//                    values.put("itemName", orderList.get(value).getChildItems().get(position).getItemName());
//                    values.put("itemNameAr", orderList.get(value).getChildItems().get(position).getItemNameAr());
//                    values.put("image", orderList.get(value).getChildItems().get(position).getImages());
//                    values.put("item_desc", orderList.get(value).getChildItems().get(position).getDescription());
//                    values.put("item_desc_Ar", orderList.get(value).getChildItems().get(position).getDescriptionAr());
//                    values.put("sub_itemName", "");
//                    values.put("sub_itemName_Ar", "");
//                    values.put("sub_itemImage", "");
//
//                    myDbHelper.insertOrder(values);
//                    try {
//                        double number;
//                        number = myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim = new DecimalFormat("0.00");
//                        OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
//                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                        double number1;
//                        number1 = myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim1 = new DecimalFormat("0.00");
//                        CategoriesListActivity.orderPrice.setText("" + decim1.format(number) + " SR");
//                        CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    try {
//                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                        PastriesSelection.orderPrice.setText("" + orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice() + " SR");
//                        PastriesSelection.orderQuantity.setText(""+myDbHelper.getSubcatOrderCount(orderList.get(value).getChildItems().get(position).getSubCatId()));
//                        PastriesSelection.mcount_basket.setText("1");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                } else {

                if (itemfinalQty.size() == 0) {

                    orderPrice = 0;
                    Qty = 0;


                    Qty = Qty + 1;
                    orderPrice = orderPrice + Integer.parseInt(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());

                    itemQty.add(orderList.get(value).getChildItems().get(position).getItemId());
                    notifyDataSetChanged();

//                    myDbHelper.updateOrder(String.valueOf(Qty), String.valueOf(orderPrice), orderList.get(value).getChildItems().get(position).getItemId());

//                try {
//                    double number;
//                    number = myDbHelper.getTotalOrderPrice();
//                    DecimalFormat decim = new DecimalFormat("0.00");
//                    OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
//                    OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    double number1;
//                    number1 = myDbHelper.getTotalOrderPrice();
//                    DecimalFormat decim1 = new DecimalFormat("0.00");
//                    CategoriesListActivity.orderPrice.setText("" + decim1.format(number) + " SR");
//                    CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                } catch (NullPointerException e) {
//                    e.printStackTrace();
//                }
//
                    itemfinalQty.add(String.valueOf(Qty));
                    itemfinalprice.add(String.valueOf(orderPrice));

                    Log.i("TAG", "qty " + itemfinalQty);
                    Log.i("TAG", "price " + itemfinalprice);

                    try {
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
                        PastriesSelection.orderQuantity.setText("" + Qty);
                        PastriesSelection.mcount_basket.setText("" + Qty);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    Qty = Qty + 1;
                    orderPrice = orderPrice + Integer.parseInt(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());

                    itemQty.add(orderList.get(value).getChildItems().get(position).getItemId());
                    notifyDataSetChanged();

//                    myDbHelper.updateOrder(String.valueOf(Qty), String.valueOf(orderPrice), orderList.get(value).getChildItems().get(position).getItemId());

//                try {
//                    double number;
//                    number = myDbHelper.getTotalOrderPrice();
//                    DecimalFormat decim = new DecimalFormat("0.00");
//                    OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
//                    OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    double number1;
//                    number1 = myDbHelper.getTotalOrderPrice();
//                    DecimalFormat decim1 = new DecimalFormat("0.00");
//                    CategoriesListActivity.orderPrice.setText("" + decim1.format(number) + " SR");
//                    CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                } catch (NullPointerException e) {
//                    e.printStackTrace();
//                }
//
                    itemfinalQty.add(String.valueOf(Qty));
                    itemfinalprice.add(String.valueOf(orderPrice));

                    Log.i("TAG", "qty " + itemfinalQty);
                    Log.i("TAG", "price " + itemfinalprice);

                    try {
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
                        PastriesSelection.orderQuantity.setText("" + Qty);
                        PastriesSelection.mcount_basket.setText("" + Qty);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Qty = myDbHelper.getItemOrderCount(orderList.get(value).getChildItems().get(position).getItemId());

                if (itemfinalQty.size() == 0) {
                    if (Integer.parseInt(holder.itemQty.getText().toString()) > 0) {

                        orderPrice = 0;
                        Qty = 0;

                        Qty = Qty - 1;
                        orderPrice = orderPrice - Integer.parseInt(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());

                        itemQty.remove(orderList.get(value).getChildItems().get(position).getItemId());
//                    holder.itemQty.setText("" + Qty);
                        notifyDataSetChanged();
//                    myDbHelper.updateOrder(String.valueOf(Qty), String.valueOf(orderPrice), orderList.get(value).getChildItems().get(position).getItemId());

//                    if (Qty == 0) {
//                        notifyDataSetChanged();
//                        myDbHelper.deleteItemFromOrder(orderList.get(value).getChildItems().get(position).getItemId());
//
//                    }

                        itemfinalQty.add(String.valueOf(Qty));
                        itemfinalprice.add(String.valueOf(orderPrice));

                        try {
                            double number;
                            number = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim = new DecimalFormat("0.00");
                            OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
                            OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            double number1;
                            number1 = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim1 = new DecimalFormat("0.00");
                            CategoriesListActivity.orderPrice.setText("" + decim1.format(number) + " SR");
                            CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        try {
                            DecimalFormat decimalFormat = new DecimalFormat("0.00");
                            PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
                            PastriesSelection.orderQuantity.setText("" + Qty);
                            PastriesSelection.mcount_basket.setText("" + Qty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {

                    if (Integer.parseInt(holder.itemQty.getText().toString()) > 0) {
                        Qty = Qty - 1;
                        orderPrice = orderPrice - Integer.parseInt(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());

                        itemQty.remove(orderList.get(value).getChildItems().get(position).getItemId());
//                    holder.itemQty.setText("" + Qty);
                        notifyDataSetChanged();
//                    myDbHelper.updateOrder(String.valueOf(Qty), String.valueOf(orderPrice), orderList.get(value).getChildItems().get(position).getItemId());

//                    if (Qty == 0) {
//                        notifyDataSetChanged();
//                        myDbHelper.deleteItemFromOrder(orderList.get(value).getChildItems().get(position).getItemId());
//
//                    }

                        itemfinalQty.add(String.valueOf(Qty));
                        itemfinalprice.add(String.valueOf(orderPrice));

                        try {
                            double number;
                            number = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim = new DecimalFormat("0.00");
                            OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
                            OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            double number1;
                            number1 = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim1 = new DecimalFormat("0.00");
                            CategoriesListActivity.orderPrice.setText("" + decim1.format(number) + " SR");
                            CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        try {
                            DecimalFormat decimalFormat = new DecimalFormat("0.00");
                            PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
                            PastriesSelection.orderQuantity.setText("" + Qty);
                            PastriesSelection.mcount_basket.setText("" + Qty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        if (language.equalsIgnoreCase("En")) {

//            if (orderList.get(value).getChildItems().get(position).getSubItems().size() == 0) {
            holder.title.setText(orderList.get(value).getChildItems().get(position).getItemName());
            Log.i("TAG", "itemname " + orderList.get(value).getChildItems().get(position).getItemName());
//            }

            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getChildItems().get(position).getImage()).into(holder.itemImage);

            holder.price.setText(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice() + " SR");
        } else if (language.equalsIgnoreCase("Ar")) {

            holder.title.setText(orderList.get(value).getChildItems().get(position).getItemNameAr());
            Log.i("TAG", "itemname " + orderList.get(value).getChildItems().get(position).getItemNameAr());
//            }

            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getChildItems().get(position).getImage()).into(holder.itemImage);

            holder.price.setText(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice() + " SR");

        }
        return convertView;
    }
}
