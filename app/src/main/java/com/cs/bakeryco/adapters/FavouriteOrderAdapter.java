package com.cs.bakeryco.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.bakeryco.R;
import com.cs.bakeryco.model.FavouriteOrder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brahmam on 19/2/16.
 */
public class FavouriteOrderAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<FavouriteOrder> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public FavouriteOrderAdapter(Context context, ArrayList<FavouriteOrder> favOrderList, String language) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView favouriteName, storeName,orderDate,totalPrice, itemDetails;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.favourite_order_listitem, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.favourite_order_listitem_arabic, null);
            }


            holder.favouriteName = (TextView) convertView
                    .findViewById(R.id.favorite_name);
            holder.storeName = (TextView) convertView
                    .findViewById(R.id.store_name);
            holder.orderDate = (TextView) convertView
                    .findViewById(R.id.order_date);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.favorite_price);
            holder.itemDetails = (TextView) convertView.findViewById(R.id.item_details);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.favouriteName.setText(favOrderList.get(position).getFavoriteName());

        double number;
        number= Double.parseDouble(favOrderList.get(position).getTotalPrice());
        NumberFormat nf=new DecimalFormat("####.####");
        holder.totalPrice.setText(nf.format(number)+" SR");
        if(language.equalsIgnoreCase("En")){
            holder.storeName.setText(favOrderList.get(position).getStoreName());
            holder.itemDetails.setText(favOrderList.get(position).getItemDetails());
        }else if(language.equalsIgnoreCase("Ar")){
            holder.storeName.setText(favOrderList.get(position).getStoreName_ar());
            holder.itemDetails.setText(favOrderList.get(position).getItemDetails_ar());
        }
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat curFormater1 = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(" ");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getOrderDate().replace("  ", " 0"));
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                dateObj = curFormater1.parse(favOrderList.get(position).getOrderDate());
            }catch (ParseException pe){

            }
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.orderDate.setText(date);
//
//        holder.totalBalance.setText(String.format("%.2f", Float.parseFloat(storesList.get(position).getTotalBalance())));


        return convertView;
    }
}