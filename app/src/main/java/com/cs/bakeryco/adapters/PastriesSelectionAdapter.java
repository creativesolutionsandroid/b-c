package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CategoriesListActivity;
import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Section;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import static com.cs.bakeryco.activities.PastriesSelection.boxSize;
import static com.cs.bakeryco.activities.PastriesSelection.itemfinalQty;
import static com.cs.bakeryco.activities.PastriesSelection.qty_mins;
import static com.cs.bakeryco.activities.PastriesSelection.qty_plus;
import static com.cs.bakeryco.activities.PastriesSelection.sections;
import static com.cs.bakeryco.activities.PastriesSelection.subitemQty;
import static com.cs.bakeryco.activities.PastriesSelection.subitemfinalQty;
import static com.cs.bakeryco.activities.PastriesSelection.totalCount;
//import static com.cs.bakeryco.activities.PastriesSelection.value1;

/**
 * Created by CS on 28-02-2017.
 */

public class PastriesSelectionAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Section> orderList = new ArrayList<>();
    String language;
    Activity parentActivity;
    private DataBaseHelper myDbHelper;
    int value;
    int value1;
    int Qty = 0;
    float orderPrice;


    public PastriesSelectionAdapter(Context context, ArrayList<Section> groups, int value, int value1, String language,
                                    Activity parentActivity) {
        this.context = context;
        this.orderList = groups;
        this.value = value;
        this.value1 = value1;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }


    public int getCount() {
//        if (orderList.get(value).getChildItems().get(0).getSubItems().size() == 0) {
        return orderList.get(value).getChildItems().get(value1).getSubItems().size();
//        }else {
//            return orderList.get(value).getChildItems().get(value1).getSubItems().size();
//        }

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {
        TextView title, itemQty, price;
        ImageView itemImage, minus, plus;
        LinearLayout grid_layout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.pastries_selection_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemQty = (TextView) convertView.findViewById(R.id.donut_qty);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.donut_image);
            holder.minus = (ImageView) convertView.findViewById(R.id.donut_minus);
            holder.plus = (ImageView) convertView.findViewById(R.id.donut_plus);
            holder.grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);
            holder.price = (TextView) convertView.findViewById(R.id.item_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.price.setVisibility(View.GONE);
        if (language.equalsIgnoreCase("En")) {
            int pos = 0;
            Log.e("TAG", "value " + orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
//            for (int i = 0; i <= pos; i++) {
//                pos++;
            if (orderList.get(value).getChildItems().get(value1).getSubItems().size() == 0) {
//                holder.title.setText(orderList.get(value).getChildItems().get(value1).getItemName());
//                Log.e("TAG","subitemsize "+orderList.get(value).getChildItems().get(value1).getSubItems().size());
            } else {

                holder.title.setText(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                Log.e("TAG", "list " + orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                notifyDataSetChanged();
            }

        } else if (language.equalsIgnoreCase("Ar")) {

            if (orderList.get(value).getChildItems().get(value1).getSubItems().size() == 0) {
//                holder.title.setText(orderList.get(value).getChildItems().get(value1).getItemNameAr());
            } else {

                holder.title.setText(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemName_Ar());
                Log.e("TAG", "list " + orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemName_Ar());
                notifyDataSetChanged();
            }
        }

        if (orderList.get(value).getChildItems().get(value1).getSubItems().size() == 0) {
//            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getChildItems().get(value1).getImage()).into(holder.itemImage);
        } else {
            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage()).into(holder.itemImage);
            notifyDataSetChanged();
        }

//        holder.itemImage.setOnDragListener(this);
        holder.itemImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                PastriesSelection.pos = position;
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 1);
                view.setVisibility(View.VISIBLE);
                return true;
            }
        });

        if (PastriesSelection.selectedItems != null) {
            int count = 0;
            for (int i = 0; i < PastriesSelection.selectedItems.size(); i++) {
                if (PastriesSelection.selectedItems.get(i).getSubitemName().equals(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName())) {
                    count = count + 1;
                }
            }
            if (count > 0) {
                holder.itemQty.setText("" + count);
            } else {
                holder.itemQty.setText("0");
            }
        } else {
            holder.itemQty.setText("0");
        }

        if (PastriesSelection.selectedItems.size() == 0) {
            totalCount = 0;
            PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
        } else {
            PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
        }

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (subitemQty.size() == 0) {
                    if (totalCount < boxSize) {
                        totalCount = totalCount + 1;
                        PastriesSelection.selectedItems.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
                        subitemQty.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                        PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
                        PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
                        PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
                        PastriesSelection.mBoxAdapter.notifyDataSetChanged();
                        notifyDataSetChanged();

                    } else {
                        String message;
//                    if(language.equalsIgnoreCase("En")) {
                        message = "You are done with Maximum " + PastriesSelection.boxSize + " " + PastriesSelection.title + " per Box, to order more use other box.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(message)
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                } else {
                    if (totalCount < boxSize) {
                        totalCount = totalCount + 1;
                        PastriesSelection.selectedItems.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
                        subitemQty.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                        PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
                        PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
                        PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
                        PastriesSelection.mBoxAdapter.notifyDataSetChanged();
                        notifyDataSetChanged();

                    } else {
                        String message;
//                    if(language.equalsIgnoreCase("En")) {
                        message = "You are done with Maximum " + PastriesSelection.boxSize + " " + PastriesSelection.title + " per Box, to order more use other box.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(message)
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                }
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (subitemQty.size() == 0) {

                    if (Integer.parseInt(holder.itemQty.getText().toString()) > 0) {
                        totalCount = totalCount - 1;
                        PastriesSelection.selectedItems.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
                        PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
                        subitemQty.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                        PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
                        PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
                        PastriesSelection.mBoxAdapter.notifyDataSetChanged();
                        notifyDataSetChanged();
                    }
                }else {

                    if (Integer.parseInt(holder.itemQty.getText().toString()) > 0) {
                        totalCount = totalCount - 1;
                        PastriesSelection.selectedItems.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
                        PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
                        subitemQty.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                        PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
                        PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
                        PastriesSelection.mBoxAdapter.notifyDataSetChanged();
                        notifyDataSetChanged();
                    }
                }
            }
        });
        return convertView;
    }
}
