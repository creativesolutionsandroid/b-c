package com.cs.bakeryco.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.model.Order;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by CS on 3/31/2017.
 */

public class ConfirmationListAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty, price;
    String language, size;
    int pos = 0;
    String names,names1;
    View v;

    public ConfirmationListAdapter(Context context, ArrayList<Order> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView itemName, itemPrice, itemNo;
        LinearLayout additionalsLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.confirmation_list, null);
            }else if (language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.confirmation_list_arabic, null);
            }

            holder.itemName = (TextView) convertView.findViewById(R.id.confirmation_item_name);
            holder.itemPrice = (TextView) convertView.findViewById(R.id.confirmation_amount);
            holder.itemNo = (TextView) convertView.findViewById(R.id.confirmation_item_no);
//            holder.additionalsLayout = (LinearLayout) convertView.findViewById(R.id.additionals_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        List<String> namesList = null;
        List<String> namesList1 = null;
        List<String> pricesList = null;


        try {
            if (language.equalsIgnoreCase("En")) {
                names = orderList.get(position).getItemName();
                Log.e("TAG","name_En "+names);
            }else if(language.equalsIgnoreCase("Ar")){
                names1 = orderList.get(position).getItemNameAr();
                Log.e("TAG", "name_Ar "+names1);
            }
            String prices = orderList.get(position).getPrice();
            Log.e("TAG","price"+prices);
            if (language.equalsIgnoreCase("En")) {
                namesList = Arrays.asList(names.split(","));
                Log.e("TAG", "namelist " + namesList);
            }else if(language.equalsIgnoreCase("Ar")) {
                namesList1 = Arrays.asList(names1.split(","));
                Log.e("TAG", "namelistarabic " + namesList1);
            }
            pricesList = Arrays.asList(prices.split(","));
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.itemPrice.setVisibility(View.VISIBLE);
        holder.itemNo.setVisibility(View.VISIBLE);
//        holder.additionalsLayout.setVisibility(View.GONE);
        if (language.equalsIgnoreCase("En")) {
            holder.itemName.setText(orderList.get(position).getItemName());
        }else if (language.equalsIgnoreCase("Ar")) {
            holder.itemName.setText(orderList.get(position).getItemNameAr());
        }
//            holder.listview.setVisibility(View.GONE);
        double price;
        price= Double.parseDouble(orderList.get(position).getPrice());
        DecimalFormat decim1 = new DecimalFormat("0.00");
        holder.itemPrice.setText(""+decim1.format(price));
        if(position == 0){
            pos=0;
        }
        pos = pos+1;
        if (pos<10) {
            holder.itemNo.setText("0" + pos);
        }
        else{
            holder.itemNo.setText(""+pos);
        }
        return convertView;
    }
}
