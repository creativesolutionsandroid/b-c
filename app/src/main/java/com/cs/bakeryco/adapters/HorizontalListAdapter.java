package com.cs.bakeryco.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.model.Order;

import java.util.ArrayList;

/**
 * Created by CS on 04-07-2016.
 */
public class HorizontalListAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty, price;

    public HorizontalListAdapter(Context context, ArrayList<Order> orderList) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView itemName, itemQty;
        ImageView itemImae;
        CardView storeItemLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();


            convertView = inflater.inflate(R.layout.horizontal_list_row, null);

            holder.itemImae = (ImageView) convertView.findViewById(R.id.item_image);
            holder.itemName = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemQty = (TextView) convertView.findViewById(R.id.item_qty);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }




        holder.itemQty.setText(orderList.get(position).getQty());
        holder.itemName.setText(orderList.get(position).getItemName());
        holder.itemImae.setImageResource(R.drawable.child_image);
//        Glide.with(context).load("http://www.ircfood.com/images/"+orderList.get(position).getItemImage()).into(holder.itemImae);
        return convertView;
    }
}

