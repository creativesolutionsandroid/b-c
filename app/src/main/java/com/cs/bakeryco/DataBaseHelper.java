package com.cs.bakeryco;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cs.bakeryco.model.Order;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 12-06-2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String LOGCAT = null;

    public DataBaseHelper(Context applicationcontext) {
        super(applicationcontext, "bakeryco.db", null, 2);
        Log.d(LOGCAT,"Created");
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String query,query1;
        query = "CREATE TABLE \"OrderTable\" (\"orderId\" INTEGER PRIMARY KEY  AUTOINCREMENT ,\"itemId\" INTEGER,\"itemTypeId\" INTEGER, \"subCategoryId\" INTEGER,\"additionals\" VARCHAR,\"qty\"VARCHAR,\"price\" TEXT DEFAULT 0,\"additionalsPrice\" INTEGER,\"additionalsTypeId\" TEXT DEFAULT 0,\"totalAmount\" TEXT DEFAULT 0,\"comment\" TEXT DEFAULT 0,\"status\" TEXT , \"creationDate\" TEXT DEFAULT 0, \"modifiedDate\" INTEGER DEFAULT 0,\"categoryId\" TEXT,\"itemName\" TEXT DEFAULT 0,\"itemNameAr\" TEXT,\"image\"TEXT,\"item_desc\" TEXT,\"item_desc_Ar\" TEXT,\"sub_itemName\" TEXT,\"sub_itemName_Ar\" TEXT,\"sub_itemImage\" TEXT)";

        database.execSQL(query);
    }
    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        String query;
        query = "DROP TABLE IF EXISTS OrderTable";
        database.execSQL(query);        onCreate(database);
    }


    public void insertOrder(HashMap<String, String> queryValues) {
        Log.e("TAG","insertorder");
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("itemId", queryValues.get("itemId"));
        values.put("itemTypeId", queryValues.get("itemTypeId"));
        values.put("subCategoryId", queryValues.get("subCategoryId"));
        values.put("additionals", queryValues.get("additionals"));
        values.put("qty", queryValues.get("qty"));
        values.put("price", queryValues.get("price"));
        values.put("additionalsPrice", queryValues.get("additionalsPrice"));
        values.put("additionalsTypeId", queryValues.get("additionalsTypeId"));
        values.put("totalAmount", queryValues.get("totalAmount"));
        values.put("comment", queryValues.get("comment"));
        values.put("status", queryValues.get("status"));
        values.put("creationDate", queryValues.get("creationDate"));
        values.put("modifiedDate", queryValues.get("modifiedDate"));
        values.put("categoryId", queryValues.get("categoryId"));
        values.put("itemName", queryValues.get("itemName"));
        values.put("itemNameAr", queryValues.get("itemNameAr"));
        values.put("image",queryValues.get("image"));
        values.put("item_desc",queryValues.get("item_desc"));
        values.put("item_desc_Ar",queryValues.get("item_desc_Ar"));
        values.put("sub_itemName",queryValues.get("sub_itemName"));
        values.put("sub_itemName_Ar",queryValues.get("sub_itemName_Ar"));
        values.put("sub_itemImage",queryValues.get("sub_itemImage"));

        database.insert("OrderTable", null, values);
        database.close();
        Log.e("TAG","insertorder1");
    }

    public void updateOrder(String qty, String price, String itemId){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET qty ="+qty+ ",totalAmount ="+ price +" where itemId ="+itemId;
        database.execSQL(updateQuery);
    }


    public ArrayList<Order> getOrderInfo(){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Log.e("TAG","orderinfo");
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setItemId(cursor.getString(1));
                sc.setItemTypeId(cursor.getString(2));
                sc.setSubCatId(cursor.getString(3));
                sc.setAdditionals(cursor.getString(4));
                sc.setQty(cursor.getString(5));
                sc.setPrice(cursor.getString(6));
                sc.setAdditionalsPrice(cursor.getString(7));
                sc.setAdditionalsTypeId(cursor.getString(8));
                sc.setTotalAmount(cursor.getString(9));
                sc.setComment(cursor.getString(10));
                sc.setStatus(cursor.getString(11));
                sc.setCreationDate(cursor.getString(12));
                sc.setModifiedDate(cursor.getString(13));
                sc.setCategoryId(cursor.getString(14));
                sc.setItemName(cursor.getString(15));
                sc.setItemNameAr(cursor.getString(16));
                sc.setImage(cursor.getString(17));
                sc.setItem_desc(cursor.getString(18));
                sc.setItem_desc_Ar(cursor.getString(19));
                sc.setSub_itemName(cursor.getString(20));
                sc.setSub_itemName_Ar(cursor.getString(21));
                sc.setSub_itemImage(cursor.getString(22));

                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public int getItemOrderCount(String itemId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where itemId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getItemOrderCountWithSize(String itemId, String size){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where itemId="+itemId+" AND itemTypeId ="+size;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getItemOrderPrice(String itemId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(price) FROM  OrderTable where orderId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getSubcatOrderCount(String subCategoryId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where subCategoryId="+subCategoryId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getTotalOrderQty(){
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable", null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        }catch (SQLiteException sqle){
            sqle.printStackTrace();
        }
        return qty;
    }


    public int getCategoryQty(String catId){
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable where categoryId = "+catId, null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        }catch (SQLiteException sqle){
            sqle.printStackTrace();
        }
        return qty;
    }

    public float getTotalOrderPrice(){
        float qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT SUM(totalAmount) FROM OrderTable", null);
        if(cur.moveToFirst())
        {
            qty  = cur.getFloat(0);
        }

        return qty;
    }

    public void deleteSubItemFromOrder(String orderId){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where subCategoryId = "+orderId;
        database.execSQL(updateQuery);

    }


    public void deleteItemFromOrder(String orderId){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where itemId = "+orderId;
        database.execSQL(updateQuery);

    }

    public void deleteOrderTable(){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable";
        database.execSQL(updateQuery);
    }

    public void updateComment(String orderId, String comment, String size){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET comment = '"+comment+"' where itemId ="+orderId+" AND itemTypeId ="+size;
        database.execSQL(updateQuery);
    }


    public ArrayList<Order> getItemOrderInfo(String itemId){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where itemId = "+itemId;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setItemId(cursor.getString(1));
                sc.setItemTypeId(cursor.getString(2));
                sc.setSubCatId(cursor.getString(3));
                sc.setAdditionals(cursor.getString(4));
                sc.setQty(cursor.getString(5));
                sc.setPrice(cursor.getString(6));
                sc.setAdditionalsPrice(cursor.getString(7));
                sc.setAdditionalsTypeId(cursor.getString(8));
                sc.setTotalAmount(cursor.getString(9));
                sc.setComment(cursor.getString(10));
                sc.setStatus(cursor.getString(11));
                sc.setCreationDate(cursor.getString(12));
                sc.setModifiedDate(cursor.getString(13));
                sc.setCategoryId(cursor.getString(14));
                sc.setItemName(cursor.getString(15));
                sc.setItemNameAr(cursor.getString(16));
                sc.setImage(cursor.getString(17));
                sc.setItem_desc(cursor.getString(18));
                sc.setItem_desc_Ar(cursor.getString(19));
                sc.setSub_itemName(cursor.getString(20));
                sc.setSub_itemName_Ar(cursor.getString(21));
                sc.setSub_itemImage(cursor.getString(22));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

}
