package com.cs.bakeryco;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.activities.OrderActivity;
import com.cs.bakeryco.fragments.MainFragment;
import com.cs.bakeryco.fragments.MoreFragment;
import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends AppCompatActivity {

    TextView prof_name;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    DrawerListAdapter adapter;
    private String[] mSidemenuTitles;
    private int[] mSidemenuIcons;
    private ListView mDrawerList;
    private TextView mTitleTV;
    TextView langAr, langEng;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    Double lat, longi;

    private LinearLayout mDrawerLinear;
    private String mTitle,mAppTitle;
    FragmentManager fragmentManager = getSupportFragmentManager();
    boolean doubleBackToExitPressedOnce = false;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        setContentView(R.layout.activity_main);

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                }
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }

        mAppTitle = (String) getTitle();
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        langAr = (TextView) findViewById(R.id.language_arabic);
        langEng = (TextView) findViewById(R.id.language_eng);
        if(language.equalsIgnoreCase("En")){
            mSidemenuTitles = new String[] { "Main", "Order", "More",
                    "Share Our App","Rate Our App" };
            langAr.setVisibility(View.VISIBLE);
            langEng.setVisibility(View.GONE);
        }else if(language.equalsIgnoreCase("Ar")) {
            mSidemenuTitles = new String[]{"الرئيسية", "الطلب", "المزيد",
                    "مشاركة طلبنا", "قيم تطبيقنا"};
            langAr.setVisibility(View.GONE);
            langEng.setVisibility(View.VISIBLE);
        }
//        mSidemenuTitles1 = new String[] { "Main", "Store", "Menu", "Account",
//                "Share Our App","Rate Our App" };
        mSidemenuIcons = new int[] {R.drawable.menu_main,R.drawable.menu_order,R.drawable.menu_more,R.drawable.menu_share,R.drawable.menu_rating};
//        mTitle = mSidemenuTitles1[0];
//        mTitle = "Oregano Pi";
        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                mSidemenuTitles, mSidemenuIcons, language);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLinear = (LinearLayout) findViewById(R.id.left_drawer);

        mTitleTV = (TextView) findViewById(R.id.header_title) ;
        configureToolbar();
        configureDrawer();
        mDrawerList.setAdapter(adapter);



        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        Fragment fragment = new MainFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        try{
            int start = getIntent().getIntExtra("startWith",-1);
            if(start != -1) {
                selectItem(start);
            }
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }

        if(language.equalsIgnoreCase("En")){
            mTitleTV.setText("Bakery & Co.");
        }else if(language.equalsIgnoreCase("Ar")) {
            mTitleTV.setText("بيكري آند كومباني");
        }

        langAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagePrefsEditor.putString("language","Ar");
                languagePrefsEditor.commit();
                recreate();
            }
        });

        langEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagePrefsEditor.putString("language","En");
                languagePrefsEditor.commit();
                recreate();
            }
        });

    }

    public void getGPSCoordinates(){
        gps = new GPSTracker(MainActivity.this);
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
//                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(MainActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(MainActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private void configureToolbar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mainToolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
//        mTitleTV.setText(mSidemenuTitles1[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);

                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    public void configureDrawer() {
        // Configure drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_closed) {

            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
//                mTitleTV.setText(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
//                mTitleTV.setText(mAppTitle);
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }


    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            view.setSelected(true);
            selectItem(position);
        }
    }

    public void selectItem(int position) {

        // update the main content by replacing fragments
        switch (position) {
            case 0:
                if(language.equalsIgnoreCase("En")){
                    langAr.setVisibility(View.VISIBLE);
                    langEng.setVisibility(View.GONE);
                    mTitleTV.setText("Bakery & Co");
                }else if(language.equalsIgnoreCase("Ar")) {
                    langAr.setVisibility(View.GONE);
                    langEng.setVisibility(View.VISIBLE);
                    mTitleTV.setText("بيكري آند كومباني");
                }

                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);
                Fragment unitsFragment = new MainFragment();

                fragmentManager.beginTransaction().replace(R.id.content_frame, unitsFragment).commit();
//
//                AnalyticsManager.sendScreenView("Units Screen");
                break;

            case 1:
                langAr.setVisibility(View.GONE);
                langEng.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mTitleTV.setText("Order");
                }else if (language.equalsIgnoreCase("Ar")){
                    mTitleTV.setText("الطلب");
                }
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);
                Fragment orderFragment = new OrderActivity();

                fragmentManager.beginTransaction().replace(R.id.content_frame, orderFragment).commit();
                break;

//            case 2:
//                Intent cateringIntent = new Intent(MainActivity.this, CateringActivity.class);
//                startActivity(cateringIntent);
//                break;
//
//            case 3:
//                Intent i = new Intent(MainActivity.this, TrackOrderActivity.class);
//                startActivity(i);
//                break;
            case 2:
                langAr.setVisibility(View.GONE);
                langEng.setVisibility(View.GONE);
                if(language.equalsIgnoreCase("En")){
                    mTitleTV.setText("More");
                }else if(language.equalsIgnoreCase("Ar")) {
                    mTitleTV.setText("المزيد");
                }
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);
                Fragment colorFragment = new MoreFragment();

                fragmentManager.beginTransaction().replace(R.id.content_frame, colorFragment).commit();

//                AnalyticsManager.sendScreenView("Color Coding Screen");
                break;

            case 3:

//                mTitle = mAppTitle;
                //mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);


                new Handler().postDelayed(new Runnable() {

                    // Using handler with postDelayed called runnable run method

                    @Override
                    public void run() {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT,
                                "https://play.google.com/store/apps/details?id=com.cs.bakeryco&hl=en");
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                                "Check out Bakery & Company App !");
                        startActivity(Intent.createChooser(intent, "Share"));
                    }
                }, 200);
                break;

            case 4:
//                mTitle = mAppTitle;
                //mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.bakeryco")));

                new Handler().postDelayed(new Runnable() {

                    // Using handler with postDelayed called runnable run method

                    @Override
                    public void run() {
//                        AppRater.app_launched(MainActivity.this);
                    }
                }, 200);
                break;




            default:
                break;
        }

//		if (fragment != null)
//			extracted(position, fragment);
//		else {
//			// error in creating fragment
//			Log.e("MainActivity", "Error in creating fragment");
//		}

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,"Press BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


}
