package com.cs.bakeryco.widgets;

/**
 * Created by CS on 7/10/2017.
 */

public enum ScrollState {
    STOP,
    UP,
    DOWN,
}
