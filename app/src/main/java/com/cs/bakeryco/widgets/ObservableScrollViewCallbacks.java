package com.cs.bakeryco.widgets;

/**
 * Created by CS on 7/10/2017.
 */

public interface ObservableScrollViewCallbacks {
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging);

    public void onDownMotionEvent();

    public void onUpOrCancelMotionEvent(ScrollState scrollState);

    public void onScrollEnded(ObservableScrollView observableScrollView, int l,
                              int t, int oldl, int oldt);
}
