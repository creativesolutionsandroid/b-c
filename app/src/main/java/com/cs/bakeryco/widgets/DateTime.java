package com.cs.bakeryco.widgets;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.cs.bakeryco.R;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

/**
 * Created by CS on 3/15/2017.
 */

public class DateTime extends AppCompatActivity {

    private View myPickerView;

    Boolean isToday;

    Context context;

    private int mYear, mMonth, mDay, mHour, mMinute;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    public DateTime(Context context) {
        this(context, null);

        init(context);
    }
    public DateTime(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DateTime(Context context, AttributeSet attrs, int defStyle) {
//          // Get LayoutInflater instance
//          final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//          // Inflate myself
//          inflater.inflate(R.layout.datetimepicker, this, true);
        LayoutInflater inflator = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myPickerView = inflator.inflate(R.layout.date_time, null);

        initializeReference();

    }


    private void init(Context mContext) {
        LayoutInflater inflator = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myPickerView = inflator.inflate(R.layout.date_time, null);

        initializeReference();
    }
    @TargetApi(Build.VERSION_CODES.N)
    private void initializeReference() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(DateTime.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if(year == mYear && monthOfYear == mMonth && dayOfMonth == mDay){
                            isToday = true;
                        }else {
                            isToday = false;
                        }
                        mYear = year;
                        mDay = dayOfMonth;
                        mMonth = monthOfYear;

                        final int hour = c.get(Calendar.HOUR_OF_DAY);
                        final int minute = c.get(Calendar.MINUTE);

                        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                                (TimePickerDialog.OnTimeSetListener) DateTime.this,
                                hour,
                                minute,
                                false
                        );
                        tpd.setThemeDark(true);
                        tpd.vibrate(false);
                        tpd.setAccentColor(Color.parseColor("#76C8FC"));
                        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                Log.d("TimePicker", "Dialog was cancelled");
                            }
                        });
                        if (isToday) {
                            tpd.setMinTime(hour + 1, minute, 00);
                        }else {
                            tpd.setMinTime(0,0,0);
                        }
                        tpd.show(getFragmentManager(), "Timepickerdialog");



                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();
    }
}
