package com.cs.bakeryco;

import com.cs.bakeryco.model.MainCategories;

import java.util.ArrayList;

/**
 * Created by CS on 09-06-2016.
 */
public class Constants {

//    static String live_url = "http://www.ircfood.com";
////    static String live_url = "http://85.194.94.241/OreganoServices";
//
//    public static String CATEGORY_ITEMS_URL = live_url+"/api/SubCategoryApi/";
//    public static String ADDITIONALS_URL = live_url+"/Api/ModifierInfoList/";
//    public static String STORES_URL = live_url+"/api/StoreInformationApi/";
//    public static String LOGIN_URL = live_url+"/api/VerifyUserCredentialsApi/";
//    public static String REGISTRATION_URL = live_url+"/api/RegistrationApi";
//    public static String INSERT_ORDER_URL = live_url+"/api/OrderDetailsApi";
//    public static String ORDER_HISTORY_URL = live_url+"/api/OrderTrackApi/";
//    public static String SAVE_ADDRESS_URL = live_url+"/api/RegistrationApi/";
//    public static String TRACK_ORDER_STEPS_URL = live_url+"/api/OrderTrackApi?orderId=";
//    public static String SAVED_ADDRESS_URL = live_url+"/api/RegistrationApi/";
//    public static String GET_FAVORITE_ORDERS_URL = live_url+"/api/FaviorateOrderApi/";
//    public static String DELETE_FAVORITE_ORDERS_URL = live_url+"/api/FaviorateOrderApi/";
//    public static String INSERT_FAVORITE_ORDER_URL = live_url+"/api/FaviorateOrderApi?OrderId=";
//    public static String CHANGE_PASSWORD_URL = live_url+"/api/VerifyUserCredentialsApi/";
//    public static String ORDERD_DETAILS_URL = live_url+"/api/OrderDetailsApi?OrderId=";
//    public static String VERIFY_RANDOM_NUMBER_URL = live_url+"/api/RegistrationApi?MobileNo=";
//    public static String GET_SAVED_CARDS_URL = live_url+"/api/CreditCardDetails?userId=";
//    public static String SAVE_CARD_DETAILS_URL = live_url+"/api/CreditCardDetails?userId=";
//    public static String FORGOT_PASSWORD_URL = live_url+"/api/VerifyUserCredentialsApi?UsrName=";
//    public static String GET_CURRENT_TIME_URL = live_url+"/api/GetCurrentTime";
//    public static String EDIT_ADDRESS_URL = live_url+"/api/RegistrationApi";
//    public static String GET_MESSAGES_URL = live_url+"/api/PushMsg?userid=";
//    public static String GET_OFFERS_URL = live_url+"/api/OfferDetails?offerId=";
//    public static String UPDATE_MESSAGE_URL = live_url+"/api/PushMsg";
//    public static String UPDATE_PROFILE_URL = live_url+"/api/VerifyUserCredentialsApi";
//    public static String GET_BANNERS_URL = live_url+"/api/OfferDetails";
//    public static String GET_PROMOS_URL = live_url+"/api/OreganoPromotions?userId=";
//    public static String LIVE_TRACKING_URL = live_url+"/api/OrderTrackApi?OrderId=";
//    public static String DELETE_ORDER_FROM_HISTORY = live_url + "OrderDetailsApi?Delete_OrderID=";

    static String local_url = "http://csadms.com/bncservices";

    public static String CATEGORY_ITEMS_URL = local_url+"/api/ItemDetails/GetSubCategoryItemsDetails";
    public static String LOGIN_URL = local_url+"/api/RegistrationApi/Signin/";
    public static String REGISTRATION_URL = local_url+"/api/RegistrationApi/RegisterUser";
    public static String INSERT_ORDER_URL = local_url+"/api/OrderDetails/InsertOrder";
    public static String ORDER_HISTORY_URL = local_url+"/api/OrderDetails/GetOrderHistory?userId=";
    public static String SAVE_ADDRESS_URL = local_url+"/api/RegistrationApi/InsertNewAddress";
    public static String TRACK_ORDER_STEPS_URL = local_url+"/api/OrderDetails/TrackOrder?orderId=";
    public static String SAVED_ADDRESS_URL = local_url+"/api/RegistrationApi/GetAddressDetails?userid=";
    public static String DELETE_ADDRESS_URL = local_url+"/api/RegistrationApi/DeleteUserAddress?addressid=";
    public static String GET_FAVORITE_ORDERS_URL = local_url+"/api/OrderDetails/GetFaviorateOrder?userid=";
    public static String DELETE_FAVORITE_ORDERS_URL = local_url+"/api/OrderDetails/DeleteFaviorateOrder?orderId=";
    public static String INSERT_FAVORITE_ORDER_URL = local_url+"/api/OrderDetails/InsertFaviorateOrder?orderId=";
    public static String CHANGE_PASSWORD_URL = local_url+"/api/RegistrationApi/ChangePassword/";
    public static String ORDERD_DETAILS_URL = local_url+"/api/OrderDeils/GetOrderDetails?userId=";
    public static String VIEW_ORDERD_DETAILS_URL=local_url+"/api/OrderDetails/GetOrderDetails?userId=";
    public static String VERIFY_RANDOM_NUMBER_URL = local_url+"/api/RegistrationApi/VerifyMobileNo?MobileNo=";
    public static String GET_SAVED_CARDS_URL = local_url+"/api/CreditCardDetails?userId=";
    public static String SAVE_CARD_DETAILS_URL = local_url+"/api/CreditCardDetails?userId=";
    public static String FORGOT_PASSWORD_URL = local_url+"/api/RegistrationApi/ForgetPassword?Username=";
    public static String GET_CURRENT_TIME_URL = local_url+"/api/CurrentTime/GetCurrentTime";
    public static String EDIT_ADDRESS_URL = local_url+"/api/RegistrationApi/UpdateUserAddress";
    public static String GET_MESSAGES_URL = local_url+"/api/PushMsg?userid=";
    public static String GET_OFFERS_URL = local_url+"/api/OfferDetails?offerId=";
    public static String UPDATE_MESSAGE_URL = local_url+"/OreganoServices/api/PushMsg";
    public static String UPDATE_PROFILE_URL = local_url+"/api/RegistrationApi/UpdateUserProfile";
    public static String GET_BANNERS_URL = local_url+"/api/OfferDetails";
    public static String LIVE_TRACKING_URL = local_url+"/api/OrderDetails/GetOrderLocation?orderId=";
    public static String CORPORATE_ORDER_URL = local_url+"/api/OrderDetails/InsertCorporateRequest";
    public static String CANCEL_ORDER_URL = local_url+"/api/OrderDetails/CancelOrder";
    public static String SEND_OTP = local_url + "/api/RegistrationApi/SendOTP?userid=";
    public static String STORES_URL = local_url + "/api/StoreInformation/GetStoreDetails?day=";
    public static String DELETE_ORDER_FROM_HISTORY = local_url + "/api/OrderDetails/DeleteOrder?orderId=";
    public static String RATING_URL=local_url+"/api/OrderDetails/PostInsertRating";
    public static String GET_PROMOS_URL = local_url + "/api/Promotions/GetPromotions?userId=";

    public static String IMAGE_URL = "http://csadms.com/bncservices/images/";

    public static String ORDER_TYPE = "";
    public static String COMMENTS = "";

    public static ArrayList<MainCategories> mainCategories = new ArrayList<>();
}
