package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.DropDownListAdapter;
import com.cs.bakeryco.adapters.PastriesBoxItemsAdapter;
import com.cs.bakeryco.adapters.PastriesSelectionAdapter;
import com.cs.bakeryco.adapters.SectionItemAdapter;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubItems;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 28-08-2017.
 */

public class PastriesSelection extends AppCompatActivity implements View.OnClickListener, View.OnDragListener {

    TextView individual, box, mini, petite, boxName;
//    TextView m6PcsBox, m12PcsBox, m24PcsBox;

    ListView boxlist, minilist, petitelist;
    TextView Title;
    public static String title;
    public static TextView mNumBox;
    LinearLayout pcs_layout;

    RelativeLayout maddmore, maddmore1;

    public static int rowSize = 2;
    public static TextView orderPrice, orderQuantity, mcount_basket;

    public static ArrayList<Section> sections = new ArrayList<>();
    public static ArrayList<SubItems> selectedItems = new ArrayList<SubItems>();
    ArrayList<Items> items = new ArrayList<>();
    public static ArrayList<Items> Boxitems = new ArrayList<>();

    public static ArrayList<String> subitemQty = new ArrayList<>();
    public static ArrayList<String> subitemfinalQty = new ArrayList<>();
    public static ArrayList<String> subitemfinalprice = new ArrayList<>();

    public static ArrayList<String> itemQty = new ArrayList<>();
    public static ArrayList<String> itemfinalQty = new ArrayList<>();
    public static ArrayList<String> itemfinalprice = new ArrayList<>();
    GridView mGridView, mGridView1;
    public static GridView mBoxGridView, mGridview2;
    PastriesSelectionAdapter mAdapter;
    DropDownListAdapter mDropDownAdapter;
    public static PastriesBoxItemsAdapter mBoxAdapter;
    ImageView fav;
    int value1 = 0;

    public static SectionItemAdapter mItemAdapter;

    LinearLayout mindividual_list_layout, mbox_list_layout, mmini_list_layout, mpetite_list_layout;

    ListView mindividual_list, mbox_list, mmini_list, mpetite_list;

    FrameLayout mbox_layout;

    public static ImageView qty_plus, qty_mins;

    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;

    DataBaseHelper myDbHelper;

    int Qty = 0;
    float orderprice;

//    String value, value1;

    int position;
    public static int pos = 0, totalCount = 0;
    public static int boxSize;

    String no_of_items;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.pastries_selection);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.pastries_selection);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sections = (ArrayList<Section>) getIntent().getSerializableExtra("list");
        title = getIntent().getStringExtra("title");

        pos = 0;
        totalCount = 0;
        boxSize = 0;
        Boxitems.clear();
        subitemQty.clear();
        subitemfinalprice.clear();
        subitemfinalQty.clear();
        itemQty.clear();
        itemfinalQty.clear();
        itemfinalprice.clear();
        selectedItems.clear();
        myDbHelper = new DataBaseHelper(PastriesSelection.this);

        individual = (TextView) findViewById(R.id.individual);
        box = (TextView) findViewById(R.id.box);
        mini = (TextView) findViewById(R.id.mini);
        petite = (TextView) findViewById(R.id.petite);
        mNumBox = (TextView) findViewById(R.id.box_size);
        boxName = (TextView) findViewById(R.id.box_name);
        Title = (TextView) findViewById(R.id.header_title);

        qty_plus = (ImageView) findViewById(R.id.count_plus);
        qty_mins = (ImageView) findViewById(R.id.count_mins);

        mbox_layout = (FrameLayout) findViewById(R.id.box_layout);

        mindividual_list = (ListView) findViewById(R.id.individual_list);
        mbox_list = (ListView) findViewById(R.id.box_list);
        mmini_list = (ListView) findViewById(R.id.mini_list);
        mpetite_list = (ListView) findViewById(R.id.petite_list);

        mindividual_list_layout = (LinearLayout) findViewById(R.id.individual_list_layout);
        mbox_list_layout = (LinearLayout) findViewById(R.id.box_list_layout);
        mmini_list_layout = (LinearLayout) findViewById(R.id.mini_list_layout);
        mpetite_list_layout = (LinearLayout) findViewById(R.id.petite_list_layout);

        orderPrice = (TextView) findViewById(R.id.item_price);
        orderQuantity = (TextView) findViewById(R.id.item_qty);
        mcount_basket = (TextView) findViewById(R.id.total_count);

        maddmore = (RelativeLayout) findViewById(R.id.addmore_layout);
        maddmore1 = (RelativeLayout) findViewById(R.id.addmore_layout1);

        try {
            Title.setText(getIntent().getStringExtra("title"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        fav = (ImageView) findViewById(R.id.fav);

        mGridView = (GridView) findViewById(R.id.gridview);
        mGridView1 = (GridView) findViewById(R.id.gridview1);
        mBoxGridView = (GridView) findViewById(R.id.grid_layout_box);

        pcs_layout = (LinearLayout) findViewById(R.id.pcs_layout);

        if (sections.size() == 1) {
            position = 0;
            int pos = 0;

            if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);
            }

            individual.setText(sections.get(0).getSectionname());

            itemQty.clear();
            try {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                PastriesSelection.orderPrice.setText("0.00 SR");
                PastriesSelection.orderQuantity.setText("0");
                PastriesSelection.mcount_basket.setText("0");
            } catch (Exception e) {
                e.printStackTrace();
            }
            individual.performClick();
            individual.setVisibility(View.VISIBLE);
            box.setVisibility(View.GONE);
            mini.setVisibility(View.GONE);
            petite.setVisibility(View.GONE);
            mindividual_list_layout.setVisibility(View.VISIBLE);
            mbox_list_layout.setVisibility(View.GONE);
            mmini_list_layout.setVisibility(View.GONE);
            mpetite_list_layout.setVisibility(View.GONE);

        } else if (sections.size() == 2) {

            position = 1;
            pos = 0;

//            for (int i = 0; i < pos; i++) {
            if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            }

            individual.setText(sections.get(0).getSectionname());
            box.setText(sections.get(1).getSectionname());
            itemQty.clear();
            try {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                PastriesSelection.orderPrice.setText("0.00 SR");
                PastriesSelection.orderQuantity.setText("0");
                PastriesSelection.mcount_basket.setText("0");
            } catch (Exception e) {
                e.printStackTrace();
            }
            individual.performClick();
            individual.setVisibility(View.VISIBLE);
            box.setVisibility(View.VISIBLE);
            mini.setVisibility(View.GONE);
            petite.setVisibility(View.GONE);
            mindividual_list_layout.setVisibility(View.VISIBLE);
            mbox_list_layout.setVisibility(View.VISIBLE);
            mmini_list_layout.setVisibility(View.GONE);
            mpetite_list_layout.setVisibility(View.GONE);

        } else if (sections.size() == 3) {

            position = 2;
            int pos = 0;
//            int value = ;
//            for (int i = 0; i < pos; i++) {
//                pos++;
            if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            }
            individual.setText(sections.get(0).getSectionname());
            box.setText(sections.get(1).getSectionname());
            mini.setText(sections.get(2).getSectionname());
            itemQty.clear();
            try {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                PastriesSelection.orderPrice.setText("0.00 SR");
                PastriesSelection.orderQuantity.setText("0");
                PastriesSelection.mcount_basket.setText("0");
            } catch (Exception e) {
                e.printStackTrace();
            }
            individual.performClick();
            individual.setVisibility(View.VISIBLE);
            box.setVisibility(View.VISIBLE);
            mini.setVisibility(View.VISIBLE);
            petite.setVisibility(View.GONE);
            mindividual_list_layout.setVisibility(View.VISIBLE);
            mbox_list_layout.setVisibility(View.VISIBLE);
            mmini_list_layout.setVisibility(View.VISIBLE);
            mpetite_list_layout.setVisibility(View.GONE);

        } else if (sections.size() == 4) {

            position = 3;
            int pos = 0;

//            for (int i = 0; i < pos; i++) {
            if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);


            } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(3).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(3).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            }

            individual.setText(sections.get(0).getSectionname());
            box.setText(sections.get(1).getSectionname());
            mini.setText(sections.get(2).getSectionname());
            petite.setText(sections.get(3).getSectionname());
            itemQty.clear();
            try {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                PastriesSelection.orderPrice.setText("0.00 SR");
                PastriesSelection.orderQuantity.setText("0");
                PastriesSelection.mcount_basket.setText("0");
            } catch (Exception e) {
                e.printStackTrace();
            }
            individual.performClick();
            individual.setVisibility(View.VISIBLE);
            box.setVisibility(View.VISIBLE);
            mini.setVisibility(View.VISIBLE);
            petite.setVisibility(View.VISIBLE);
            mindividual_list_layout.setVisibility(View.VISIBLE);
            mbox_list_layout.setVisibility(View.VISIBLE);
            mmini_list_layout.setVisibility(View.VISIBLE);
            mpetite_list_layout.setVisibility(View.VISIBLE);
        }

        mindividual_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                value1 = i;

                subitemQty.clear();
                subitemfinalQty.clear();
                subitemfinalprice.clear();

                if (sections.size() >= 1) {

                    position = 0;

                    boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("2")) {

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        position = 0;

                        boxName.setText(sections.get(0).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(0).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();
                        boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);
                    }
                }
                mindividual_list.setVisibility(View.GONE);

            }
        });

        mbox_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                subitemQty.clear();
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                if (sections.size() == 1) {

                    position = 0;

                    value1 = i;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 0;

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(0).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(0).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }


                } else if (sections.size() >= 2) {

                    position = 1;

                    value1 = i;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(1).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 1;

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(1).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(1).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(1).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }


                }
                mbox_list.setVisibility(View.GONE);
            }
        });

        mmini_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                subitemQty.clear();
                subitemfinalQty.clear();
                subitemfinalprice.clear();

                if (sections.size() == 1) {

                    position = 0;

                    value1 = i;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 0;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(0).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(0).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);
                    }

                } else if (sections.size() == 2) {

                    position = 1;

                    value1 = i;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(1).getChildItems().get(value1).getBuildingType().equals("2")) {
                        position = 1;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(1).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(1).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(1).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                } else if (sections.size() >= 3) {

                    position = 2;

                    value1 = i;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(2).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 2;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(2).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(2).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(2).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                }
                mmini_list.setVisibility(View.GONE);
            }
        });

        mpetite_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                subitemQty.clear();
                subitemfinalQty.clear();
                subitemfinalprice.clear();

                if (sections.size() == 1) {

                    position = 0;

                    value1 = i;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("1")) {

                    } else if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 0;

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(0).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(0).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                } else if (sections.size() == 2) {

                    position = 1;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(1).getChildItems().get(value1).getBuildingType().equals("1")) {

                    } else if (sections.get(1).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 1;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(1).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(1).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(1).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                } else if (sections.size() == 3) {

                    position = 2;

                    value1 = i;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(2).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 2;

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(2).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(2).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(2).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                } else if (sections.size() == 4) {

                    position = 3;

                    value1 = i;

                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter = new PastriesSelectionAdapter(getApplicationContext(), sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mAdapter);

                    if (sections.get(3).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 3;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())) + " SR");
                        Qty = Qty + 1;
                        orderQuantity.setText("" + Qty);
                        boxName.setText(sections.get(3).getChildItems().get(value1).getItemName());

                        no_of_items = sections.get(3).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(3).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();
                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }
                }
                mpetite_list.setVisibility(View.GONE);
            }
        });

        individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
                if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }

            }
        });

        box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();

                if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }

            }
        });

        mini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();

                if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }

            }
        });

        petite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();

                if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }

            }
        });

        maddmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemfinalQty.size() == 0) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    HashMap<String, String> values = new HashMap<>();
                    values.put("itemId", sections.get(position).getChildItems().get(value1).getItemId());
                    values.put("itemTypeId", sections.get(position).getChildItems().get(value1).getPriceList().get(0).getSize());
                    values.put("subCategoryId", sections.get(position).getChildItems().get(value1).getSubCatId());
                    values.put("additionals", "");
                    values.put("qty", String.valueOf(SectionItemAdapter.Qty));
                    values.put("price", sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());
                    values.put("additionalsPrice", "");
                    values.put("additionalsTypeId", "");
                    values.put("totalAmount", String.valueOf(SectionItemAdapter.orderPrice));
                    values.put("comment", "");
                    values.put("status", "1");
                    values.put("creationDate", "14/07/2015");
                    values.put("modifiedDate", "14/07/2015");
                    values.put("categoryId", "");
                    values.put("itemName", sections.get(position).getChildItems().get(value1).getItemName());
                    values.put("itemNameAr", sections.get(position).getChildItems().get(value1).getItemNameAr());
                    values.put("image", sections.get(position).getChildItems().get(value1).getImages());
                    values.put("item_desc", sections.get(position).getChildItems().get(value1).getDescription());
                    values.put("item_desc_Ar", sections.get(position).getChildItems().get(value1).getDescriptionAr());
                    values.put("sub_itemName", "");
                    values.put("sub_itemName_Ar", "");
                    values.put("sub_itemImage", "");

                    myDbHelper.insertOrder(values);

                    Intent a = new Intent(PastriesSelection.this, CategoriesListActivity.class);
                    startActivity(a);
                    finish();
                }

            }
        });

        maddmore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (subitemfinalQty.size() == 0) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                } else if (totalCount < boxSize) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "You are select with " + PastriesSelection.totalCount + "/" + PastriesSelection.boxSize + PastriesSelection.title + " per Box, to order more use other box.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    HashMap<String, String> values = new HashMap<>();
                    values.put("itemId", sections.get(position).getChildItems().get(value1).getItemId());
                    values.put("itemTypeId", sections.get(position).getChildItems().get(value1).getPriceList().get(0).getSize());
                    values.put("subCategoryId", sections.get(position).getChildItems().get(value1).getSubCatId());
                    values.put("additionals", "");
                    values.put("qty", String.valueOf(Qty));
                    values.put("price", sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());
                    values.put("additionalsPrice", "");
                    values.put("additionalsTypeId", "");
                    values.put("totalAmount", String.valueOf(orderprice));
                    values.put("comment", "");
                    values.put("status", "1");
                    values.put("creationDate", "14/07/2015");
                    values.put("modifiedDate", "14/07/2015");
                    values.put("categoryId", "");
                    values.put("itemName", sections.get(position).getChildItems().get(value1).getItemName());
                    values.put("itemNameAr", sections.get(position).getChildItems().get(value1).getItemNameAr());
                    values.put("image", sections.get(position).getChildItems().get(value1).getImages());
                    values.put("item_desc", sections.get(position).getChildItems().get(value1).getDescription());
                    values.put("item_desc_Ar", sections.get(position).getChildItems().get(value1).getDescriptionAr());
                    values.put("sub_itemName", "");
                    values.put("sub_itemName_Ar", "");
                    values.put("sub_itemImage", "");

                    myDbHelper.insertOrder(values);

                    Intent a = new Intent(PastriesSelection.this, CategoriesListActivity.class);
                    startActivity(a);
                    finish();
                }

            }
        });

        qty_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (subitemfinalQty.size() == 0) {

                    orderprice = 0;
                    Qty = 0;

                    Qty = Qty + 1;
                    orderprice = Integer.parseInt(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice()) * Qty;

                    subitemfinalQty.add(String.valueOf(Qty));
                    subitemfinalprice.add(String.valueOf(orderprice));

                    try {
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderprice) + " SR");
                        PastriesSelection.orderQuantity.setText("" + Qty);
                        PastriesSelection.mcount_basket.setText("" + Qty);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    Qty = Qty + 1;
                    orderprice = Integer.parseInt(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice()) * Qty;

                    subitemfinalQty.add(String.valueOf(Qty));
                    subitemfinalprice.add(String.valueOf(orderprice));

                    try {
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderprice) + " SR");
                        PastriesSelection.orderQuantity.setText("" + Qty);
                        PastriesSelection.mcount_basket.setText("" + Qty);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        qty_mins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (subitemfinalQty.size() == 0) {

                    if (Qty > 0) {

                        orderprice = 0;
                        Qty = 0;

                        Qty = Qty - 1;
                        orderprice = orderprice - Integer.parseInt(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());

                        orderQuantity.setText("" + Qty);

                        try {
                            double number;
                            number = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim = new DecimalFormat("0.00");
                            OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
                            OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            double number1;
                            number1 = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim1 = new DecimalFormat("0.00");
                            CategoriesListActivity.orderPrice.setText("" + decim1.format(number) + " SR");
                            CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        try {
                            DecimalFormat decimalFormat = new DecimalFormat("0.00");
                            PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderprice) + " SR");
                            PastriesSelection.orderQuantity.setText("" + Qty);
                            PastriesSelection.mcount_basket.setText("" + Qty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (Qty > 0) {

                        Qty = Qty - 1;
                        orderprice = orderprice - Integer.parseInt(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());

                        orderQuantity.setText("" + Qty);

                        try {
                            double number;
                            number = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim = new DecimalFormat("0.00");
                            OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
                            OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            double number1;
                            number1 = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim1 = new DecimalFormat("0.00");
                            CategoriesListActivity.orderPrice.setText("" + decim1.format(number) + " SR");
                            CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        try {
                            DecimalFormat decimalFormat = new DecimalFormat("0.00");
                            PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderprice) + " SR");
                            PastriesSelection.orderQuantity.setText("" + Qty);
                            PastriesSelection.mcount_basket.setText("" + Qty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });


//        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
//                ClipData data = ClipData.newPlainText("", "");
//                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
//                view.startDrag(data, shadowBuilder, view, 0);
//                view.setVisibility(View.VISIBLE);
//                return true;
//            }
//        });


        mBoxGridView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    return true;
                }
                return false;
            }
        });

        mBoxGridView.setOnDragListener(this);

        individual.setOnClickListener(this);
        box.setOnClickListener(this);
        mini.setOnClickListener(this);
        petite.setOnClickListener(this);
//        m6PcsBox.setOnClickListener(this);
//        m12PcsBox.setOnClickListener(this);
//        m24PcsBox.setOnClickListener(this);

        try {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            PastriesSelection.orderPrice.setText("0.00 SR");
            PastriesSelection.orderQuantity.setText("0");
            PastriesSelection.mcount_basket.setText("0");
        } catch (Exception e) {
            e.printStackTrace();
        }
        itemQty.clear();
        individual.performClick();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.individual:
                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
                try {
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                    PastriesSelection.orderPrice.setText("0.00 SR");
                    PastriesSelection.orderQuantity.setText("0");
                    PastriesSelection.mcount_basket.setText("0");
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                pcs_layout.setVisibility(View.INVISIBLE);
                individual.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                petite.setBackgroundColor(getResources().getColor(R.color.blue_color));

                try {
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                    PastriesSelection.orderPrice.setText("0.00 SR");
                    PastriesSelection.orderQuantity.setText("0");
                    PastriesSelection.mcount_basket.setText("0");
                } catch (Exception e) {
                    e.printStackTrace();
                }

//
//                if (sections.size() == 1) {
                position = 0;


                pcs_layout.setVisibility(View.VISIBLE);
                mindividual_list.setVisibility(View.VISIBLE);
                mbox_list.setVisibility(View.GONE);
                mmini_list.setVisibility(View.GONE);
                mpetite_list.setVisibility(View.GONE);


                if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                    mbox_layout.setVisibility(View.GONE);
                    qty_plus.setVisibility(View.GONE);
                    qty_mins.setVisibility(View.GONE);
                    mGridView1.setVisibility(View.VISIBLE);
                    mGridView.setVisibility(View.GONE);
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);

                    mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mGridView1.setAdapter(mItemAdapter);

                    Log.e("TAG", "section name " + sections.get(position).getSectionname());

                } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                    mbox_layout.setVisibility(View.VISIBLE);
                    qty_plus.setVisibility(View.VISIBLE);
                    qty_mins.setVisibility(View.VISIBLE);
                    mGridView1.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);

                }

                mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                mindividual_list.setAdapter(mDropDownAdapter);

                mDropDownAdapter.notifyDataSetChanged();
                break;

            case R.id.box:

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
                box.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                petite.setBackgroundColor(getResources().getColor(R.color.blue_color));

                try {
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                    PastriesSelection.orderPrice.setText("0.00 SR");
                    PastriesSelection.orderQuantity.setText("0");
                    PastriesSelection.mcount_basket.setText("0");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (sections.size() == 1) {

                    position = 0;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.VISIBLE);
                    mmini_list.setVisibility(View.GONE);
                    mpetite_list.setVisibility(View.GONE);
//                    mbox_layout.setVisibility(View.VISIBLE);
                    qty_plus.setVisibility(View.VISIBLE);
                    qty_mins.setVisibility(View.VISIBLE);

                    if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }


                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mbox_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();

                } else if (sections.size() >= 2) {

                    position = 1;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.VISIBLE);
                    mmini_list.setVisibility(View.GONE);
                    mpetite_list.setVisibility(View.GONE);

                    if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }

                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mbox_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();
                }
                break;

            case R.id.mini:
                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
//                pcs_layout.setVisibility(View.INVISIBLE);
                mini.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                petite.setBackgroundColor(getResources().getColor(R.color.blue_color));

                try {
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                    PastriesSelection.orderPrice.setText("0.00 SR");
                    PastriesSelection.orderQuantity.setText("0");
                    PastriesSelection.mcount_basket.setText("0");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (sections.size() == 1) {

                    position = 0;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.GONE);
                    mmini_list.setVisibility(View.VISIBLE);
                    mpetite_list.setVisibility(View.GONE);

                    if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }


                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mmini_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();

                } else if (sections.size() == 2) {

                    position = 1;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.GONE);
                    mmini_list.setVisibility(View.VISIBLE);
                    mpetite_list.setVisibility(View.GONE);

                    if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }

                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mmini_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();

                } else if (sections.size() >= 3) {

                    position = 2;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.GONE);
                    mmini_list.setVisibility(View.VISIBLE);
                    mpetite_list.setVisibility(View.GONE);

                    if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }

                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mmini_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();

                }


                break;

            case R.id.petite:
                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
//                pcs_layout.setVisibility(View.INVISIBLE);
                petite.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                box.setBackgroundColor(getResources().getColor(R.color.blue_color));

                try {
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                    PastriesSelection.orderPrice.setText("0.00 SR");
                    PastriesSelection.orderQuantity.setText("0");
                    PastriesSelection.mcount_basket.setText("0");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (sections.size() == 1) {

                    position = 0;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.GONE);
                    mmini_list.setVisibility(View.GONE);
                    mpetite_list.setVisibility(View.VISIBLE);

                    if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }

                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mpetite_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();

                } else if (sections.size() == 2) {

                    position = 1;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.GONE);
                    mmini_list.setVisibility(View.GONE);
                    mpetite_list.setVisibility(View.VISIBLE);

                    if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }

                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mpetite_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();

                } else if (sections.size() == 3) {

                    position = 2;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.GONE);
                    mmini_list.setVisibility(View.GONE);
                    mpetite_list.setVisibility(View.VISIBLE);

                    if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }

                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mpetite_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();

                } else if (sections.size() == 4) {

                    position = 3;

                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.GONE);
                    mbox_list.setVisibility(View.GONE);
                    mmini_list.setVisibility(View.GONE);
                    mpetite_list.setVisibility(View.VISIBLE);

                    if (sections.get(3).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);

                        mItemAdapter = new SectionItemAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mGridView1.setAdapter(mItemAdapter);

                    } else if (sections.get(3).getChildItems().get(0).getBuildingType().equals("2")) {

                        mbox_layout.setVisibility(View.VISIBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView.setVisibility(View.VISIBLE);
                        maddmore.setVisibility(View.GONE);
                        maddmore1.setVisibility(View.VISIBLE);
                    }

                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mpetite_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();
                }
                break;

//            case R.id.pcs_6_box:
//                pcs_layout.setVisibility(View.INVISIBLE);
//                boxName.setText("6pcs Box");
//                boxSize = 6;
//                mNumBox.setText("0/"+boxSize);
//                totalCount = 0;
//                Boxitems.clear();
//                mBoxGridView.setNumColumns(3);
//                mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, PastriesSelection.this);
//                mBoxAdapter.notifyDataSetChanged();
//                break;

//            case R.id.pcs_12_box:
//                pcs_layout.setVisibility(View.INVISIBLE);
//                boxName.setText("12pcs Box");
//                boxSize = 12;
//                mNumBox.setText("0/"+boxSize);
//                Boxitems.clear();
//                totalCount = 0;
//                mBoxGridView.setNumColumns(6);
//                mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, PastriesSelection.this);
//                mBoxAdapter.notifyDataSetChanged();
//                break;
//
//            case R.id.pcs_24_box:
//                pcs_layout.setVisibility(View.INVISIBLE);
//                boxName.setText("24pcs Box");
//                boxSize = 24;
//                mNumBox.setText("0/"+boxSize);
//                totalCount = 0;
//                Boxitems.clear();
//                mBoxGridView.setNumColumns(12);
//                mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, PastriesSelection.this);
//                mBoxAdapter.notifyDataSetChanged();
//                break;
        }
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        final int action = event.getAction();
        // Handles each of the expected events
        switch (action) {

            case DragEvent.ACTION_DRAG_STARTED:

                // Returns false. During the current drag and drop operation, this View will
                // not receive events again until ACTION_DRAG_ENDED is sent.
                return true;

            case DragEvent.ACTION_DRAG_ENTERED:
//                Log.i("TAG","drap entered ACTION_DRAG_ENTERED");

                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
//                Log.i("TAG","drap entered ACTION_DRAG_LOCATION");
                // Ignore the event
                return true;

            case DragEvent.ACTION_DRAG_EXITED:

                return true;

            case DragEvent.ACTION_DROP:
                if (boxSize > totalCount) {
                    selectedItems.add(sections.get(position).getChildItems().get(value1).getSubItems().get(pos));
                    totalCount = totalCount + 1;
                    mNumBox.setText(totalCount + "/" + boxSize);
                    mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                    mBoxGridView.setAdapter(mBoxAdapter);
                    mBoxAdapter.notifyDataSetChanged();
                    mAdapter.notifyDataSetChanged();
                } else {
                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "You are done with Maximum " + boxSize + " " + getIntent().getStringExtra("title") + " per Box, to order more use other box.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
                // Returns true. DragEvent.getResult() will return true.
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                mBoxAdapter.notifyDataSetChanged();
                // returns true; the value is ignored.
                return true;

            // An unknown action type was received.
            default:
                Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                break;
        }
        return false;
    }
//    public void refreshData(){
//        mAdapter.notifyDataSetChanged();
//    }

    public void Noofboxes() {

        if (no_of_items.equals("1")) {

            mBoxGridView.setNumColumns(1);
            rowSize = 1;

        } else if (no_of_items.equals("5")) {

            mBoxGridView.setNumColumns(5);
            rowSize = 1;

        } else if (no_of_items.equals("6")) {

            mBoxGridView.setNumColumns(3);
            rowSize = 2;

        } else if (no_of_items.equals("10")) {

            mBoxGridView.setNumColumns(5);
            rowSize = 2;

        } else if (no_of_items.equals("12")) {

            mBoxGridView.setNumColumns(6);
            rowSize = 2;

        } else if (no_of_items.equals("14")) {

            mBoxGridView.setNumColumns(7);
            rowSize = 2;

        } else if (no_of_items.equals("18")) {

            mBoxGridView.setNumColumns(9);
            rowSize = 2;

        } else if (no_of_items.equals("20")) {

            mBoxGridView.setNumColumns(10);
            rowSize = 2;

        } else if (no_of_items.equals("24")) {

            mBoxGridView.setNumColumns(12);
            rowSize = 2;

        } else if (no_of_items.equals("25")) {

            mBoxGridView.setNumColumns(5);
            rowSize = 5;

        } else if (no_of_items.equals("30")) {

            mBoxGridView.setNumColumns(10);
            rowSize = 3;

        } else if (no_of_items.equals("35")) {

            mBoxGridView.setNumColumns(7);
            rowSize = 5;

        } else if (no_of_items.equals("36")) {

            mBoxGridView.setNumColumns(9);
            rowSize = 4;

        } else if (no_of_items.equals("40")) {

            mBoxGridView.setNumColumns(10);
            rowSize = 4;

        } else if (no_of_items.equals("48")) {

            mBoxGridView.setNumColumns(12);
            rowSize = 4;

        } else if (no_of_items.equals("50")) {

            mBoxGridView.setNumColumns(10);
            rowSize = 5;

        } else if (no_of_items.equals("60")) {

            mBoxGridView.setNumColumns(12);
            rowSize = 5;

        } else if (no_of_items.equals("70")) {

            mBoxGridView.setNumColumns(14);
            rowSize = 5;

        } else if (no_of_items.equals("75")) {

            mBoxGridView.setNumColumns(15);
            rowSize = 5;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (itemfinalQty.size() == 0) {
            orderPrice.setText("0.00 SR");
            orderQuantity.setText("0");
            mcount_basket.setText("0");
        } else if (subitemfinalQty.size() == 0) {
            orderPrice.setText("0.00 SR");
            orderQuantity.setText("0");
            mcount_basket.setText("0");
        }


    }
}
