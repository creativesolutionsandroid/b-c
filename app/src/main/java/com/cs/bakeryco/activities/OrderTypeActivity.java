package com.cs.bakeryco.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.GPSTracker;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.StoreInfo;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 14-09-2016.
 */
public class OrderTypeActivity extends AppCompatActivity {
    private DataBaseHelper myDbHelper;
    RelativeLayout carryOut, delivery;
    Toolbar toolbar;
    SharedPreferences userPrefs;
    String mLoginStatus;
    SharedPreferences languagePrefs;
    String language;

    public static TextView mcount_basket;

    RelativeLayout mcount;

    GPSTracker gps;

    private String timeResponse = null;

    String response;
    int position=0;
    private ArrayList<StoreInfo> storesList = new ArrayList<>();

    Double lat, longi;
    Double dest;
    String serverTime;

    String storeId,starttime,endtime,storeName,storeAddress,imagesURL,is24x7,storename_ar,storeaddress_ar;
    Double latitude,longitudes;

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_type_layout);

        myDbHelper = new DataBaseHelper(OrderTypeActivity.this);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.order_type_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.order_type_layout_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        carryOut = (RelativeLayout) findViewById(R.id.carryout_layout);
        delivery = (RelativeLayout) findViewById(R.id.delivery_layout);

        mcount= (RelativeLayout) findViewById(R.id.count);

        mcount_basket= (TextView) findViewById(R.id.total_count);


        carryOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginStatus = userPrefs.getString("login_status", "");
                Constants.ORDER_TYPE = "Pickup";
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
//                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//                    if (currentapiVersion >= Build.VERSION_CODES.M) {
//                        if (!canAccessLocation()) {
//                            requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
//                        } else {
//                            getGPSCoordinates();
//                        }
//                    } else {
//                        getGPSCoordinates();
//                    }

                    Intent intent = new Intent(OrderTypeActivity.this, OrderConfirmation.class);
                    startActivity(intent);

                }else {
                    Intent intent = new Intent(OrderTypeActivity.this, LoginActivity.class);
                    startActivityForResult(intent, 2);
                }

            }
        });


        delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myDbHelper.getTotalOrderPrice() >= 100) {
                    mLoginStatus = userPrefs.getString("login_status", "");
                    Constants.ORDER_TYPE = "Delivery";
                    if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                        Intent intent = new Intent(OrderTypeActivity.this, AddressActivity.class);
                        intent.putExtra("confirm_order",true);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(OrderTypeActivity.this, LoginActivity.class);
                        startActivityForResult(intent, 2);
                    }
                } else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderTypeActivity.this, android.R.style.Theme_Material_Light_Dialog));


                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Minimum delivery order amount 100 SR\n" +
                                        "\n" +
                                        "Add more items to continue for delivery order")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                        Intent intent = new Intent(OrderTypeActivity.this, OrderFragment.class);
//                intent.putExtra("startWith",1);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("الحد الأدنى للتوصيل بقيمة 50 ريال")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void getGPSCoordinates(){
        gps = new GPSTracker(OrderTypeActivity.this);
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderTypeActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(OrderTypeActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String dayOfWeek;
        @Override
        protected void onPreExecute() {
            storesList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
            dialog = ProgressDialog.show(OrderTypeActivity.this, "",
                    "Fetching stores...");
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]+dayOfWeek);
                Log.e("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            storesList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderTypeActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderTypeActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            JSONArray ja = mainObj.getJSONArray("success");
                            for (int i = 0; i < ja.length(); i++) {
                                StoreInfo si = new StoreInfo();
                                JSONObject jo = ja.getJSONObject(i);
//                                lat = 24.70321657;
//                                longi = 46.68097073;

                                storeId = (jo.getString("storeId"));
                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
                                starttime = (jo.getString("ST"));
                                endtime = (jo.getString("ET"));
                                storeName = (jo.getString("StoreName"));
                                storeAddress = (jo.getString("StoreAddress"));
                                latitude = (jo.getDouble("Latitude"));
                                longitudes = (jo.getDouble("Longitude"));
                                si.setCountryName(jo.getString("CountryName"));
                                si.setCityName(jo.getString("CityName"));
                                imagesURL = (jo.getString("imageURL"));
                                si.setFamilySection(jo.getString("FamilySection"));
                                si.setWifi(jo.getString("Wifi"));
                                si.setPatioSitting(jo.getString("PatioSitting"));
                                si.setDriveThru(jo.getString("DriveThru"));
                                si.setMeetingSpace(jo.getString("MeetingSpace"));
                                si.setHospital(jo.getString("Hospital"));
                                si.setUniversity(jo.getString("University"));
                                si.setOffice(jo.getString("Office"));
                                si.setShoppingMall(jo.getString("ShoppingMall"));
                                si.setStoreName(storeName);
                                si.setStoreAddress(storeAddress);
                                si.setStoreId(storeId);
                                si.setStarttime(starttime);
                                si.setEndtime(endtime);
                                si.setLatitude(latitude);
                                si.setLongitude(longitudes);
                                si.setImageURL(imagesURL);
                                try {
                                    si.setAirport(jo.getString("Airport"));
                                } catch (Exception e) {
                                    si.setAirport("false");
                                }
                                try {
                                    si.setDineIn(jo.getString("DineIn"));
                                } catch (Exception e) {
                                    si.setDineIn("false");
                                }
                                try {
                                    si.setLadies(jo.getString("Ladies"));
                                } catch (Exception e) {
                                    si.setLadies("false");
                                }

                                si.setNeighborhood(jo.getString("Neighborhood"));
                                si.setStoreNumber(jo.getString("phone"));
                                is24x7 = (jo.getString("is24x7"));
                                si.setStatus(jo.getString("status"));
                                si.setOgCountry(jo.getString("OGCountry"));
                                si.setOgCity(jo.getString("OGCity"));
                                storename_ar = (jo.getString("StoreName_ar"));
                                storeaddress_ar = (jo.getString("StoreAddress_ar"));
                                si.setStoreNumber(jo.getString("phone"));
                                si.setStoreName_ar(storename_ar);
                                si.setIs24x7(is24x7);
                                si.setStoreAddress_ar(storeaddress_ar);
                                try {
                                    si.setMessage(jo.getString("Message"));
                                } catch (Exception e) {
                                    si.setMessage("");
                                }

                                try {
                                    si.setMessage_ar(jo.getString("Message_ar"));
                                } catch (Exception e) {
                                    si.setMessage_ar("");
                                }

                                Location me = new Location("");
                                Location dest = new Location("");

                                me.setLatitude(lat);
                                me.setLongitude(longi);

                                dest.setLatitude(jo.getDouble("Latitude"));
                                dest.setLongitude(jo.getDouble("Longitude"));

                                float dist = (me.distanceTo(dest)) / 1000;
                                si.setDistance(dist);


                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                Calendar c = Calendar.getInstance();
                                System.out.println("Current time => " + c.getTime());
                                serverTime = timeResponse;
                                String startTime = si.getStarttime().replace(" ", "");
                                String endTime = si.getEndtime().replace(" ", "");
//                                String startTime = "03:00AM";
//                                String endTime = "11:00PM";
                                if(dist <= 50 && (jo.getBoolean("OnlineOrderStatus"))){

                                    if (startTime.equals("null") && endTime.equals("null")) {
                                        si.setOpenFlag(-1);
                                        storesList.add(si);
                                    } else {

                                        if (endTime.equals("00:00AM")) {
                                            si.setOpenFlag(1);
                                            storesList.add(si);

                                            continue;
                                        } else if (endTime.equals("12:00AM")) {
                                            endTime = "11:59PM";
                                        }

                                        Calendar now = Calendar.getInstance();

                                        int hour = now.get(Calendar.HOUR_OF_DAY);
                                        int minute = now.get(Calendar.MINUTE);


                                        Date serverDate = null;
                                        Date end24Date = null;
                                        Date start24Date = null;
                                        Date current24Date = null;
                                        Date dateToday = null;
                                        Calendar dateStoreClose = Calendar.getInstance();
                                        try {
                                            end24Date = timeFormat.parse(endTime);
                                            start24Date = timeFormat.parse(startTime);
                                            serverDate = dateFormat.parse(serverTime);
                                            dateToday = dateFormat1.parse(serverTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        dateStoreClose.setTime(dateToday);
                                        dateStoreClose.add(Calendar.DATE, 1);
                                        String current24 = timeFormat1.format(serverDate);
                                        String end24 = timeFormat1.format(end24Date);
                                        String start24 = timeFormat1.format(start24Date);
                                        String startDateString = dateFormat1.format(dateToday);
                                        String endDateString = dateFormat1.format(dateToday);
                                        String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                                        dateStoreClose.add(Calendar.DATE, -2);
                                        String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

                                        Date startDate = null;
                                        Date endDate = null;

                                        try {
                                            end24Date = timeFormat1.parse(end24);
                                            start24Date = timeFormat1.parse(start24);
                                            current24Date = timeFormat1.parse(current24);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        String[] parts2 = start24.split(":");
                                        int startHour = Integer.parseInt(parts2[0]);
                                        int startMinute = Integer.parseInt(parts2[1]);

                                        String[] parts = end24.split(":");
                                        int endHour = Integer.parseInt(parts[0]);
                                        int endMinute = Integer.parseInt(parts[1]);

                                        String[] parts1 = current24.split(":");
                                        int currentHour = Integer.parseInt(parts1[0]);
                                        int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


                                        if (startTime.contains("AM") && endTime.contains("AM")) {
                                            if (startHour < endHour) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                                try {
                                                    startDate = dateFormat2.parse(startDateString);
                                                    endDate = dateFormat2.parse(endDateString);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                            } else if (startHour > endHour) {
                                                if (serverTime.contains("AM")) {
                                                    if (currentHour > endHour) {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        startDateString = endDateYesterday + " " + startTime;
                                                        endDateString = endDateString + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateTomorrow + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        } else if (startTime.contains("AM") && endTime.contains("PM")) {
                                            startDateString = startDateString + " " + startTime;
                                            endDateString = endDateString + "  " + endTime;
                                            try {
                                                startDate = dateFormat2.parse(startDateString);
                                                endDate = dateFormat2.parse(endDateString);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        } else if (startTime.contains("PM") && endTime.contains("AM")) {
                                            if (serverTime.contains("AM")) {
                                                if (currentHour <= endHour) {
                                                    startDateString = endDateYesterday + " " + startTime;
                                                    endDateString = endDateString + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateTomorrow + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            } else {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateTomorrow + "  " + endTime;
                                                try {
                                                    startDate = dateFormat2.parse(startDateString);
                                                    endDate = dateFormat2.parse(endDateString);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                        } else if (startTime.contains("PM") && endTime.contains("PM")) {
                                            startDateString = startDateString + " " + startTime;
                                            endDateString = endDateString + "  " + endTime;
                                            try {
                                                startDate = dateFormat2.parse(startDateString);
                                                endDate = dateFormat2.parse(endDateString);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        }


                                        String serverDateString = dateFormat2.format(serverDate);

                                        try {
                                            serverDate = dateFormat2.parse(serverDateString);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("TAG DATE", "" + startDate);
                                        Log.i("TAG DATE1", "" + endDate);
                                        Log.i("TAG DATE2", "" + serverDate);

                                        if (serverDate.after(startDate) && serverDate.before(endDate)) {
                                            Log.i("TAG Visible", "true");
                                            si.setOpenFlag(1);
                                            storesList.add(si);
                                            carryout();
                                        } else {
                                            si.setOpenFlag(0);
                                            storesList.add(si);


                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderTypeActivity.this, android.R.style.Theme_Material_Light_Dialog));

                                            if (language.equalsIgnoreCase("En")) {
                                                // set title
                                                alertDialogBuilder.setTitle("BAKERY & Co.");

                                                // set dialog message
                                                alertDialogBuilder
                                                        .setMessage("Sorry! We couldn't find any open stores in your location")
                                                        .setCancelable(false)
                                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.dismiss();
                                                            }
                                                        });
                                            } else if (language.equalsIgnoreCase("Ar")) {
                                                // set title
                                                alertDialogBuilder.setTitle("بيكري آند كومباني");

                                                // set dialog message
                                                alertDialogBuilder
                                                        .setMessage("نأسف ! لم نتمكن من إيجاد أي فرع في موقعك الحالي ")
                                                        .setCancelable(false)
                                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.dismiss();
                                                            }
                                                        });
                                            }

                                            // create alert dialog
                                            AlertDialog alertDialog = alertDialogBuilder.create();

                                            // show it
                                            alertDialog.show();
                                        }
                                    }
                                }else {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderTypeActivity.this, android.R.style.Theme_Material_Light_Dialog));

                                    if (language.equalsIgnoreCase("En")) {
                                        // set title
                                        alertDialogBuilder.setTitle("BAKERY & Co.");

                                        // set dialog message
                                        alertDialogBuilder
                                                .setMessage("Sorry! We couldn't find any open stores in your location")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        // set title
                                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                                        // set dialog message
                                        alertDialogBuilder
                                                .setMessage("نأسف ! لم نتمكن من إيجاد أي فرع في موقعك الحالي ")
                                                .setCancelable(false)
                                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                    }

                                    // create alert dialog
                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                    // show it
                                    alertDialog.show();
                                }
//                                storesList.add(si);
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(storesList, StoreInfo.storeOpenSort);
                    }
                }

            }else {
                Toast.makeText(OrderTypeActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
//            mAdapter.notifyDataSetChanged();
//            loading = false;
//            swipeLayout.setRefreshing(false);

//            dest=24.681921,46.700222;
            super.onPostExecute(result);

        }

    }




    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
            dialog = ProgressDialog.show(OrderTypeActivity.this, "",
                    "Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            }else{
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(serverTime == null){
                dialog.dismiss();
            } else if(serverTime.equals("no internet")){
                dialog.dismiss();
                Toast.makeText(OrderTypeActivity.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
//                    timeResponse = "15/02/2017 09:17 AM";
                    new GetStoresInfo().execute(Constants.STORES_URL);

                }catch (JSONException je){
                    je.printStackTrace();
                }
//                new GetStoresInfo().execute(Projecturl.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

//                mAdapter = new SelectStoresAdapter(SelectStoresActivity.this, storesList, lat, longi, timeResponse);
//                listView.setAdapter(mAdapter);
//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

            }


            super.onPostExecute(result1);
        }
    }

    public void carryout() {

//        new GetCurrentTime().execute();

        if (mLoginStatus.equalsIgnoreCase("loggedin")) {

            Intent intent = new Intent(OrderTypeActivity.this, OrderConfirmation.class);
            if (language.equalsIgnoreCase("En")) {
                intent.putExtra("storeName", storeName);
                intent.putExtra("storeAddress", storeAddress);
            } else if (language.equalsIgnoreCase("Ar")) {
                intent.putExtra("storeName_ar", storename_ar);
                intent.putExtra("storeAddress_ar", storeaddress_ar);
            }
            intent.putExtra("storeImage", imagesURL);
            intent.putExtra("storeId", storeId);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitudes);
            intent.putExtra("lat", lat);
            intent.putExtra("longi", longi);
            intent.putExtra("start_time", starttime);
            intent.putExtra("end_time", endtime);
            intent.putExtra("full_hours", is24x7);
            intent.putExtra("order_type", Constants.ORDER_TYPE);
            startActivity(intent);
        } else {
            Intent intent = new Intent(OrderTypeActivity.this, LoginActivity.class);
            startActivityForResult(intent, 2);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderTypeActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("There is no items in your order? To proceed checkout please add the items")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    } else if (language.equalsIgnoreCase("Ar")) {
                    // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderTypeActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("You have "+ myDbHelper.getTotalOrderQty() +" item(s) in bag, by this action all items get clear.")
                            .setCancelable(false)
                            .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent a=new Intent(OrderTypeActivity.this,CheckoutActivity.class);
                                    startActivity(a);
                                }
                            });
                    alertDialogBuilder
                            .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    myDbHelper.deleteOrderTable();
//                                    CheckoutActivity.orderQuantity.setText("0");
//                                    CheckoutActivity.orderPrice.setText("0.00SR");
//                                    CheckoutActivity.mcount_basket.setText("0");
//                                    CategoriesListActivity.orderQuantity.setText("0");
//                                    CategoriesListActivity.orderPrice.setText("0.00SR");
//                                    CategoriesListActivity.mcount_basket.setText("0");
//                                    OrderFragment.orderQuantity.setText("0");
//                                    OrderFragment.orderPrice.setText("0.00SR");
//                                    OrderFragment.mcount_basket.setText("0");
                                    mcount_basket.setText("0");
                                    Intent a=new Intent(OrderTypeActivity.this,OrderFragment.class);
                                    startActivity(a);
                                }
                            });
                } else if (language.equalsIgnoreCase("Ar")) {
//                     set title
                    alertDialogBuilder.setTitle("بيكري آند كومباني");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات"+myDbHelper.getTotalOrderQty()+ "لديك " )
                            .setCancelable(false)
                            .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    myDbHelper.deleteOrderTable();
//                                    CheckoutActivity.orderQuantity.setText("0");
//                                    CheckoutActivity.orderPrice.setText("0.00SR");
//                                    CheckoutActivity.mcount_basket.setText("0");
//                                    CategoriesListActivity.orderQuantity.setText("0");
//                                    CategoriesListActivity.orderPrice.setText("0.00SR");
//                                    CategoriesListActivity.mcount_basket.setText("0");
//                                    OrderFragment.orderQuantity.setText("0");
//                                    OrderFragment.orderPrice.setText("0.00SR");
//                                    OrderFragment.mcount_basket.setText("0");
                                    mcount_basket.setText("0");
                                    Intent a=new Intent(OrderTypeActivity.this,OrderFragment.class);
                                    startActivity(a);
                                }
                            });
                    alertDialogBuilder
                            .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent a=new Intent(OrderTypeActivity.this,CheckoutActivity.class);
                                    startActivity(a);
                                }
                            });
                    }


//                     create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }
        });

        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
    }
}
