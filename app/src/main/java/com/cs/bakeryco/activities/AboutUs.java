package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.JustifiedTextView;


/**
 * Created by CS on 11-07-2016.
 */
public class AboutUs extends AppCompatActivity {

    JustifiedTextView aboutusTxt;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.about_us);
        }else if (language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.about_us_arabic);
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);

        String text = "<HTML><BODY><p align=\\\"justify\\\" style=\"text-align: justify !important;> Bakery & Company began baking confectionary products in low volume bakery facilities. The original handmade pastries, cakes and sandwiches have always used the finest quality. 100 percent natural healthy ingredients: Highest quality flour, Fresh whole grade A eggs, butter creams and dairy products, pure cane sugar, dark and white chocolates, fresh fruit purees and fillings, the world's finest vanilla and essences.</p> <p align=\\\"justify\\\">Today, B&C is a high volume bakery that turns out thousands of products daily. At one point or another, B&C food products are served at the best restaurants and establishments, just waiting to be enjoyed by food enthusiasts and connoisseurs.</p><p align=\\\"justify\\\"> We've come a long way, but there are some things that haven't changed. We will always use the finest quality ingredients and maintain consistency, dedication, and passion to our bakery products for all valued clients.</p></BODY></HTML>";

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        aboutusTxt = (JustifiedTextView) findViewById(R.id.aboutus_txt);

        if (language.equalsIgnoreCase("En")) {
            aboutusTxt.setAlignment(Paint.Align.LEFT);
        }else if (language.equalsIgnoreCase("Ar")) {
            aboutusTxt.setAlignment(Paint.Align.RIGHT);
        }
        aboutusTxt.setTextColor(Color.parseColor("#FFFFFF"));
        aboutusTxt.setLineSpacing(4);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
