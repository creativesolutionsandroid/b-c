package com.cs.bakeryco.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.GPSTracker;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.TrackItemsAdapter;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Rating;
import com.cs.bakeryco.model.RatingDetails;
import com.cs.bakeryco.model.TrackItems;
import com.cs.bakeryco.model.TrackSteps;
import com.cs.bakeryco.model.Track_itemtypes;
import com.cs.bakeryco.model.Track_types;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.cs.bakeryco.R.id.expected_time;
import static com.cs.bakeryco.R.id.expected_time1;
import static com.cs.bakeryco.R.id.order_type;
import static com.cs.bakeryco.R.id.payment_mode;
import static com.cs.bakeryco.R.id.total_items;

//import android.support.annotation.Nullable;

/**
 * Created by CS on 26-09-2016.
 */
public class TrackOrderActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView emptyView;
    TextView title,promoAmount, netAmount;
    TextView totalItems, totalAmount,total_Amount, estTime,estTime1, orderNumbers, paymentMode, orderType ,neworder,orderId_tv;
//    ImageView share;
    ImageView orderImage, acceptImage, readyImage, onthewayImage, deliveryImage;
    TextView orderTime, acceptTime, acceptDesc, acceptText,readyTime, onthewayTime, deliveryTime, expectedTime;
    TextView trackDeliver, trackDeliverText;
    private Timer timer = new Timer();

    public static String driverName, driverNumber, driverId;

    RelativeLayout acceptLayout, readyLayout, onthewayLayout, deliveryLayout ,cancelOrder;
    ScrollView scrollView;
    View acceptLine;

    Double lat, longi;
    String  latitude, longitude, phone, totalPrice;
    String orderNumber, OrderStatus , StoreName, StoreNameAr, OrderType = "", StoreAddress, TrackingTime, ExpectedTime, esttime, address;
    String orderId;
    String mtotal_amt, mtotal_items, mexpected_time, mpayment_mode, morder_type, morder_number;
    SharedPreferences userPrefs;
    String userId;
    SharedPreferences languagePrefs;
    String language;
    LinearLayout trackIcons;

    public static TextView totalQty;

    private ArrayList<TrackItems> ItemsList = new ArrayList<>();
    ArrayList<Track_itemtypes> list = new ArrayList<>();

    TrackItemsAdapter mOrderAdapter;

    ArrayList<TrackSteps> arrayList = new ArrayList<>();

    Date timeformat=null;

    RelativeLayout share,getDirection,call;

    RelativeLayout List_expand,summary_layout;

    Boolean show=true;

    int ratingFromService;
    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel;
    final ArrayList<RatingDetails> rateArray = new ArrayList<>();

    ListView orderListView;

    ImageView plus;


    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    DecimalFormat decim = new DecimalFormat("0.00");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
        setContentView(R.layout.track_order);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.track_order_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        orderId = getIntent().getExtras().getString("orderId");
        emptyView = (TextView) findViewById(R.id.empty_view);
        title = (TextView) findViewById(R.id.header_title);
//        share = (ImageView) findViewById(R.id.track_share);

        scrollView = (ScrollView) findViewById(R.id.scrollView);

        acceptLayout = (RelativeLayout) findViewById(R.id.accept_layout_track);
        readyLayout = (RelativeLayout) findViewById(R.id.ready_layout_track);
        onthewayLayout = (RelativeLayout) findViewById(R.id.ontheway_layout_track);
        deliveryLayout = (RelativeLayout) findViewById(R.id.delivery_layout_track);
        cancelOrder = (RelativeLayout) findViewById(R.id.cancel_order);

        trackIcons = (LinearLayout) findViewById(R.id.track_icons);
        neworder = (TextView) findViewById(R.id.new_order);
        orderTime = (TextView) findViewById(R.id.order_time);
        acceptDesc = (TextView) findViewById(R.id.accept_desc);
        acceptTime = (TextView) findViewById(R.id.accepted_time);
        acceptText = (TextView) findViewById(R.id.accept_text);
        readyTime = (TextView) findViewById(R.id.ready_time);
        onthewayTime = (TextView) findViewById(R.id.ontheway_time);
        deliveryTime = (TextView) findViewById(R.id.delivered_time);
        expectedTime = (TextView) findViewById(expected_time);
        trackDeliver = (TextView) findViewById(R.id.track_deliver);
        trackDeliverText = (TextView) findViewById(R.id.track_delivery_desc);

        share = (RelativeLayout) findViewById(R.id.share_order);
        getDirection = (RelativeLayout) findViewById(R.id.get_direction);
//        cancel = (TextView) findViewById(R.id.cancel_button);
        call= (RelativeLayout) findViewById(R.id.get_direction1);

        totalItems = (TextView) findViewById(total_items);
        total_Amount = (TextView) findViewById(R.id.total_amount);
        estTime = (TextView) findViewById(expected_time1);
//        estTime1= (TextView) findViewById(R.id.expected_time2);
        orderNumbers = (TextView) findViewById(R.id.order_number);
        paymentMode = (TextView) findViewById(payment_mode);
        orderType = (TextView) findViewById(order_type);
        orderId_tv = (TextView) findViewById(R.id.orderId);
        totalAmount = (TextView) findViewById(R.id.totalAmount);
        promoAmount = (TextView) findViewById(R.id.promoAmount);
        netAmount = (TextView) findViewById(R.id.netAmount);
        totalQty = (TextView) findViewById(R.id.totalQty);

        acceptLine = (View) findViewById(R.id.accept_line);

        orderImage = (ImageView) findViewById(R.id.order_image);
        acceptImage = (ImageView) findViewById(R.id.accept_image);
        readyImage = (ImageView) findViewById(R.id.ready_image);
        onthewayImage = (ImageView) findViewById(R.id.ontheway_image);
        deliveryImage = (ImageView) findViewById(R.id.delivery_image);

        List_expand = (RelativeLayout) findViewById(R.id.list_expand);
        orderListView= (ListView) findViewById(R.id.items_list);
        summary_layout= (RelativeLayout) findViewById(R.id.summary_layout);
        plus= (ImageView) findViewById(R.id.list_plus);
        ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBar1);
        cancel = (TextView) findViewById(R.id.skip_rating);
        rate_layout = (RelativeLayout) findViewById(R.id.rate_layout);
        rate_layout.setVisibility(View.GONE);
        List_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(show){
                    orderListView.setVisibility(View.VISIBLE);
                    summary_layout.setVisibility(View.VISIBLE);
                    plus.setImageResource(R.drawable.track_mins);
                    show=false;
                }
                else {
                    orderListView.setVisibility(View.GONE);
                    summary_layout.setVisibility(View.GONE);
                    plus.setImageResource(R.drawable.track_plus);
                    show=true;
                }
            }
        });

        summary_layout.setVisibility(View.GONE);
        orderListView.setVisibility(View.GONE);

        mOrderAdapter = new TrackItemsAdapter(TrackOrderActivity.this, list, language);
        orderListView.setAdapter(mOrderAdapter);

//        mtotal_amt = getIntent().getExtras().getString("total_amt");
//        mtotal_items = getIntent().getExtras().getString("total_items");
//        mexpected_time = getIntent().getExtras().getString("expected_time");
//        mpayment_mode = getIntent().getExtras().getString("payment_mode");
//        morder_type = getIntent().getExtras().getString("order_type");
//        morder_number = getIntent().getExtras().getString("order_number");

        neworder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a=new Intent(TrackOrderActivity.this, OrderFragment.class);
                startActivity(a);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessLocation()) {
                        requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                    } else {
                        getGPSCoordinates();
                    }
                }else {
                    getGPSCoordinates();
                }

            }
        });

        getDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+ latitude + "," + longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phone));
                        if (ActivityCompat.checkSelfPermission(TrackOrderActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phone));
                    startActivity(intent);
                }
            }
        });


        if(language.equalsIgnoreCase("En")){
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("تابع الطلب");
            emptyView.setText("لا يوجد منتجات في السلة");
        }


        driverName = "";
        driverNumber = "";
        driverId = "";
        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL+orderId+"&userId="+userId);

        Log.e("TAG","orderid"+orderId);
        Log.e("TAG","userid"+userId);

        Log.e("TAG",orderId);
        Log.e("TAG",userId);


        timer.schedule(new MyTimerTask(), 30000, 30000);


//        share.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog();
//            }
//        });

        cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrderActivity.this, android.R.style.Theme_Material_Light_Dialog));
                String title= "", msg= "", posBtn = "", negBtn = "";
                if(language.equalsIgnoreCase("En")){
                    posBtn = "Yes";
                    negBtn = "No";
                    title = "Backery & Co.";
                    msg = "Do you want to cancel the order?";
                }
                else if(language.equalsIgnoreCase("Ar")){
                    posBtn = "نعم";
                    negBtn = "لا";
                    title = "بيكري آند كومباني";
                    msg = "هل ترغب في إلغاء الطلب ؟";
                }
                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                JSONObject parent = new JSONObject();
                                try {

                                    JSONArray mainItem = new JSONArray();

                                    JSONObject mainObj = new JSONObject();
                                    mainObj.put("OrderId", orderId);
                                    mainObj.put("UserId", userId);
                                    mainObj.put("Device_token", SplashActivity.regid);

                                    mainItem.put(mainObj);

                                    parent.put("CancelOrder", mainItem);
                                }catch (JSONException je){
                                    je.printStackTrace();
                                }
                                new CancelOrder().execute(parent.toString());
                            }
                        }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL+orderId+"&userId="+userId);
                }
            });
        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        timer.cancel();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class GetOrderTrackDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
            dialog = ProgressDialog.show(TrackOrderActivity.this, "",
                    "Reloading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(TrackOrderActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(TrackOrderActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String failure = jo.getString("Failure");
                                scrollView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            } catch (JSONException jse) {

                                try {
                                    JSONArray ja = jo.getJSONArray("Received");
                                    cancelOrder.setEnabled(true);
                                    cancelOrder.setClickable(true);
                                    cancelOrder.setAlpha(1f);

                                    TrackSteps ts = new TrackSteps();
                                    JSONObject jo1 = ja.getJSONObject(0);
                                    orderNumber = jo1.getString("InvoiceNo");
                                    orderId = jo1.getString("OrderId");
                                    OrderStatus = "Order!";
                                    orderId_tv.setText("#"+orderId);
                                    StoreName = jo1.getString("StoreName");
                                    StoreNameAr = jo1.getString("StoreName_ar");
                                    StoreAddress = jo1.getString("StoreAddress");
                                    OrderType = jo1.getString("OrderType");
                                    TrackingTime = jo1.getString("TrackingTime");
                                    ExpectedTime = jo1.getString("ExpectedTime");
                                    latitude = jo1.getString("Latitude");
                                    longitude = jo1.getString("Longitude");
                                    phone = jo1.getString("phone");
                                    totalPrice = jo1.getString("Total_Price");
                                    mtotal_items=jo1.getString("QuantityCount");
//                                    mpayment_mode=jo1.getString("PaymentMode");
                                    float promoAmt = jo1.getInt("PromoAmt");
                                    float total = promoAmt+jo1.getInt("Total_Price");
                                    ratingFromService = jo1.getInt("Rating");
                                    netAmount.setText(""+decim.format(Float.parseFloat(totalPrice)));
                                    promoAmount.setText(""+decim.format(promoAmt));
                                    totalAmount.setText(""+decim.format(total));
                                    totalQty.setText(jo1.getString("QuantityCount"));

                                    if(OrderType.equalsIgnoreCase("Delivery")){
                                        getDirection.setVisibility(View.GONE);
                                    }

                                    ItemsList.clear();
                                    list.clear();
                                    TrackItems ti = new TrackItems();
                                    ArrayList<Track_itemtypes> itemtypesList = new ArrayList<>();
                                    JSONArray itemsArray = jo1.getJSONArray("items");
                                    for(int i=0; i<itemsArray.length(); i++) {
                                        Track_itemtypes items = new Track_itemtypes();
                                        ArrayList<Track_types> itemsList = new ArrayList<>();

                                        JSONObject jo2 = itemsArray.getJSONObject(i);
                                        items.setItem_name(jo2.getString("ItmName"));
                                        items.setItem_name_ar(jo2.getString("ItmName_ar"));
                                        items.setItem_qty(jo2.getString("ItemQty"));
                                        items.setPrice(jo2.getString("price"));

                                        items.setItemtypes(itemsList);
                                        itemtypesList.add(items);
                                        list.add(items);
                                    }
                                    ti.setItems(itemtypesList);
                                    ItemsList.add(ti);

                                    try {
                                        RatingDetails rd = new RatingDetails();
                                        JSONArray jA = jo1.getJSONArray("Ratings");
                                        JSONObject obj = jA.getJSONObject(0);
                                        rd.setGivenrating("5");
                                        JSONArray ratingArray = obj.getJSONArray("5");
                                        ArrayList<Rating> ratingsArrayList = new ArrayList<>();
                                        for (int i = 0; i < ratingArray.length(); i++) {
                                            JSONObject object1 = ratingArray.getJSONObject(i);
                                            Rating rating = new Rating();
                                            rating.setRatingId(object1.getString("RatingId"));
                                            rating.setComment(object1.getString("Comment_En"));
                                            rating.setComment_ar(object1.getString("Comment_Ar"));
                                            rating.setRating(object1.getString("Rating"));
                                            ratingsArrayList.add(rating);

                                        }
                                        rd.setArrayList(ratingsArrayList);
                                        JSONArray ratingArray1 = obj.getJSONArray("4");
                                        rd.setGivenrating("4");
                                        for (int i = 0; i < ratingArray1.length(); i++) {
                                            JSONObject object1 = ratingArray1.getJSONObject(i);
                                            Rating rating = new Rating();
                                            rating.setRatingId(object1.getString("RatingId"));
                                            rating.setComment(object1.getString("Comment_En"));
                                            rating.setComment_ar(object1.getString("Comment_Ar"));
                                            rating.setRating(object1.getString("Rating"));
                                            ratingsArrayList.add(rating);
                                        }
                                        rd.setArrayList(ratingsArrayList);
                                        JSONArray ratingArray2 = obj.getJSONArray("3");
                                        rd.setGivenrating("3");
                                        for (int i = 0; i < ratingArray2.length(); i++) {
                                            JSONObject object1 = ratingArray2.getJSONObject(i);
                                            Rating rating = new Rating();
                                            rating.setRatingId(object1.getString("RatingId"));
                                            rating.setComment(object1.getString("Comment_En"));
                                            rating.setComment_ar(object1.getString("Comment_Ar"));
                                            rating.setRating(object1.getString("Rating"));
                                            ratingsArrayList.add(rating);
                                        }
                                        rd.setArrayList(ratingsArrayList);

                                        JSONArray ratingArray3 = obj.getJSONArray("2");
                                        rd.setGivenrating("2");
                                        for (int i = 0; i < ratingArray3.length(); i++) {
                                            JSONObject object1 = ratingArray3.getJSONObject(i);
                                            Rating rating = new Rating();
                                            rating.setRatingId(object1.getString("RatingId"));
                                            rating.setComment(object1.getString("Comment_En"));
                                            rating.setComment_ar(object1.getString("Comment_Ar"));
                                            rating.setRating(object1.getString("Rating"));
                                            ratingsArrayList.add(rating);
                                        }
                                        rd.setArrayList(ratingsArrayList);
                                        JSONArray ratingArray4 = obj.getJSONArray("1");
                                        rd.setGivenrating("1");
                                        for (int i = 0; i < ratingArray4.length(); i++) {
                                            JSONObject object1 = ratingArray4.getJSONObject(i);
                                            Rating rating = new Rating();
                                            rating.setRatingId(object1.getString("RatingId"));
                                            rating.setComment(object1.getString("Comment_En"));
                                            rating.setComment_ar(object1.getString("Comment_Ar"));
                                            rating.setRating(object1.getString("Rating"));
                                            ratingsArrayList.add(rating);
                                        }
                                        rd.setArrayList(ratingsArrayList);

                                        rateArray.add(rd);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

//

                                    ts.setOrderNumber(orderNumber);
                                    ts.setStatus(OrderStatus);
                                    ts.setStoreName(StoreName);
                                    ts.setStoreNameAr(StoreNameAr);
                                    ts.setStoreAddress(StoreAddress);
                                    ts.setExpectedTime(ExpectedTime);
                                    ts.setTrackTime(TrackingTime);
                                    ts.setAddress(address);
                                    ts.setOrderId(orderId);


                                    arrayList.add(ts);

                                    String[] order_no = orderNumber.split("-");
                                    orderNumber = order_no[1];
                                    SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                    SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    String date;
                                    Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                    try {
                                        dateObj = curFormater.parse(TrackingTime);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        try {
                                            dateObj = curFormater1.parse(TrackingTime);
                                        } catch (ParseException pe) {

                                        }

                                    }
                                    SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                    date = postFormater.format(dateObj);

                                    SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                                    String expDateString = ExpectedTime;

                                    Log.e("TAG","date & time"+expDateString);

                                    try {
                                        timeformat = timeFormat.parse(expDateString);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    if(timeformat!= null) {
                                        expDateString = formatter.format(timeformat);
                                    }
                                    Log.e("TAG","date & time"+expDateString);

                                    esttime=expDateString;



                                    orderTime.setText(date);
                                    totalItems.setText(mtotal_items);
                                    total_Amount.setText(totalPrice);
                                    estTime.setText(esttime);
                                    orderNumbers.setText(orderNumber);

                                    if(language.equalsIgnoreCase("En")) {
//                                        if (mpayment_mode.equals("2")) {
                                            paymentMode.setText("Cash Payment");
//                                        } else if (mpayment_mode.equals("3")) {
//                                            paymentMode.setText("Online Payment");
//                                        }else if(mpayment_mode.equals("4")){
//                                            paymentMode.setText("Free Drink");
//                                        }
                                        orderType.setText(OrderType);
                                    }else if(language.equalsIgnoreCase("Ar")) {
//                                        if (mpayment_mode.equals("2")) {
                                            paymentMode.setText("نقدي عند استلام الطلب");
//                                        } else if (mpayment_mode.equals("3")) {
//                                            paymentMode.setText("الدفع أونلاين");
//                                        } else if(mpayment_mode.equals("4")){
//                                            paymentMode.setText("مجاني");
//                                        }

                                        if(OrderType.equalsIgnoreCase("Dine-In")){
                                            orderType.setText("داخل الفرع");
                                        }else if(OrderType.equalsIgnoreCase("Pickup")){
                                            orderType.setText("خارج الفرع");
                                        }else if(OrderType.equalsIgnoreCase("Delivery")){
                                            orderType.setText("توصيل");
                                        }
                                    }

                                } catch (JSONException je) {
//                                    scrollView.setVisibility(View.GONE);
//                                    emptyView.setVisibility(View.VISIBLE);
                                }


                                try {
                                    JSONArray ja = jo.getJSONArray("Cancel");
                                    cancelOrder.setVisibility(View.GONE);
                                    JSONObject jo1 = ja.getJSONObject(0);
                                    TrackingTime = jo1.getString("TrackingTime");
                                    SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                    SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    String date;
                                    Date dateObj = null;
                                    try {
                                        dateObj = curFormater.parse(TrackingTime);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        try {
                                            dateObj = curFormater1.parse(TrackingTime);
                                        } catch (ParseException pe) {

                                        }

                                    }
                                    SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                    date = postFormater.format(dateObj);
                                    acceptTime.setText(date);
                                    if (language.equalsIgnoreCase("En")) {
//                                        accepted.setText("Cancel");
                                        acceptDesc.setText("Your Order has been cancelled");
                                        acceptText.setText("Cancel");

                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        acceptText.setText("الغاء");
                                        acceptDesc.setText("طلبك تم الغاءه");
                                    }
                                    acceptImage.setImageDrawable(getResources().getDrawable(R.drawable.cancel_track_order));
                                    acceptLayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    acceptLayout.setAlpha(1.0f);
                                    onthewayLayout.setAlpha(0.15f);
                                    deliveryLayout.setAlpha(0.15f);
                                    readyLayout.setAlpha(0.15f);
                                    acceptLine.setAlpha(0.15f);
                                    trackIcons.setVisibility(View.GONE);
//                                }
                                } catch (JSONException jsone) {

                                    try {
                                        JSONArray ja = jo.getJSONArray("Confirmed");
                                        JSONObject jo1 = ja.getJSONObject(0);
                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setClickable(true);
                                        cancelOrder.setAlpha(1f);
                                        TrackingTime = jo1.getString("TrackingTime");
                                        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                        String date;
                                        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                        try {
                                            dateObj = curFormater.parse(TrackingTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            try {
                                                dateObj = curFormater1.parse(TrackingTime);
                                            } catch (ParseException pe) {

                                            }

                                        }
                                        SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                        date = postFormater.format(dateObj);
//                                        if(OrderType.equalsIgnoreCase("Delivery")){
//                                            expectedTime.setText("Your order is accepted, will be delivered by "+esttime);
//                                        }else {
//                                            trackDeliver.setText("SERVED");
//                                            trackDeliverText.setText("Your order is served");
//                                            expectedTime.setText("Your order is accepted, will be served by "+esttime);
//                                        }

                                        acceptTime.setText(date);
                                        acceptLayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    } catch (JSONException je) {

                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setClickable(true);
                                        cancelOrder.setAlpha(1f);
                                        acceptLayout.setAlpha(0.15f);
                                        readyLayout.setAlpha(0.15f);

                                        if(OrderType.equalsIgnoreCase("Delivery")){
//                                            expectedTime.setText("Your order is accepted, will be delivered by ");
                                            onthewayLayout.setAlpha(0.15f);
                                            deliveryLayout.setAlpha(0.15f);
                                        }else {
                                            trackDeliver.setText("SERVED");
                                            trackDeliverText.setText("Your order is served");
//                                            expectedTime.setText("Your order is accepted, will be served by ");
                                            onthewayLayout.setVisibility(View.GONE);
                                            deliveryLayout.setAlpha(0.15f);
                                        }
                                    }

                                    try {
                                        JSONArray ja = jo.getJSONArray("Ready");
                                        JSONObject jo1 = ja.getJSONObject(0);
                                        cancelOrder.setEnabled(false);
                                        cancelOrder.setClickable(false);
                                        cancelOrder.setAlpha(0.5f);
                                        TrackingTime = jo1.getString("TrackingTime");
                                        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                        String date;
                                        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                        try {
                                            dateObj = curFormater.parse(TrackingTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            try {
                                                dateObj = curFormater1.parse(TrackingTime);
                                            } catch (ParseException pe) {

                                            }

                                        }
                                        SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                        date = postFormater.format(dateObj);
                                        readyTime.setText(date);
                                        readyLayout.setAlpha(1f);
                                    } catch (JSONException je) {
                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setAlpha(1f);
                                        cancelOrder.setClickable(true);
                                        readyLayout.setAlpha(0.15f);
                                        if(OrderType.equalsIgnoreCase("Delivery")){
                                            onthewayLayout.setAlpha(0.15f);
                                            deliveryLayout.setAlpha(0.15f);
                                        }else {
                                            onthewayLayout.setVisibility(View.GONE);
                                            deliveryLayout.setAlpha(0.15f);
                                        }
                                    }

                                    if (OrderType.equalsIgnoreCase("Delivery")) {
                                        onthewayLayout.setVisibility(View.VISIBLE);
                                        try {
                                            JSONArray ja = jo.getJSONArray("OnTheWay");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            JSONArray driverArray = jo1.getJSONArray("DriverName");
                                            JSONObject driverObj = driverArray.getJSONObject(0);
                                            driverName = driverObj.getString("FullName");
                                            driverNumber = driverObj.getString("Mobile");
                                            driverId = jo1.getString("UserId");
                                            TrackingTime = jo1.getString("TrackingTime");
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            onthewayTime.setText(date);
                                            onthewayLayout.setAlpha(1f);

                                            onthewayLayout.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent i = new Intent(TrackOrderActivity.this, LiveTrackingActivity.class);
                                                    i.putExtra("driver_name", driverName);
                                                    i.putExtra("driver_number", driverNumber);
                                                    i.putExtra("driver_id", driverId);
                                                    i.putExtra("order_id", orderId);
                                                    i.putExtra("exp_time", ExpectedTime);
                                                    startActivity(i);
                                                }
                                            });

                                        } catch (JSONException je) {

                                            onthewayLayout.setAlpha(0.15f);
                                            deliveryLayout.setAlpha(0.15f);
                                        }


                                        try {
                                            JSONArray ja = jo.getJSONArray("Delivered");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            TrackSteps ts = new TrackSteps();
                                            TrackingTime = jo1.getString("TrackingTime");
                                            OrderStatus = "Delivered!";

                                            if(ratingFromService == 0) {
                                                rate_layout.setVisibility(View.VISIBLE);
                                                cancel.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                });

                                                ratingBar.setRating(0);
                                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                    @Override
                                                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                        if (fromUser) {
                                                            Intent intent = new Intent(TrackOrderActivity.this, RatingActivity.class);
                                                            intent.putExtra("rating", rating);
                                                            intent.putExtra("array", rateArray);
                                                            intent.putExtra("userid", userId);
                                                            intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                            startActivity(intent);
                                                            rate_layout.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                            }

                                            ts.setOrderNumber(orderNumber);
                                            ts.setStatus(OrderStatus);
                                            ts.setStoreName(StoreName);
                                            ts.setStoreNameAr(StoreNameAr);
                                            ts.setStoreAddress(StoreAddress);
                                            ts.setExpectedTime(ExpectedTime);
                                            ts.setTrackTime(TrackingTime);
                                            ts.setAddress(address);
                                            ts.setOrderId(orderId);


                                            arrayList.add(ts);
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            deliveryTime.setText(date);
                                            deliveryLayout.setAlpha(1f);
                                            onthewayLayout.setClickable(false);
                                        } catch (JSONException je) {
                                            deliveryLayout.setAlpha(0.15f);
                                        }

                                    } else {
                                        onthewayLayout.setVisibility(View.GONE);
                                        try {
                                            JSONArray ja = jo.getJSONArray("Delivered");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            TrackingTime = jo1.getString("TrackingTime");
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            deliveryTime.setText(date);
                                            deliveryLayout.setAlpha(1f);
                                        } catch (JSONException je) {
                                            deliveryLayout.setAlpha(0.15f);
                                        }

                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(TrackOrderActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);

        }

    }



    public void dialog() {

        final Dialog dialog2 = new Dialog(TrackOrderActivity.this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if(language.equalsIgnoreCase("En")){
            dialog2.setContentView(R.layout.track_order_share_dialog);
        }else if(language.equalsIgnoreCase("Ar")){
            dialog2.setContentView(R.layout.track_order_share_dialog);
        }
        dialog2.setCanceledOnTouchOutside(true);
        TextView share = (TextView) dialog2.findViewById(R.id.share_order);
        TextView getDirection = (TextView) dialog2.findViewById(R.id.get_direction);
        TextView cancel = (TextView) dialog2.findViewById(R.id.cancel_button);


        if(OrderType.equalsIgnoreCase("Delivery")){
            getDirection.setVisibility(View.GONE);
        }

        share.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessLocation()) {
                        requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                    } else {
                        getGPSCoordinates();
                    }
                }else {
                    getGPSCoordinates();
                }

            }
        });

        getDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+ latitude + "," + longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });




        dialog2.show();

    }


    public void getGPSCoordinates(){
        gps = new GPSTracker(TrackOrderActivity.this);
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "Bakery & Co. Order Details \n Order No: " + orderNumber + "\n Expected Time: " + ExpectedTime + "\n Store: " + StoreName + "\n Amount: " + totalPrice + "\nhttp://maps.google.com/maps?saddr="+lat+","+longi+"&daddr=" + latitude + "," + longitude);
                ;

                startActivity(Intent.createChooser(intent, "Share"));
                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(TrackOrderActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phone));
                    if (ActivityCompat.checkSelfPermission(TrackOrderActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(TrackOrderActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }
//
//    private boolean hasPermission(String perm) {
//        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(TrackOrderActivity.this, perm));
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//
//        switch (requestCode) {
//
//
//            case LOCATION_REQUEST:
//                if (canAccessLocation()) {
//                    getGPSCoordinates();
//                }
//                else {
//                    Toast.makeText(TrackOrderActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
//                }
//                break;
//        }
//    }

    public class CancelOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
            dialog = ProgressDialog.show(TrackOrderActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.CANCEL_ORDER_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(arg0[0]);

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            return result;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + result);
            }else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrderActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Backery & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    try {
                        JSONObject jo= new JSONObject(result);

                        JSONArray ja = jo.getJSONArray("Success");
                        Toast.makeText(TrackOrderActivity.this,ja.toString().replace("[","").replace("]","").replace("\"",""),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            Log.i("TAG","orderid"+orderId);
            new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL+orderId+"&userId="+userId);
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

}
