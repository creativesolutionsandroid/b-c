package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.CheckoutAdapter;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Order;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 14-09-2016.
 */
public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener{
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;


    public static TextView orderPrice, orderQuantity,mcount_basket;
    ListView listView;
    RelativeLayout ordernowLayout, addMoreLayout;
    Toolbar toolbar;

    RelativeLayout mcount;
    CheckoutAdapter mAdapter;
    SharedPreferences languagePrefs;
    String language;

    TextView totalcount;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.checkout_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.checkout_layout_arabic);
        }

        myDbHelper = new DataBaseHelper(CheckoutActivity.this);

        orderList = myDbHelper.getOrderInfo();

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        orderPrice = (TextView) findViewById(R.id.item_price);
        orderQuantity = (TextView) findViewById(R.id.item_qty);
        addMoreLayout = (RelativeLayout) findViewById(R.id.addmore_layout);
        ordernowLayout = (RelativeLayout) findViewById(R.id.ordernow_layout);
        listView = (ListView) findViewById(R.id.order_info_list);

        mcount= (RelativeLayout) findViewById(R.id.count);
        mcount_basket= (TextView) findViewById(R.id.total_count);

        mAdapter = new CheckoutAdapter(CheckoutActivity.this, orderList, language);
        listView.setAdapter(mAdapter);

        double number;
        number=myDbHelper.getTotalOrderPrice();
        DecimalFormat decim = new DecimalFormat("0.00");
        orderPrice.setText("" + decim.format(number) + " SR");
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
        ordernowLayout.setOnClickListener(this);
        addMoreLayout.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addmore_layout:
                Intent intent = new Intent(CheckoutActivity.this, OrderFragment.class);
//                intent.putExtra("startWith",1);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
//            case R.id.clear_order:
//                AlertDialog.Builder alertDialogBuilder1 = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//
//                if(language.equalsIgnoreCase("En")) {
//                    // set title
//                    alertDialogBuilder1.setTitle("BAKERY & Co.");
//
//                    // set dialog message
//                    alertDialogBuilder1
//                            .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear")
//                            .setCancelable(false)
//                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    myDbHelper.deleteOrderTable();
//                                    try {
//                                        CheckoutActivity.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        OrderFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        CategoryItems.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                    } catch (NullPointerException e) {
//                                        e.printStackTrace();
//                                    }
//                                    dialog.dismiss();
//                                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
//                                    intent.putExtra("startWith",2);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            });
//                }else if(language.equalsIgnoreCase("Ar")){
//                    // set title
//                    alertDialogBuilder1.setTitle("اوريجانو");
//
//                    // set dialog message
//                    alertDialogBuilder1
//                            .setMessage(" منتج في الحقيبة ، من خلال هذا الإجراء ستصبح حقيبتك خالية من المنتجا ت" +myDbHelper.getTotalOrderQty()+"  لديك  ")
//                            .setCancelable(false)
//                            .setNegativeButton("لا", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                            .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    myDbHelper.deleteOrderTable();
//                                    try {
//                                        CheckoutActivity.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        OrderFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        CategoryItems.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                    } catch (NullPointerException e) {
//                                        e.printStackTrace();
//                                    }
//                                    dialog.dismiss();
//                                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
//                                    intent.putExtra("startWith",2);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            });
//                }
//
//
//                // create alert dialog
//                AlertDialog alertDialog1 = alertDialogBuilder1.create();
//
//                // show it
//                alertDialog1.show();
//                break;
            case R.id.ordernow_layout:
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));


                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Intent menuIntent = new Intent(CheckoutActivity.this, OrderTypeActivity.class);
                    startActivity(menuIntent);
                }
                break;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }
//                     create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have "+ myDbHelper.getTotalOrderQty() +" item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        myDbHelper.deleteOrderTable();
//                                        CheckoutActivity.orderQuantity.setText("0");
//                                        CheckoutActivity.orderPrice.setText("0.00SR");
//                                        CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("0");
//                                        CategoriesListActivity.orderPrice.setText("0.00SR");
//                                        CategoriesListActivity.mcount_basket.setText("0");
//                                        OrderFragment.orderQuantity.setText("0");
//                                        OrderFragment.orderPrice.setText("0.00SR");
//                                        OrderFragment.mcount_basket.setText("0");
                                        Intent a=new Intent(CheckoutActivity.this,OrderFragment.class);
                                        startActivity(a);
                                        finish();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات"+myDbHelper.getTotalOrderQty()+ "لديك " )
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        myDbHelper.deleteOrderTable();
//                                        CheckoutActivity.orderQuantity.setText("0");
//                                        CheckoutActivity.orderPrice.setText("0.00SR");
//                                        CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("0");
//                                        CategoriesListActivity.orderPrice.setText("0.00SR");
//                                        CategoriesListActivity.mcount_basket.setText("0");
//                                        OrderFragment.orderQuantity.setText("0");
//                                        OrderFragment.orderPrice.setText("0.00SR");
//                                        OrderFragment.mcount_basket.setText("0");
                                        Intent a=new Intent(CheckoutActivity.this,OrderFragment.class);
                                        startActivity(a);
                                        finish();
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("لا", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }
//

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }
        });

        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
    }
}
