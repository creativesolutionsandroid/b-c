package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.widgets.JustifiedTextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 03-10-2016.
 */
public class CakesInnerActivity extends AppCompatActivity {
    Toolbar toolbar;
    private DataBaseHelper myDbHelper;
    TextView itemName;
    JustifiedTextView itemDesc;
    ImageView itemimage;
    ImageView pieceOfCake, wholeCake,wholeCake1;
    ImageView plusBtn, minusBtn;
    TextView qty, quantity, price, piecePrice, wholePrice;
    RelativeLayout addLayout, wholeCakeLayout, pieceCakeLayout,wholeCakeLayout1;
    View line;

    SharedPreferences languagePrefs;
    private ArrayList<Price> pricesList = new ArrayList<>();
    String language;

    int itemQty;
    String size;
    String itemPrice;
    String finalPrice;
    Float itemPrice1, finalPrice1;

    AlertDialog alertDialog;

    String sItemName, sItemNameAr, sItemId, sCatId, sItemDesc, sSubCatId,sItemImage;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.cakes_inner_screen);
        }else if (language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.cakes_inner_screen_arabic);
        }
        myDbHelper = new DataBaseHelper(CakesInnerActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        itemName = (TextView) findViewById(R.id.item_name);
        itemimage= (ImageView) findViewById(R.id.item_image);
        itemDesc = (JustifiedTextView) findViewById(R.id.item_description);
        pieceOfCake = (ImageView) findViewById(R.id.cake_piece);
        wholeCake = (ImageView) findViewById(R.id.cake_whole);
        wholeCake1 = (ImageView) findViewById(R.id.cake_whole1);
        plusBtn = (ImageView) findViewById(R.id.plus_btn);
        minusBtn = (ImageView) findViewById(R.id.minus_btn);
        qty = (TextView) findViewById(R.id.item_count);
//        quantity = (TextView) findViewById(R.id.item_qty);
        price = (TextView) findViewById(R.id.item_price);
//        piecePrice = (TextView) findViewById(R.id.piece_price);
//        wholePrice = (TextView) findViewById(R.id.whole_price);
        pieceCakeLayout = (RelativeLayout) findViewById(R.id.piece_cake_layout);
        wholeCakeLayout = (RelativeLayout) findViewById(R.id.whole_cake_layout);
        wholeCakeLayout1 = (RelativeLayout) findViewById(R.id.whole_cake_layout1);
        addLayout = (RelativeLayout) findViewById(R.id.add_layout);
        line = findViewById(R.id.cake_line);

        pricesList = (ArrayList<Price>) getIntent().getSerializableExtra(
                "price_list");

        if (language.equalsIgnoreCase("En")){
            itemName.setText(getIntent().getStringExtra("item_name"));
            itemDesc.setText(getIntent().getStringExtra("item_desc"));
            itemDesc.setAlignment(Paint.Align.LEFT);
            itemDesc.setTextColor(Color.parseColor("#B6967D"));
            itemDesc.setLineSpacing(2);
            itemDesc.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        }else if (language.equalsIgnoreCase("Ar")){
            itemName.setText(getIntent().getStringExtra("item_nameAr"));
            itemDesc.setText(getIntent().getStringExtra("item_descAr"));
            itemDesc.setAlignment(Paint.Align.RIGHT);
            itemDesc.setTextColor(Color.parseColor("#B6967D"));
            itemDesc.setLineSpacing(2);
            itemDesc.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        }

        sCatId = getIntent().getStringExtra("catId");
        sItemId = getIntent().getStringExtra("itemId");
        sItemName = getIntent().getStringExtra("item_name");
        sItemNameAr = getIntent().getStringExtra("item_nameAr");
        sSubCatId = getIntent().getStringExtra("subCatId");
        sItemImage=getIntent().getStringExtra("itemImage");

        Glide.with(CakesInnerActivity.this).load("http://csadms.com/bncservices/images/"+sItemImage).into(itemimage);

        itemimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CakesInnerActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.bigimage_popup;
//                if(language.equalsIgnoreCase("En")){
//                    layout = R.layout.bigimage_popup;
//                }else if(language.equalsIgnoreCase("Ar")){
//                    layout = R.layout.bigimage_popup;
//                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);

                ImageView img = (ImageView) dialogView.findViewById(R.id.big_image);

                Glide.with(CakesInnerActivity.this).load("http://csadms.com/bncservices/images/"+sItemImage).into(img);



                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });

        pieceCakeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pieceCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg1));
                wholeCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
                wholeCakeLayout1.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
            }
        });

        wholeCakeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pieceCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
                wholeCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg1));
                wholeCakeLayout1.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
            }
        });

        wholeCakeLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pieceCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
                wholeCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
                wholeCakeLayout1.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg1));
            }
        });

        if(pricesList.size()==1){
            if(pricesList.get(0).getSize().equals("2")){
                pieceCakeLayout.setVisibility(View.GONE);
                line.setVisibility(View.GONE);
            }
        }
//
//        for(int i = 0; i<pricesList.size(); i++){
//            if(pricesList.get(i).getSize().equals("1")){
//                piecePrice.setText(Float.parseFloat(pricesList.get(i).getPrice())+" SR");
//            }else if(pricesList.get(i).getSize().equals("2")){
//                wholePrice.setText(Float.parseFloat(pricesList.get(i).getPrice())+" SR");
//            }
//        }

        pieceOfCake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pieceCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg1));
                wholeCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
                wholeCakeLayout1.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
//                pieceOfCake.setImageResource(R.drawable.cake_piece_selected);
//                wholeCake.setImageResource(R.drawable.cake_whole);
                for(int i = 0; i<pricesList.size(); i++){
                    if(pricesList.get(i).getSize().equals("1")){
                        size = pricesList.get(i).getSize();
                        double cost;
                        cost= Float.parseFloat(pricesList.get(i).getPrice());
//                        NumberFormat nf=new DecimalFormat("####.####");

                        DecimalFormat decim = new DecimalFormat("0.00");
                        itemPrice = decim.format(cost);
                        if(itemQty==0) {
                            itemQty = 1;
                            double cost1;
                            cost1= Double.parseDouble(pricesList.get(i).getPrice());
                            DecimalFormat decim1 = new DecimalFormat("0.00");
                            finalPrice = decim1.format(cost1);
                            qty.setText("1");
                            price.setText(""+decim1.format(cost1));
                        }else{
                            float item_price = 0;
                            double cost2;
                            item_price= Float.parseFloat(itemPrice);
                            cost2= itemQty * item_price;
                            DecimalFormat decim2 = new DecimalFormat("0.00");
                            finalPrice = decim2.format(cost2);
                            qty.setText(""+itemQty);
                            price.setText(""+finalPrice+" SR");
                        }
                    }
                }
            }
        });


        wholeCake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pieceCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
                wholeCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg1));
                wholeCakeLayout1.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
//                pieceOfCake.setImageResource(R.drawable.cake_piece);
//                wholeCake.setImageResource(R.drawable.cake_whole_selected);
                for(int i = 0; i<pricesList.size(); i++){
                    if(pricesList.get(i).getSize().equals("2")){
                        size = pricesList.get(i).getSize();
                        double cost;
                        cost= Double.parseDouble(pricesList.get(i).getPrice());
                        DecimalFormat decim = new DecimalFormat("0.00");
                        itemPrice = decim.format(cost);
                        if(itemQty==0) {
                            itemQty = 1;
                            double cost1;
                            cost1= Double.parseDouble(pricesList.get(i).getPrice());
                            DecimalFormat decim1 = new DecimalFormat("0.00");
                            finalPrice = decim1.format(cost1);
                            qty.setText("1");
                            price.setText(""+decim1.format(cost1));
                        }else{
                            float item_price = 0;
                            double cost2;
                            item_price= Float.parseFloat(itemPrice);
                            cost2=itemQty * item_price;
                            DecimalFormat decim1 = new DecimalFormat("0.00");
                            finalPrice =decim1.format(cost2);
                            qty.setText(""+itemQty);
                            price.setText(""+finalPrice+" SR");
                        }
                    }
                }
            }
        });

        wholeCake1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pieceCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
                wholeCakeLayout.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg));
                wholeCakeLayout1.setBackground(getResources().getDrawable(R.drawable.cake_slices_bg1));
//                pieceOfCake.setImageResource(R.drawable.cake_piece);
//                wholeCake.setImageResource(R.drawable.cake_whole_selected);
                for(int i = 0; i<pricesList.size(); i++){
                    if(pricesList.get(i).getSize().equals("2")){
                        size = pricesList.get(i).getSize();
                        double cost;
                        cost= Double.parseDouble(pricesList.get(i).getPrice());
                        DecimalFormat decim = new DecimalFormat("0.00");
                        itemPrice = decim.format(cost);
                        if(itemQty==0) {
                            itemQty = 1;
                            finalPrice = decim.format(cost);
                            qty.setText("1");
                            price.setText(""+decim.format(cost));
                        }else{
                            float item_price = 0;
                            double cost2;
                            item_price= Float.parseFloat(itemPrice);
                            cost2=itemQty * item_price;
                            DecimalFormat decim1 = new DecimalFormat("0.00");
                            finalPrice = decim1.format(cost2);
                            qty.setText(""+itemQty);
                            price.setText(""+finalPrice+" SR");
                        }
                    }
                }
            }
        });



        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemQty > 0) {
                    itemQty = itemQty + 1;
                    float item_price = 0;
                    item_price= Float.parseFloat(itemPrice);
                    qty.setText("" + itemQty);
                    double cost;
                    cost=itemQty * item_price;
                    DecimalFormat decim1 = new DecimalFormat("0.00");
                    finalPrice =decim1.format(cost);
                    price.setText("" + finalPrice + " SR");
                }else{
                    showDialog();
                }
            }
        });

        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemQty>1) {
                    itemQty = itemQty - 1;
                    float item_price = 0;
                    item_price= Float.parseFloat(itemPrice);
                    finalPrice = String.valueOf(itemQty * item_price);
                    qty.setText("" + itemQty);
                    double cost;
                    cost=itemQty * item_price;
                    DecimalFormat decim1 = new DecimalFormat("0.00");
                    finalPrice =decim1.format(cost);
                    price.setText("" + finalPrice + " SR");
                }else if(itemQty == 0){
                    showDialog();
                }
            }
        });

        addLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemQty == 0){
                    showDialog();
                }else{
                    float item_price = 0;
                    item_price= Float.parseFloat(itemPrice);
                    for(int i = 0; i<pricesList.size(); i++) {
                        itemPrice1 = Float.valueOf(pricesList.get(i).getPrice());
                    }
                    finalPrice = String.valueOf(itemQty * item_price);
                    for(int i = 0; i<pricesList.size(); i++) {
                        finalPrice1 = itemPrice1*itemQty;
                    }

                    if(myDbHelper.getItemOrderCountWithSize(sItemId, size)==0) {
                        HashMap<String, String> values = new HashMap<>();
                        values.put("itemId", sItemId);
                        values.put("itemTypeId", size);
                        values.put("subCategoryId", sSubCatId);
                        values.put("additionals", "");
                        values.put("qty", Integer.toString(itemQty));
                        values.put("price", Float.toString(itemPrice1));
                        values.put("additionalsPrice", "");
                        values.put("additionalsTypeId", "");
                        values.put("totalAmount", Float.toString(finalPrice1));
                        values.put("comment", "");
                        values.put("status", "1");
                        values.put("creationDate", "14/07/2015");
                        values.put("modifiedDate", "14/07/2015");
                        values.put("categoryId", sCatId);
                        values.put("itemName", sItemName);
                        values.put("itemNameAr", sItemNameAr);
                        values.put("image",sItemImage);
                        Log.e("TAG","price cake"+Float.toString(itemPrice1));
                        Log.e("TAG","Qty cake"+itemQty);
                        myDbHelper.insertOrder(values);

                    }else{
                        int cnt = myDbHelper.getItemOrderCountWithSize(sItemId, size);
                        itemQty = itemQty + cnt;
                        float item_price1 = 0;
                        item_price1= Float.parseFloat(itemPrice);
                        finalPrice = String.valueOf(itemQty * item_price1);

                        myDbHelper.updateOrder(String.valueOf(itemQty), String.valueOf(finalPrice), sItemId);
                    }

                    try {

                        double number;
                        number=myDbHelper.getTotalOrderPrice();
                        DecimalFormat decim = new DecimalFormat("0.00");
                        CategoriesListActivity.orderPrice.setText("" + decim.format(number) + " SR");
                        CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    try {
                        double number;
                        number=myDbHelper.getTotalOrderPrice();
                        DecimalFormat decim = new DecimalFormat("0.00");
                        OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    onBackPressed();
                }

            }
        });
    }

    public void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CakesInnerActivity.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
            // set title
            alertDialogBuilder.setTitle("BAKERY & Co.");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Please select an item to use this function")
                    .setCancelable(false)
                    .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.dismiss();
                        }
                    });
//        }else if(language.equalsIgnoreCase("Ar")){
//            // set title
//            alertDialogBuilder.setTitle("اوريجانو");
//
//            // set dialog message
//            alertDialogBuilder
//                    .setMessage("من فضلك إختر منتج على الأقل لإستخدام تلك العملية")
//                    .setCancelable(false)
//                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.dismiss();
//                        }
//                    });
//        }
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
