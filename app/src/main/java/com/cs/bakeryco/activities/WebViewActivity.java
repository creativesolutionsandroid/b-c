package com.cs.bakeryco.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.cs.bakeryco.R;


/**
 * Created by SKT on 03-01-2016.
 */
public class WebViewActivity extends AppCompatActivity {
    TextView doneBtn;
    TextView screenTitle;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_webview);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        doneBtn = (TextView) findViewById(R.id.done_btn);
        screenTitle = (TextView) findViewById(R.id.header_title);
        doneBtn.setVisibility(View.VISIBLE);
        String info  = getIntent().getExtras().getString("webview_toshow");

        String registerTerms ="<HTML><HEAD><style type=\"text/css\" media=\"all\">@import \"dc1.css\";</style><TITLE>My Web Page</TITLE></HEAD><BODY><p><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><u><b>PLEASE READ THE TERMS AND CONDITIONS OF USER AGREEMENT CAREFULLY BEFORE USING THIS APPLICATION.</b></u></span><br><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>1. REGISTRATION POLICY</b></span><p align=\"justify\">At Bakery & Co., we are committed to honoring the privacy of our online clients. We recognize the importance to you of maintaining an appropriate level of privacy and security for personal information we collect from you. We created this private policy to demonstrate our commitment to our business. Your access and use of this app are subjected to this policy.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>COLLECTION AND USE OF PERSONALLY IDENTIFIABLE INFORMATION</b></span><p align=\"justify\">Bakery & Co., uses your information to better understand customer needs and continuously improve the level of service provided. Specifically, your information is used to help complete a transaction, to communicate back to you, to update you on service and benefits. Credit card numbers are used only for payment processing and are not utilized for other purposes.<br><br>We strive to keep your personally identifiable information accurate. Every effort is made to provide you with online access to your registration data so that you may update or correct your information at any time. We are committed to ensuring the security of your information. To prevent unauthorized access, maintain data accuracy, and ensure the appropriate use of information, appropriate procedures are in place to safeguard and secure the information collected online.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>WHO HAS ACCESS TO THIS INFORMATION?</b></span><br> <p align=\"justify\">We may use this information to contact you or to send materials to you for marketing purposes, we will take commercially reasonable steps to safeguard such information from unauthorized access by any other parties. As we strive to meet the needs or our customers, we may disclose personally identifiable information we collect, as described above, to companies and vendors that perform marketing or other services on our behalf or with whom we have joint marketing agreements.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>E-MAIL</b></span><p align=\"justify\">Bakery & Co. policy is not to read or disclose private email communications that are transmitted using our services except to respond, if directed to us, or as required to operate the service, as set forth in the terms of use and policies established from time to time governing the service, or as otherwise required by law.<br><br>We may use your e-mail address to convey marketing-related communications like newsletters and updates regarding new participating merchants, specials or features on the site and/or our mobile phone apps. We may also use this information to contact you for administrative or customer service.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>SECURITY</b></span><p align=\"justify\">All procedural safeguards are designed to protect the confidentiality of personal information provided by you at our mobile app. We regularly test and update our technology to help protect your personal information. However, such precautions do not guarantee that our app site is invulnerable to all security breaches. Bakery & Co. makes no warranty, guarantee, or representation that use of our mobile app is protected from all viruses, security threats or other vulnerabilities and that your information will always be secure.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>REVISIONS</b></span><p align=\"justify\">Because of the evolving nature of the technologies that we use and the way that we conduct business, we reserve the right to revise, amend or modify this policy and our other policies and agreements at any time and in any manner. We will post any revisions, modifications or amendments on this site.</p><br>If you have any questions and suggestions, please contact us at <span style=\"color:blue;\">info@bakery-co.com</span></BODY></HTML>";

           WebView wv = (WebView) findViewById(R.id.webView);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);


        if(info.equalsIgnoreCase("register_terms")){
            screenTitle.setText("Terms and Conditions");
            wv.loadDataWithBaseURL("", registerTerms, "image/html", "UTF-8", "");
        }else if(info.equalsIgnoreCase("card_terms")){
            screenTitle.setText("Terms and Conditions");
//            wv.loadDataWithBaseURL("", cardTerms, "image/html", "UTF-8", "");
        }else if(info.equalsIgnoreCase("desclaimer")){
//            screenTitle.setText("Disclaimer");
//            wv.loadDataWithBaseURL("", disclaimer, "image/html", "UTF-8", "");
        }


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
