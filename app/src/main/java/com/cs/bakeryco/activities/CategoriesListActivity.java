package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
//import com.cs.bakeryco.adapters.ItemsListAdapter;
import com.cs.bakeryco.adapters.SubMenuAdapter;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.MainCategories;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.model.SubCategories;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.bakeryco.R.id.item_price;

/**
 * Created by CS on 22-09-2016.
 */
public class CategoriesListActivity extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<Price> pricesList = new ArrayList<>();
    private ArrayList<SubCategories> subCatList = new ArrayList<>();
    private ArrayList<Order> orderlist = new ArrayList<>();
    String response;
    private DataBaseHelper myDbHelper;

    private int lastExpandedPosition = 0;
    public static TextView orderPrice, orderQuantity ,mcount_basket;

//    public static ItemsListAdapter mAdapter;
    HorizontalScrollView hsv;
    GridView mGridView;
    SubMenuAdapter mSubMenuAdapter;

    int catId , position;
    ArrayList<MainCategories> mainitem =new ArrayList<>();

    RelativeLayout checkOut, addMoreLayout;
    ExpandableListView mExpListView;
    Toolbar toolbar;
    TextView pastry, cake, bread, sandwich, salad, macarons, hotmeals, catering, occation;
    RelativeLayout mcount;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.categories_list);
        }else if (language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.categories_list_arabic);
        }
        myDbHelper = new DataBaseHelper(CategoriesListActivity.this);
        Log.e("TAG",""+getIntent().getExtras().getInt("catId"));
        catId=getIntent().getExtras().getInt("catId");
        mainitem = (ArrayList<MainCategories>) getIntent().getSerializableExtra("mainList");
//        if(catId == 1){
//            pastry.performClick();
//        }else if(catId == 2){
//            cake.performClick();
//        }else if(catId == 3){mainitem
//            hsv.post(new Runnable() {
//                @Override
//                public void run() {
//                    hsv.scrollTo(sandwich.getLeft(), sandwich.getTop());
//                }
//            });
//            sandwich.performClick();
//        }else if(catId == 4){
//            hsv.post(new Runnable() {
//                @Override
//                public void run() {
//                    hsv.scrollTo(macarons.getLeft(), macarons.getTop());
//                }
//            });
//            macarons.performClick();
//        }else if(catId == 6){
//            bread.performClick();
//        }else if(catId == 7){
//            hsv.post(new Runnable() {
//                @Override
//                public void run() {
//                    hsv.scrollTo(hotmeals.getLeft(), hotmeals.getTop());
//                }
//            });
//            hotmeals.performClick();
//        }else if(catId == 9){
//            hsv.post(new Runnable() {
//                @Override
//                public void run() {
//                    hsv.scrollTo(salad.getLeft(), salad.getTop());
//                }
//            });
//            salad.performClick();
//        }else if(catId == 10){
//            hsv.post(new Runnable() {
//                @Override
//                public void run() {
//                    hsv.scrollTo(collections.getLeft(), collections.getTop());
//                }
//            });
//            collections.performClick();
//        }else if(catId == 8){
//            hsv.post(new Runnable() {
//                @Override
//                public void run() {
//                    hsv.scrollTo(catering.getLeft(), catering.getTop());
//                }
//            });
//            catering.performClick();
//        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mExpListView = (ExpandableListView) findViewById(R.id.expandableListView);
        hsv = (HorizontalScrollView) findViewById(R.id.hsv);
        checkOut = (RelativeLayout) findViewById(R.id.checkout_layout);
        addMoreLayout = (RelativeLayout) findViewById(R.id.addmore_layout);
        pastry = (TextView) findViewById(R.id.pastry_text);
        cake = (TextView) findViewById(R.id.cake_text);
        bread = (TextView) findViewById(R.id.bread_text);
        sandwich = (TextView) findViewById(R.id.sandwich_text);
        salad = (TextView) findViewById(R.id.salads_text);
        macarons = (TextView) findViewById(R.id.macarons_text);
        hotmeals = (TextView) findViewById(R.id.hotmeals_text);
//        collections = (TextView) findViewById(R.id.local_colec_text);
        catering = (TextView) findViewById(R.id.catering_text);
        orderPrice = (TextView) findViewById(item_price);
        orderQuantity = (TextView) findViewById(R.id.item_qty);
        mcount_basket= (TextView) findViewById(R.id.total_count);

        mcount= (RelativeLayout) findViewById(R.id.count);
        mGridView = (GridView) findViewById(R.id.gridview);

//        mAdapter = new ItemsListAdapter(CategoriesListActivity.this, subCatList,language);
//        mExpListView.setAdapter(mAdapter);
//
//        mExpListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                if (lastExpandedPosition != -1
//                        && groupPosition != lastExpandedPosition) {
//                    mExpListView.collapseGroup(lastExpandedPosition);
//                }
//                lastExpandedPosition = groupPosition;
////                mExpListView.setSelectionFromTop(groupPosition, 0);
//            }
//        });


        pastry.setOnClickListener(this);
        cake.setOnClickListener(this);
        bread.setOnClickListener(this);
        sandwich.setOnClickListener(this);
        salad.setOnClickListener(this);
        macarons.setOnClickListener(this);
        hotmeals.setOnClickListener(this);
//        collections.setOnClickListener(this);
        catering.setOnClickListener(this);

        if(catId == 1){
            pastry.performClick();
            if (language.equalsIgnoreCase("En")){
            }else if (language.equalsIgnoreCase("Ar")){
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(pastry.getRight(), pastry.getTop());
                    }
                });
            }
        }else if(catId == 2){
            cake.performClick();
            if (language.equalsIgnoreCase("En")){
            }else if (language.equalsIgnoreCase("Ar")){
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(cake.getRight(), cake.getTop());
                    }
                });
            }
        }else if(catId == 3){
            sandwich.performClick();
            if (language.equalsIgnoreCase("En")) {
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(sandwich.getLeft(), sandwich.getTop());
                    }
                });
            }else if (language.equalsIgnoreCase("Ar")){
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(sandwich.getRight(), sandwich.getTop());
                    }
                });
            }

        }else if(catId == 4){
            macarons.performClick();
            if (language.equalsIgnoreCase("En")) {
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(macarons.getLeft(), macarons.getTop());
                    }
                });
            }else if (language.equalsIgnoreCase("Ar")){
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(macarons.getRight(), macarons.getTop());
                    }
                });
            }
        }else if(catId == 6){
            if (mainitem == null){

                Intent a = new Intent(CategoriesListActivity.this,OrderFragment.class);
                startActivity(a);
                finish();
            }else {
                bread.performClick();
                if (language.equalsIgnoreCase("En")) {
                    hsv.post(new Runnable() {
                        @Override
                        public void run() {
                            hsv.scrollTo(bread.getLeft(), bread.getTop());
                        }
                    });
                } else if (language.equalsIgnoreCase("Ar")) {
                    hsv.post(new Runnable() {
                        @Override
                        public void run() {
                            hsv.scrollTo(bread.getRight(), bread.getTop());
                        }
                    });
                }
            }
        }else if(catId == 13){
            hotmeals.performClick();
            if (language.equalsIgnoreCase("En")) {
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(hotmeals.getLeft(), hotmeals.getTop());
                    }
                });
            }else if (language.equalsIgnoreCase("Ar")){
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(hotmeals.getLeft(), hotmeals.getTop());
                    }
                });
            }
        }else if(catId == 14){
            salad.performClick();
            if (language.equalsIgnoreCase("En")) {
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(salad.getLeft(), salad.getTop());
                    }
                });
            }else if (language.equalsIgnoreCase("Ar")){
//                hsv.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        hsv.scrollTo(salad.getRight(), salad.getTop());
//                    }
//                });
            }

        }
//        else if(catId == 10){
//            collections.performClick();
//            if (language.equalsIgnoreCase("En")) {
//                hsv.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        hsv.scrollTo(collections.getLeft(), collections.getTop());
//                    }
//                });
//            }
//            else if (language.equalsIgnoreCase("Ar")){
//                hsv.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        hsv.scrollTo(collections.getRight(), collections.getTop());
//                    }
//                });
//            }

//        }
        else if(catId == 8){
            catering.performClick();
            if (language.equalsIgnoreCase("En")) {
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(catering.getLeft(), catering.getTop());
                    }
                });
            }else if (language.equalsIgnoreCase("Ar")){
//                hsv.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        hsv.scrollTo(catering.getRight(), catering.getTop());
//                    }
//                });
            }
        }

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i,  long l) {

                if(catId == 1) {
                    Intent intent = new Intent(CategoriesListActivity.this, PastriesSelection.class);
                    intent.putExtra("list", mainitem.get(0).getSubCategories().get(i).getSections());
                    intent.putExtra("postion",i);
                    if(language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", mainitem.get(0).getSubCategories().get(i).getSubCat());
                    }
                    else{
                        intent.putExtra("title", mainitem.get(0).getSubCategories().get(i).getSubCatAr());
                    }
                    startActivity(intent);
                }
                else if (catId == 2){
                    Intent intent = new Intent(CategoriesListActivity.this, PastriesSelection.class);
                    intent.putExtra("list", mainitem.get(1).getSubCategories().get(i).getSections());
                    intent.putExtra("postion",i);
                    if(language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", mainitem.get(1).getSubCategories().get(i).getSubCat());
                    }
                    else{
                        intent.putExtra("title", mainitem.get(1).getSubCategories().get(i).getSubCatAr());
                    }
                    startActivity(intent);
                }
                else if(catId == 3){
                    Intent intent = new Intent(CategoriesListActivity.this, PastriesSelection.class);
                    intent.putExtra("list", mainitem.get(2).getSubCategories().get(i).getSections());
                    intent.putExtra("postion",i);
                    if(language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", mainitem.get(2).getSubCategories().get(i).getSubCat());
                    }
                    else{
                        intent.putExtra("title", mainitem.get(2).getSubCategories().get(i).getSubCatAr());
                    }
                    startActivity(intent);
                }
                else if (catId == 4){
                    Intent intent = new Intent(CategoriesListActivity.this, PastriesSelection.class);
                    intent.putExtra("list", mainitem.get(3).getSubCategories().get(i).getSections());
                    intent.putExtra("postion",i);
                    if(language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", mainitem.get(3).getSubCategories().get(i).getSubCat());
                    }
                    else{
                        intent.putExtra("title", mainitem.get(3).getSubCategories().get(i).getSubCatAr());
                    }
                    startActivity(intent);
                }
                else if (catId == 6){

                    Intent intent = new Intent(CategoriesListActivity.this, PastriesSelection.class);
                    intent.putExtra("list", mainitem.get(4).getSubCategories().get(i).getSections());
                    intent.putExtra("postion",i);
                    if(language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", mainitem.get(4).getSubCategories().get(i).getSubCat());
                    }
                    else{
                        intent.putExtra("title", mainitem.get(4).getSubCategories().get(i).getSubCatAr());
                    }
                    startActivity(intent);
                }
                else if (catId == 13){
                    Intent intent = new Intent(CategoriesListActivity.this, PastriesSelection.class);
                    intent.putExtra("list", mainitem.get(5).getSubCategories().get(i).getSections());
                    intent.putExtra("postion",i);
                    if(language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", mainitem.get(5).getSubCategories().get(i).getSubCat());
                    }
                    else{
                        intent.putExtra("title", mainitem.get(5).getSubCategories().get(i).getSubCatAr());
                    }
                    startActivity(intent);
                }
            }
        });

        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CategoriesListActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("There is no items in your order? To proceed checkout please add the items")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Intent checkoutIntent = new Intent(CategoriesListActivity.this, CheckoutActivity.class);
                    startActivity(checkoutIntent);
                }
            }
        });


        addMoreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CategoriesListActivity.this, OrderFragment.class);
//                intent.putExtra("startWith",1);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        double number;
        number=myDbHelper.getTotalOrderPrice();
        DecimalFormat decim = new DecimalFormat("0.00");
        orderPrice.setText("" + decim.format(number) + " SR");
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pastry_text:
                catId = 1;
                mExpListView.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry_selected,0,0);
                pastry.setTextColor(Color.parseColor("#000000"));
                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake,0,0);
                cake.setTextColor(Color.parseColor("#FFFFFF"));
                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread,0,0);
                bread.setTextColor(Color.parseColor("#FFFFFF"));
                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich,0,0);
                sandwich.setTextColor(Color.parseColor("#FFFFFF"));
                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_special_offers,0,0);
                salad.setTextColor(Color.parseColor("#FFFFFF"));
                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons,0,0);
                macarons.setTextColor(Color.parseColor("#FFFFFF"));
                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_our_chef,0,0);
                hotmeals.setTextColor(Color.parseColor("#FFFFFF"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection,0,0);
//                collections.setTextColor(Color.parseColor("#FFFFFF"));
                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering,0,0);
                catering.setTextColor(Color.parseColor("#FFFFFF"));

                position = 0;

                Log.e("TAG","Value" +mainitem.size());
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                mGridView.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();

                break;
            case R.id.cake_text:
                catId = 2;
                mExpListView.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry,0,0);
                pastry.setTextColor(Color.parseColor("#FFFFFF"));
                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake_selected,0,0);
                cake.setTextColor(Color.parseColor("#000000"));
                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread,0,0);
                bread.setTextColor(Color.parseColor("#FFFFFF"));
                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich,0,0);
                sandwich.setTextColor(Color.parseColor("#FFFFFF"));
                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_special_offers,0,0);
                salad.setTextColor(Color.parseColor("#FFFFFF"));
                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons,0,0);
                macarons.setTextColor(Color.parseColor("#FFFFFF"));
                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_our_chef,0,0);
                hotmeals.setTextColor(Color.parseColor("#FFFFFF"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection,0,0);
//                collections.setTextColor(Color.parseColor("#FFFFFF"));
                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering,0,0);
                catering.setTextColor(Color.parseColor("#FFFFFF"));

                position = 1;
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                mGridView.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
                break;

            case R.id.bread_text:
                catId = 6;
                mExpListView.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry,0,0);
                pastry.setTextColor(Color.parseColor("#FFFFFF"));
                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake,0,0);
                cake.setTextColor(Color.parseColor("#FFFFFF"));
                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread_selected,0,0);
                bread.setTextColor(Color.parseColor("#000000"));
                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich,0,0);
                sandwich.setTextColor(Color.parseColor("#FFFFFF"));
                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_special_offers,0,0);
                salad.setTextColor(Color.parseColor("#FFFFFF"));
                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons,0,0);
                macarons.setTextColor(Color.parseColor("#FFFFFF"));
                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_our_chef,0,0);
                hotmeals.setTextColor(Color.parseColor("#FFFFFF"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection,0,0);
//                collections.setTextColor(Color.parseColor("#FFFFFF"));
                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering,0,0);
                catering.setTextColor(Color.parseColor("#FFFFFF"));

                position = 4;
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                mGridView.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
                break;

            case R.id.sandwich_text:
                catId = 3;
                mExpListView.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry,0,0);
                pastry.setTextColor(Color.parseColor("#FFFFFF"));
                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake,0,0);
                cake.setTextColor(Color.parseColor("#FFFFFF"));
                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread,0,0);
                bread.setTextColor(Color.parseColor("#FFFFFF"));
                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich_selected,0,0);
                sandwich.setTextColor(Color.parseColor("#000000"));
                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_special_offers,0,0);
                salad.setTextColor(Color.parseColor("#FFFFFF"));
                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons,0,0);
                macarons.setTextColor(Color.parseColor("#FFFFFF"));
                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_our_chef,0,0);
                hotmeals.setTextColor(Color.parseColor("#FFFFFF"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection,0,0);
//                collections.setTextColor(Color.parseColor("#FFFFFF"));
                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering,0,0);
                catering.setTextColor(Color.parseColor("#FFFFFF"));

                position = 2;
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                mGridView.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
                break;

            case R.id.salads_text:
//                catId = 14;
                mExpListView.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry,0,0);
                pastry.setTextColor(Color.parseColor("#FFFFFF"));
                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake,0,0);
                cake.setTextColor(Color.parseColor("#FFFFFF"));
                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread,0,0);
                bread.setTextColor(Color.parseColor("#FFFFFF"));
                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich,0,0);
                sandwich.setTextColor(Color.parseColor("#FFFFFF"));
                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_special_offers_selected,0,0);
                salad.setTextColor(Color.parseColor("#000000"));
                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons,0,0);
                macarons.setTextColor(Color.parseColor("#FFFFFF"));
                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_our_chef,0,0);
                hotmeals.setTextColor(Color.parseColor("#FFFFFF"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection,0,0);
//                collections.setTextColor(Color.parseColor("#FFFFFF"));
                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering,0,0);
                catering.setTextColor(Color.parseColor("#FFFFFF"));
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                mGridView.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();

                break;

            case R.id.macarons_text:
                catId = 4;
                mExpListView.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry,0,0);
                pastry.setTextColor(Color.parseColor("#FFFFFF"));
                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake,0,0);
                cake.setTextColor(Color.parseColor("#FFFFFF"));
                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread,0,0);
                bread.setTextColor(Color.parseColor("#FFFFFF"));
                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich,0,0);
                sandwich.setTextColor(Color.parseColor("#FFFFFF"));
                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_special_offers,0,0);
                salad.setTextColor(Color.parseColor("#FFFFFF"));
                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons_selected,0,0);
                macarons.setTextColor(Color.parseColor("#000000"));
                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_our_chef,0,0);
                hotmeals.setTextColor(Color.parseColor("#FFFFFF"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection,0,0);
//                collections.setTextColor(Color.parseColor("#FFFFFF"));
                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering,0,0);
                catering.setTextColor(Color.parseColor("#FFFFFF"));

                position = 3;
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                mGridView.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
                break;

            case R.id.hotmeals_text:
                catId = 13;
                mExpListView.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry,0,0);
                pastry.setTextColor(Color.parseColor("#FFFFFF"));
                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake,0,0);
                cake.setTextColor(Color.parseColor("#FFFFFF"));
                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread,0,0);
                bread.setTextColor(Color.parseColor("#FFFFFF"));
                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich,0,0);
                sandwich.setTextColor(Color.parseColor("#FFFFFF"));
                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_special_offers,0,0);
                salad.setTextColor(Color.parseColor("#FFFFFF"));
                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons,0,0);
                macarons.setTextColor(Color.parseColor("#FFFFFF"));
                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_our_chef_selected,0,0);
                hotmeals.setTextColor(Color.parseColor("#000000"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection,0,0);
//                collections.setTextColor(Color.parseColor("#FFFFFF"));
                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering,0,0);
                catering.setTextColor(Color.parseColor("#FFFFFF"));

                position = 5;
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                mGridView.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
                break;

//            case R.id.local_colec_text:
//                catId = 10;
//                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry,0,0);
//                pastry.setTextColor(Color.parseColor("#FFFFFF"));
//                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake,0,0);
//                cake.setTextColor(Color.parseColor("#FFFFFF"));
//                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread,0,0);
//                bread.setTextColor(Color.parseColor("#FFFFFF"));
//                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich,0,0);
//                sandwich.setTextColor(Color.parseColor("#FFFFFF"));
//                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_salads,0,0);
//                salad.setTextColor(Color.parseColor("#FFFFFF"));
//                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons,0,0);
//                macarons.setTextColor(Color.parseColor("#FFFFFF"));
//                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_hotmeals,0,0);
//                hotmeals.setTextColor(Color.parseColor("#FFFFFF"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection_selected,0,0);
//                collections.setTextColor(Color.parseColor("#000000"));
//                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering,0,0);
//                catering.setTextColor(Color.parseColor("#FFFFFF"));
//                new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL+catId);
//                break;

            case R.id.catering_text:
//                catId = 8;
                mExpListView.setVisibility(View.VISIBLE);
                mGridView.setVisibility(View.GONE);
                pastry.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.cat_pastry,0,0);
                pastry.setTextColor(Color.parseColor("#FFFFFF"));
                cake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_cake,0,0);
                cake.setTextColor(Color.parseColor("#FFFFFF"));
                bread.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_bread,0,0);
                bread.setTextColor(Color.parseColor("#FFFFFF"));
                sandwich.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_sandwich,0,0);
                sandwich.setTextColor(Color.parseColor("#FFFFFF"));
                salad.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_special_offers,0,0);
                salad.setTextColor(Color.parseColor("#FFFFFF"));
                macarons.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_macarons,0,0);
                macarons.setTextColor(Color.parseColor("#FFFFFF"));
                hotmeals.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_our_chef,0,0);
                hotmeals.setTextColor(Color.parseColor("#FFFFFF"));
//                collections.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_local_collection,0,0);
//                collections.setTextColor(Color.parseColor("#FFFFFF"));
                catering.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cat_catering_selected,0,0);
                catering.setTextColor(Color.parseColor("#000000"));
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                mGridView.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
//                new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL+catId);
                break;
        }
//        HashMap<String, String> values = new HashMap<>();
//        values.put("itemId", "");
//        values.put("itemTypeId", "");
//        values.put("subCategoryId", "");
//        values.put("additionals", "");
//        values.put("qty", "");
//        values.put("price", "");
//        values.put("additionalsPrice", "");
//        values.put("additionalsTypeId", "");
//        values.put("totalAmount", "");
//        values.put("comment", "");
//        values.put("status", "1");
//        values.put("creationDate", "14/07/2015");
//        values.put("modifiedDate", "14/07/2015");
//        values.put("categoryId", String.valueOf(catId));
//        values.put("itemName", "");
//        values.put("itemNameAr", "");
//        values.put("image","");
//        myDbHelper.insertOrder(values);
////        notify();
//        Log.e("TAG","order list"+catId);
    }


//    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String  networkStatus;
//        ProgressDialog dialog;
//        @Override
//        protected void onPreExecute() {
//            subCatList.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(CategoriesListActivity.this);
//            dialog = ProgressDialog.show(CategoriesListActivity.this, "",
//                    "Loading items...");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            }else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result != null) {
//                if(result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(CategoriesListActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                }else{
//                    if(result.equals("")){
//                        Toast.makeText(CategoriesListActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//                    }else {
//
//                        try {
//                            JSONObject jo = new JSONObject(result);
//                            JSONArray ja = jo.getJSONArray("Success");
//
//                            for(int i = 0; i<ja.length(); i++){
//                                SubCategories cat = new SubCategories();
//                                ArrayList<Items> itemList = new ArrayList<>();
//                                JSONArray ja1 = ja.getJSONArray(i);
//                                for(int k = 0; k < ja1.length(); k++){
//                                    if(k == 0){
//                                        JSONObject mainObj = ja1.getJSONObject(0);
//                                        cat.setCatId(Integer.toString(catId));
//                                        cat.setSubCatId(mainObj.getString("subCatId"));
//                                        cat.setSubCat(mainObj.getString("sName"));
//                                        cat.setSubCatAr(mainObj.getString("sName_Ar"));
//                                        cat.setDescription(mainObj.getString("sDesc"));
//                                        cat.setDescriptionAr(mainObj.getString("sDesc_Ar"));
//                                        cat.setImages(mainObj.getString("Simg"));
//                                    }else{
//                                        JSONArray itemsArry = ja1.getJSONArray(k);
//                                        ArrayList<Price> priceList = new ArrayList<>();
//                                        Items items = new Items();
//                                        for(int l = 0; l<itemsArry.length(); l++){
//
//                                            if(l == 0){
//                                                JSONObject itemsObj = itemsArry.getJSONObject(0);
//                                                items.setCatId(Integer.toString(catId));
//                                                items.setSubCatId(cat.getSubCatId());
//                                                items.setItemId(itemsObj.getString("ItemId"));
//                                                items.setItemName(itemsObj.getString("iName"));
//                                                items.setItemNameAr(itemsObj.getString("iName_Ar"));
//                                                items.setDescription(itemsObj.getString("iDesc"));
//                                                items.setDescriptionAr(itemsObj.getString("iDesc_Ar"));
//                                                items.setImages(itemsObj.getString("Iimg"));
//                                            }else {
//                                                JSONArray priceArray = itemsArry.getJSONArray(l);
//                                                for(int m = 0; m < priceArray.length(); m++){
//                                                    Price price = new Price();
//                                                    JSONObject priceObj = priceArray.getJSONObject(m);
//                                                    price.setPrice(priceObj.getString("price"));
//                                                    price.setPriceId(priceObj.getString("priceId"));
//                                                    price.setSize(priceObj.getString("size"));
//                                                    priceList.add(price);
//                                                }
//                                                items.setPriceList(priceList);
//                                                itemList.add(items);
//                                            }
//
//                                        }
//                                        cat.setChildItems(itemList);
//                                    }
//
//                                }
//
//
//                                subCatList.add(cat);
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }
//
//            }else {
//                Toast.makeText(CategoriesListActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }
//            mAdapter.notifyDataSetChanged();
//

//            int count =  mAdapter.getGroupCount();
//            for (int i = 0; i <count ; i++) {
//                mExpListView.collapseGroup(i);
//            }
//            super.onPostExecute(result);
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();
//        mAdapter.notifyDataSetChanged();
        mSubMenuAdapter.notifyDataSetChanged();

        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CategoriesListActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
//                         set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CategoriesListActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have "+ myDbHelper.getTotalOrderQty() +" item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent a=new Intent(CategoriesListActivity.this,CheckoutActivity.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        myDbHelper.deleteOrderTable();
//                                        CheckoutActivity.orderQuantity.setText("0");
//                                        CheckoutActivity.orderPrice.setText("0SR");
//                                        CheckoutActivity.mcount_basket.setText("0");
                                        CategoriesListActivity.orderQuantity.setText("0");
                                        CategoriesListActivity.orderPrice.setText("0.00SR");
                                        CategoriesListActivity.mcount_basket.setText("0");
//                                        OrderFragment.orderQuantity.setText("0");
//                                        OrderFragment.orderPrice.setText("0.00SR");
//                                        OrderFragment.mcount_basket.setText("0");
                                        Intent a=new Intent(CategoriesListActivity.this,OrderFragment.class);
                                        startActivity(a);
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
//                         set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات"+myDbHelper.getTotalOrderQty()+ "لديك " )
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        myDbHelper.deleteOrderTable();
//                                        CheckoutActivity.orderQuantity.setText("0");
//                                        CheckoutActivity.orderPrice.setText("0SR");
//                                        CheckoutActivity.mcount_basket.setText("0");
                                        CategoriesListActivity.orderQuantity.setText("0");
                                        CategoriesListActivity.orderPrice.setText("0.00SR");
                                        CategoriesListActivity.mcount_basket.setText("0");
//                                        OrderFragment.orderQuantity.setText("0");
//                                        OrderFragment.orderPrice.setText("0.00SR");
//                                        OrderFragment.mcount_basket.setText("0");
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent a=new Intent(CategoriesListActivity.this,CheckoutActivity.class);
                                        startActivity(a);
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }
        });

        double number;
        number=myDbHelper.getTotalOrderPrice();
        DecimalFormat decim = new DecimalFormat("0.00");
        orderPrice.setText("" + decim.format(number) + " SR");
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

    }
}
