package com.cs.bakeryco.activities;

/**
 * Created by CS on 4/30/2017.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgetPassword extends AppCompatActivity {

    private static final int VERIFICATION_REQUEST = 1;
    private String response = null;
    TextView msend;
    EditText mMobile_number;
    String language;
    String password;
    SharedPreferences userPrefs;
    String mLoginStatus,userID;
    SharedPreferences languagePrefs;
    String mobile_number,mobile_number1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")){
            setContentView(R.layout.forget_password);
        }else if (language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.forget_password_arabic);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");
        userID=userPrefs.getString("userId","");

        mMobile_number = (EditText) findViewById(R.id.mobile_number);
        msend = (TextView) findViewById(R.id.send);
        password=getIntent().getExtras().getString("pass");
        mobile_number1=getIntent().getExtras().getString("mobile");
        mobile_number = mMobile_number.getText().toString();
        Log.e("TAG",mobile_number);

        msend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile_number = mMobile_number.getText().toString();
                if (mobile_number.length() == 0) {
                    mMobile_number.setError("Please Enter Phone Number");
                } else {
                    new GetSendOTP().execute(Constants.SEND_OTP +"966"+mobile_number);
                }

            }

        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VERIFICATION_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    public class GetSendOTP extends AsyncTask<String, Integer, String>{
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(ForgetPassword.this);
            dialog = ProgressDialog.show(ForgetPassword.this, "",
                    "Sending E-mail...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ForgetPassword.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(ForgetPassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {

                            JSONObject jo = new JSONObject(result);
                            try {
                                JSONObject ja = jo.getJSONObject("Success");
                                String mobile = ja.getString("MobileNo");
                                String otp = ja.getString("OTP");
                                dialog.dismiss();
                                Intent loginIntent = new Intent(ForgetPassword.this, VerifyRandomNumber.class);
                                loginIntent.putExtra("phone_number", mobile);
                                loginIntent.putExtra("OTP", otp);
                                loginIntent.putExtra("forgot", true);
                                startActivity(loginIntent);
//                                boolean isVerified = jo1.getBoolean("IsVerified");
//                                String familyName = jo1.getString("FamilyName");
//                                String nickName = jo1.getString("NickName");
//                                String gender = jo1.getString("Gender");
//                                if (isVerified) {
//                                    if(mLoginStatus.equalsIgnoreCase("loggedin")){
//                                        setResult(RESULT_OK);
//                                        finish();
//                                    }
////                                        Intent loginIntent = new Intent();
//                                } else {
//                                    Intent loginIntent = new Intent(ForgetPassword.this, Verify_Random_number.class);
//                                    loginIntent.putExtra("phone_number", mobile);
//                                    startActivityForResult(loginIntent, VERIFICATION_REQUEST);
//                                }
                            } catch (JSONException je) {
                                String msg = jo.getString("Failure");
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgetPassword.this);


                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("Broasted Express");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(msg)
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                    onBackPressed();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("بيكري آند كومباني");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(msg)
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }

            } else {
                Toast.makeText(ForgetPassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }

//    public class GetForgetpassword extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String networkStatus;
//        ProgressDialog dialog;
//
//        @Override
//        protected void onPreExecute() {
//            response = null;
//            networkStatus = NetworkUtil.getConnectivityStatusString(ForgetPassword.this);
//            dialog = ProgressDialog.show(ForgetPassword.this, "",
//                    "Sending E-mail...");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            } else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result != null) {
//                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(ForgetPassword.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    if (result.equals("")) {
//                        Toast.makeText(ForgetPassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        try {
//
//                            JSONObject jo = new JSONObject(result);
//                            try {
//                                JSONArray ja = jo.getJSONArray("Success");
//                                JSONObject jo1 = ja.getJSONObject(0);
//                                String userId = jo1.getString("UserId");
//                                String email = jo1.getString("Email");
//                                String fullName = jo1.getString("FullName");
//                                String mobile = jo1.getString("Mobile");
//                                String language = jo1.getString("Language");
//                                boolean isVerified = jo1.getBoolean("IsVerified");
//                                String familyName = jo1.getString("FamilyName");
//                                String nickName = jo1.getString("NickName");
//                                String gender = jo1.getString("Gender");
//                                if (isVerified) {
//                                    if(mLoginStatus.equalsIgnoreCase("loggedin")){
//                                        setResult(RESULT_OK);
//                                        finish();
//                                    }
////                                        Intent loginIntent = new Intent();
//                                } else {
//                                    Intent loginIntent = new Intent(ForgetPassword.this, Verify_Random_number.class);
//                                    loginIntent.putExtra("phone_number", mobile);
//                                    startActivityForResult(loginIntent, VERIFICATION_REQUEST);
//                                }
//                            } catch (JSONException je) {
//                                String msg = jo.getString("Failure");
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgetPassword.this);
//
//
//                                if (language.equalsIgnoreCase("En")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("Broasted Express");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(msg)
//                                            .setCancelable(false)
//                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                    onBackPressed();
//                                                }
//                                            });
//                                } else if (language.equalsIgnoreCase("Ar")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("بروستد إكسبريس");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(msg)
//                                            .setCancelable(false)
//                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//
//                    }
//                }
//
//            } else {
//                Toast.makeText(ForgetPassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }
//
//            super.onPostExecute(result);
//
//        }
//
//    }
}

