package com.cs.bakeryco.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CS on 23-08-2016.
 */
public class LiveTrackingActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
    SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
    SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
    SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);

    Toolbar toolbar;
    TextView driverNameTxt, callNow, expTime, deliveryTime;
    String storePhone, driverPhone;
    Double storeLat, storeLong, userLat, userLong, driverLat, driverLong;
    String driverName, driverNumber, driverId, orderId, expectedTime;

    private String timeResponse = null;

    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private Timer timer = new Timer();

    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
        setContentView(R.layout.live_tracking);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.live_tracking_arabic);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        driverName = getIntent().getStringExtra("driver_name");
        driverNumber = getIntent().getStringExtra("driver_number");
        driverId = getIntent().getStringExtra("driver_id");
        orderId = getIntent().getStringExtra("order_id");
        expectedTime = getIntent().getStringExtra("exp_time");

        callNow = (TextView) findViewById(R.id.call_now);
        deliveryTime = (TextView) findViewById(R.id.delivery_time);
        expTime = (TextView) findViewById(R.id.expected_time);
        driverNameTxt = (TextView) findViewById(R.id.driver_name);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(LiveTrackingActivity.this);
        driverNameTxt.setText(driverName);

        if(language.equalsIgnoreCase("En")){
            expTime.setText("Expected Time : "+expectedTime);
        }else if(language.equalsIgnoreCase("Ar")){
            expTime.setText("الوقت المتوقع  : "+expectedTime);
        }

        callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + driverNumber));
                        if (ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + driverNumber));
                    startActivity(intent);
                }
            }
        });

        timer.schedule(new MyTimerTask(), 60000, 60000);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new getTrackingDetails().execute(Constants.LIVE_TRACKING_URL + orderId);
                }
            });
        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + driverNumber));
                    if (ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                }
                else {
                    Toast.makeText(LiveTrackingActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new getTrackingDetails().execute(Constants.LIVE_TRACKING_URL + orderId);
//        LatLng driver = new LatLng(driverLat, driverLong);
//        LatLng store = new LatLng(storeLat, storeLong);
//        LatLng user = new LatLng(userLat, userLong);
//
//        mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));
//        mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.store_marker)));
//        mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
    }

    public class getTrackingDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog dialog;
        String networkStatus;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);
            dialog = ProgressDialog.show(LiveTrackingActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser.getJSONFromUrl(params[0]);
                Log.i("TAG", "user response: " + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LiveTrackingActivity.this);

                    // set title
                    alertDialogBuilder.setTitle("Bakery & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    try {
                        JSONObject jo = new JSONObject(result);

                        try {
                            JSONArray ja = jo.getJSONArray("Success");
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo1 = ja.getJSONObject(i);
                                storeLat = jo1.getDouble("sLatitude");
                                storeLong = jo1.getDouble("sLongitude");
                                userLat = jo1.getDouble("uLatitude");
                                userLong = jo1.getDouble("uLongitude");
                                driverLat = jo1.getDouble("dLatitude");
                                driverLong = jo1.getDouble("dLongitude");
                                storePhone = jo1.getString("sPhone");
                                driverPhone = jo1.getString("dPhone");
                                if(mMap != null){
                                    LatLng driver = new LatLng(driverLat, driverLong);
                                    LatLng store = new LatLng(storeLat, storeLong);
                                    LatLng user = new LatLng(userLat, userLong);

                                    mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));
                                    mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
                                    mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.store_marker)));
                                    mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
                                    mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
                                }

                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            if(dialog != null) {
                dialog.dismiss();
            }

            new GetCurrentTime().execute();
            super.onPostExecute(result);

        }

    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);
            dialog = ProgressDialog.show(LiveTrackingActivity.this, "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            }else{
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(serverTime == null){
                dialog.dismiss();
            } else if(serverTime.equals("no internet")){
                dialog.dismiss();
                Toast.makeText(LiveTrackingActivity.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
                }catch (JSONException je){
                    je.printStackTrace();
                }
                new getTrafficTime().execute();

            }


            super.onPostExecute(result1);
        }
    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        String distanceResponse;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + userLat +","+ userLong +"&destinations="+ driverLat +","+ driverLong+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LiveTrackingActivity.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        String value = jo3.getString("value");
//                        if(language.equalsIgnoreCase("En")) {
                        if(language.equalsIgnoreCase("En")){
                            deliveryTime.setText("Enjoy your food in : "+secs);
                        }else if(language.equalsIgnoreCase("Ar")){
                            deliveryTime.setText("استمتع بوجبتك في : "+secs);
                        }

                        Date current24Date = null, currentServerDate = null;
                        Date expectedTimeDate = null, expectedTime24 = null;
                        try {
                            current24Date = timeFormat.parse(timeResponse);
                            expectedTime24 = timeFormat2.parse(expectedTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String currentTime =timeFormat1.format(current24Date);
                        String expTimeStr = timeFormat1.format(expectedTime24);
                        try {
                            currentServerDate = timeFormat1.parse(currentTime);
                            expectedTimeDate = timeFormat1.parse(expTimeStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diff = expectedTimeDate.getTime() - currentServerDate.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        int expMins = (int) diffMinutes * 60 * 1000;
                        Log.i("TAG", "mins response: " + expMins);
                        int mins = (Integer.parseInt(value)/60)+1;
                        Calendar now = Calendar.getInstance();
                        now.setTime(currentServerDate);
                        now.add(Calendar.MINUTE, mins);
                        currentServerDate = now.getTime();
                        String CTimeString = timeFormat2.format(currentServerDate);
                        if(language.equalsIgnoreCase("En")){
                            expTime.setText("Expected Time : "+CTimeString);
                        }else if(language.equalsIgnoreCase("Ar")){
                            expTime.setText("الوقت المتوقع  : "+CTimeString);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        new getTrackingDetails().cancel(true);
    }
}
