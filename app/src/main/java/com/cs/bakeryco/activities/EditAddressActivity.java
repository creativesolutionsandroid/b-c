package com.cs.bakeryco.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by CS on 04-08-2016.
 */
public class EditAddressActivity extends AppCompatActivity {
    RelativeLayout addressHome,addressWork, addressOther;
    TextView title, homeTxt, workTxt, otherTxt, updateAddress;
    EditText flatNo, landmark, otherAddress, addressSelected;

    String addressType, response;
    RelativeLayout homeRight, workRight, otherRight;

    String address, latitude, longitude, houseNo, houseName, sLandmark, id;

    SharedPreferences userPrefs;
    String userId;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.save_address);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.save_address_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        address = getIntent().getExtras().getString("address");
        latitude = getIntent().getExtras().getString("latitude");
        longitude = getIntent().getExtras().getString("longitude");
        addressType = getIntent().getExtras().getString("address_type");
        houseName = getIntent().getExtras().getString("house_name");
        houseNo = getIntent().getExtras().getString("house_no");
        sLandmark = getIntent().getExtras().getString("landmark");
        id = getIntent().getExtras().getString("id");
        updateAddress = (TextView) findViewById(R.id.save_address);
        flatNo = (EditText) findViewById(R.id.flat_no);
        landmark = (EditText) findViewById(R.id.landmark);
        otherAddress = (EditText) findViewById(R.id.address_extra);
        addressSelected = (EditText) findViewById(R.id.address);

        addressHome = (RelativeLayout) findViewById(R.id.address_home);
        addressWork = (RelativeLayout) findViewById(R.id.address_work);
        addressOther = (RelativeLayout) findViewById(R.id.other_address);

        homeRight = (RelativeLayout) findViewById(R.id.home);
        workRight = (RelativeLayout) findViewById(R.id.work);
        otherRight = (RelativeLayout) findViewById(R.id.other);

        homeTxt = (TextView) findViewById(R.id.home_txt);
        workTxt = (TextView) findViewById(R.id.work_txt);
        otherTxt = (TextView) findViewById(R.id.other_txt);
        title = (TextView) findViewById(R.id.header_title);

        addressSelected.setText(address);
        flatNo.setText(houseNo);
        otherAddress.setText(houseName);
        landmark.setText(sLandmark);
        updateAddress.setText("Update");
        title.setText("Edit Address");

        if(addressType.equals("1")){
            homeRight.setBackground(getResources().getDrawable(R.drawable.save_selected_bg));
            workRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
            otherRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
            otherAddress.setVisibility(View.VISIBLE);
            if(language.equalsIgnoreCase("En")){
                otherAddress.setHint("Eg. My Home");
            }else if(language.equalsIgnoreCase("Ar")){
                otherAddress.setHint("منزلي");
            }
        }else if(addressType.equals("2")){
            homeRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
            workRight.setBackground(getResources().getDrawable(R.drawable.save_selected_bg));
            otherRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
            otherAddress.setVisibility(View.VISIBLE);
            if(language.equalsIgnoreCase("En")){
                otherAddress.setHint("Eg. My Office");
            }else if(language.equalsIgnoreCase("Ar")){
                otherAddress.setHint("مكتبي");
            }
        }else if(addressType.equals("3")){
//            homeRight.setVisibility(View.GONE);
            homeRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
            workRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
            otherRight.setBackground(getResources().getDrawable(R.drawable.save_selected_bg));
            if(language.equalsIgnoreCase("En")){
                otherAddress.setHint("Eg. Friend Home");
            }else if(language.equalsIgnoreCase("Ar")){
                otherAddress.setHint("منزل صديق");
            }
        }

        addressHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "1";
                homeRight.setBackground(getResources().getDrawable(R.drawable.save_selected_bg));
                workRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
                otherRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
                otherAddress.setVisibility(View.VISIBLE);
                if(language.equalsIgnoreCase("En")){
                    otherAddress.setHint("Eg. My Home");
                }else if(language.equalsIgnoreCase("Ar")){
                    otherAddress.setHint("منزلي");
                }


            }
        });

        addressWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "2";
                homeRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
                workRight.setBackground(getResources().getDrawable(R.drawable.save_selected_bg));
                otherRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
                otherAddress.setVisibility(View.VISIBLE);
                if(language.equalsIgnoreCase("En")){
                    otherAddress.setHint("Eg. My Office");
                }else if(language.equalsIgnoreCase("Ar")){
                    otherAddress.setHint("مكتبي");
                }
            }
        });

        addressOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "3";
                homeRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
                workRight.setBackground(getResources().getDrawable(R.drawable.save_unselected_bg));
                otherRight.setBackground(getResources().getDrawable(R.drawable.save_selected_bg));
                otherAddress.setVisibility(View.VISIBLE);
                if(language.equalsIgnoreCase("En")){
                    otherAddress.setHint("Eg. Friend Home");
                }else if(language.equalsIgnoreCase("Ar")){
                    otherAddress.setHint("منزل صديق");
                }
            }
        });

        updateAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String flatNumber = flatNo.getText().toString();
                String landMark = landmark.getText().toString();
                String address = addressSelected.getText().toString();
                String addressOther = otherAddress.getText().toString();
                if(addressOther.length() == 0){
                    otherAddress.setError("");
                }else{
                    JSONObject parent = new JSONObject();
                    try {
                        JSONArray mainItem = new JSONArray();



                        JSONObject mainObj = new JSONObject();
                        mainObj.put("Id",id);
                        mainObj.put("UserId", userId);
                        mainObj.put("HouseNo", flatNumber);
                        mainObj.put("LandMark", landMark);
                        mainObj.put("Address", address);
                        mainObj.put("AddressType", addressType);
                        mainObj.put("Latitude", latitude);
                        mainObj.put("Longitude", longitude);
                        mainObj.put("IsActive", true);
                        mainObj.put("HouseName", addressOther);
                        mainItem.put(mainObj);



                        parent.put("UserAddress", mainItem);
                        Log.i("TAG", parent.toString());
                    }catch (JSONException je){

                    }
                    new EditAddressDetails().execute(parent.toString());
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class EditAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EditAddressActivity.this);
            dialog = ProgressDialog.show(EditAddressActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.EDIT_ADDRESS_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(EditAddressActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(EditAddressActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            String s = jo.getString("Success");
                            setResult(RESULT_OK);
                            finish();
                            Toast.makeText(EditAddressActivity.this, "Address updated successfully", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            setResult(RESULT_CANCELED);
                            finish();
                        }

                    }
                }

            }else {
                Toast.makeText(EditAddressActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

}
