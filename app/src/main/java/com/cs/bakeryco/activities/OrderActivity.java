package com.cs.bakeryco.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.cs.bakeryco.R;
import com.cs.bakeryco.fragments.OrderFragment;

/**
 * Created by CS on 26-09-2016.
 */
public class OrderActivity extends Fragment {

    private static final int ORDER_HISTORY_REQUEST = 1;
    private static final int FAVORITE_ORDER_REQUEST = 2;
    RelativeLayout mNewOrder, mFavouriteOrder, mOrderHistory;
    View rootView;
    String language;
    SharedPreferences userPrefs;
    String mLoginStatus;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.order_layout_arabic, container,
                    false);
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        mNewOrder = (RelativeLayout) rootView.findViewById(R.id.neworder_layout);
        mFavouriteOrder = (RelativeLayout) rootView.findViewById(R.id.favorder_layout);
        mOrderHistory = (RelativeLayout) rootView.findViewById(R.id.orderhistory_layout);

        mNewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), OrderFragment.class);
                startActivity(i);
            }
        });

        mFavouriteOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), FavoriteOrderActivity.class);
                    startActivity(i);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, FAVORITE_ORDER_REQUEST);
                }

            }
        });

        mOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), OrderHistoryActivity.class);
                    startActivity(i);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, ORDER_HISTORY_REQUEST);
                }

            }
        });
        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLoginStatus = userPrefs.getString("login_status", "");
        if(requestCode == ORDER_HISTORY_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent intent = new Intent(getActivity(), OrderHistoryActivity.class);
            startActivity(intent);
        }else if(requestCode == FAVORITE_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent intent = new Intent(getActivity(), FavoriteOrderActivity.class);
            startActivity(intent);
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
