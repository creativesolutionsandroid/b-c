package com.cs.bakeryco.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CategoriesListActivity;
import com.cs.bakeryco.activities.CheckoutActivity;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.MainCategories;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubCategories;
import com.cs.bakeryco.model.SubItems;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 08-09-2016.
 */
public class OrderFragment extends AppCompatActivity implements View.OnClickListener {
    DataBaseHelper myDbHelper;
    public static TextView orderPrice, orderQuantity, mcount_basket;
    RelativeLayout checkOut ,mcount;
    RelativeLayout pastryCntLayout, cakeCntLayout, breadCntLayout, sandwichCntLayout, saladCntLayout, macaronCntLayout, mealsCntLayout, collcetionCntLayout, cateringCntLayout;
    TextView pastryCnt, cakeCnt, breadCnt, sandwichCnt, saladCnt, macaronCnt, mealsCnt, collcetionsCnt, cateringCnt;
    LinearLayout pastryLayout, cakeLayout, breadLayout, sandwichLayout, saladsLayout, macaronsLayout, hotmealsLayout, collectionsLayout, cateringLayout;
    String language;
    SharedPreferences languagePrefs;
    Toolbar toolbar;

    String response;
    ArrayList<MainCategories> mainCatList =new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs=getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language=languagePrefs.getString("language","En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_fragment);
        }else if (language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.order_fragment_arabic);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myDbHelper = new DataBaseHelper(OrderFragment.this);
        orderPrice = (TextView) findViewById(R.id.item_price);
        orderQuantity = (TextView) findViewById(R.id.item_qty);
        checkOut = (RelativeLayout) findViewById(R.id.checkout_layout);
        pastryLayout = (LinearLayout) findViewById(R.id.pastry_layout);
        cakeLayout = (LinearLayout) findViewById(R.id.cake_layout);
        breadLayout = (LinearLayout) findViewById(R.id.bread_layout);
        sandwichLayout = (LinearLayout) findViewById(R.id.sandwich_layout);
        saladsLayout = (LinearLayout) findViewById(R.id.salads_layout);
        macaronsLayout = (LinearLayout) findViewById(R.id.macarons_layout);
        hotmealsLayout = (LinearLayout) findViewById(R.id.hotmeals_layout);
//        collectionsLayout = (LinearLayout) findViewById(R.id.collections_layout);
        cateringLayout = (LinearLayout) findViewById(R.id.catering_layout);

        pastryCntLayout = (RelativeLayout) findViewById(R.id.pastry_cnt_layout);
        cakeCntLayout = (RelativeLayout) findViewById(R.id.cake_cnt_layout);
        breadCntLayout = (RelativeLayout) findViewById(R.id.bread_cnt_layout);
        sandwichCntLayout = (RelativeLayout) findViewById(R.id.sandwich_cnt_layout);
        saladCntLayout = (RelativeLayout) findViewById(R.id.salad_cnt_layout);
        macaronCntLayout = (RelativeLayout) findViewById(R.id.macaron_cnt_layout);
        mealsCntLayout = (RelativeLayout) findViewById(R.id.meals_cnt_layout);
//        collcetionCntLayout = (RelativeLayout) findViewById(R.id.collections_cnt_layout);
        cateringCntLayout = (RelativeLayout) findViewById(R.id.catering_cnt_layout);

        pastryCnt = (TextView) findViewById(R.id.pastry_count);
        cakeCnt = (TextView) findViewById(R.id.cake_count);
        breadCnt = (TextView) findViewById(R.id.bread_count);
        sandwichCnt = (TextView) findViewById(R.id.sandwich_count);
        saladCnt = (TextView) findViewById(R.id.salad_count);
        macaronCnt = (TextView) findViewById(R.id.macaron_count);
        mealsCnt = (TextView) findViewById(R.id.meals_count);
//        collcetionsCnt = (TextView) findViewById(R.id.collections_count);
        cateringCnt = (TextView) findViewById(R.id.catering_count);

//        setQty();
        mcount_basket= (TextView) findViewById(R.id.total_count);

        mcount= (RelativeLayout) findViewById(R.id.count);

        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderFragment.this, android.R.style.Theme_Material_Light_Dialog));

                    if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("There is no items in your order? To proceed checkout please add the items")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Intent checkoutIntent = new Intent(OrderFragment.this, CheckoutActivity.class);
                    startActivity(checkoutIntent);
                }
            }
        });

        new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL);

        pastryLayout.setOnClickListener(this);
        cakeLayout.setOnClickListener(this);
        breadLayout.setOnClickListener(this);
        sandwichLayout.setOnClickListener(this);
        saladsLayout.setOnClickListener(this);
        macaronsLayout.setOnClickListener(this);
        hotmealsLayout.setOnClickListener(this);
//        collectionsLayout.setOnClickListener(this);
        cateringLayout.setOnClickListener(this);

        double number;
        number=myDbHelper.getTotalOrderPrice();
        DecimalFormat decim = new DecimalFormat("0.00");
        orderPrice.setText("" + decim.format(number) + " SR");
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
    }


    private void setQty(){
        if(myDbHelper.getCategoryQty("1") == 0){
            pastryCntLayout.setVisibility(View.GONE);
        }else {
            pastryCntLayout.setVisibility(View.VISIBLE);
            pastryCnt.setText(""+myDbHelper.getCategoryQty("1"));
        }

        if(myDbHelper.getCategoryQty("2") == 0){
            cakeCntLayout.setVisibility(View.GONE);
        }else {
            cakeCntLayout.setVisibility(View.VISIBLE);
            cakeCnt.setText(""+myDbHelper.getCategoryQty("2"));
        }

        if(myDbHelper.getCategoryQty("6") == 0){
            breadCntLayout.setVisibility(View.GONE);
        }else {
            breadCntLayout.setVisibility(View.VISIBLE);
            breadCnt.setText(""+myDbHelper.getCategoryQty("6"));
        }

        if(myDbHelper.getCategoryQty("3") == 0){
            sandwichCntLayout.setVisibility(View.GONE);
        }else {
            sandwichCntLayout.setVisibility(View.VISIBLE);
            sandwichCnt.setText(""+myDbHelper.getCategoryQty("3"));
        }

        if(myDbHelper.getCategoryQty("14") == 0){
            saladCntLayout.setVisibility(View.GONE);
        }else {
            saladCntLayout.setVisibility(View.VISIBLE);
            saladCnt.setText(""+myDbHelper.getCategoryQty("14"));
        }

        if(myDbHelper.getCategoryQty("4") == 0){
            macaronCntLayout.setVisibility(View.GONE);
        }else {
            macaronCntLayout.setVisibility(View.VISIBLE);
            macaronCnt.setText(""+myDbHelper.getCategoryQty("4"));
        }

        if(myDbHelper.getCategoryQty("13") == 0){
            mealsCntLayout.setVisibility(View.GONE);
        }else {
            mealsCntLayout.setVisibility(View.VISIBLE);
            mealsCnt.setText(""+myDbHelper.getCategoryQty("13"));
        }

//        if(myDbHelper.getCategoryQty("10") == 0){
//            collcetionCntLayout.setVisibility(View.GONE);
//        }else {
//            collcetionCntLayout.setVisibility(View.VISIBLE);
//            collcetionsCnt.setText(""+myDbHelper.getCategoryQty("10"));
//        }

        if(myDbHelper.getCategoryQty("8") == 0){
            cateringCntLayout.setVisibility(View.GONE);
        }else {
            cateringCntLayout.setVisibility(View.VISIBLE);
            cateringCnt.setText(""+myDbHelper.getCategoryQty("8"));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            mainCatList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderFragment.this);
            dialog = ProgressDialog.show(OrderFragment.this, "",
                    "Loading items...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderFragment.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(OrderFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            JSONArray ja = jo.getJSONArray("Success");

                            for (int i=0;i<ja.length();i++){

                                MainCategories maincat =new MainCategories();
                                ArrayList<SubCategories> subcatList = new ArrayList<>();

                                JSONObject jo1 = ja.getJSONObject(i);

                                maincat.setMaincatid(jo1.getString("MainCategoryId"));
                                maincat.setMaincatname(jo1.getString("MainCategoryName"));
                                maincat.setMaincatname_ar(jo1.getString("MainCategoryName_Ar"));
                                maincat.setMaincatdesc(jo1.getString("MainCategoryDesc"));
                                maincat.setMaincatdesc_ar(jo1.getString("MainCategoryDesc_Ar"));
                                maincat.setMainimg(jo1.getString("Simg"));

                                JSONArray ja1 =jo1.getJSONArray("SubCategoryItems");

                                for (int k=0;k<ja1.length();k++){

                                    SubCategories subcat = new SubCategories();
                                    ArrayList<Section> sectionList = new ArrayList<>();

                                    JSONObject jo2 = ja1.getJSONObject(k);
                                    subcat.setCatId(jo2.getString("SubCategoryId"));
                                    subcat.setSubCat(jo2.getString("SubCategoryName"));
                                    subcat.setSubCatAr(jo2.getString("SubCategoryName_Ar"));
                                    subcat.setDescription(jo2.getString("SubCategoryDesc"));
                                    subcat.setDescriptionAr(jo2.getString("SubCategoryDesc_Ar"));
                                    subcat.setImages(jo2.getString("SubCategoryImage"));
                                    subcat.setSubCatSeq(jo2.getString("SubCategorySeq"));
                                    subcat.setMaincatid(jo2.getString("MainCategoryId"));


                                    JSONArray ja2 = jo2.getJSONArray("Sections");

                                    for (int l=0;l<ja2.length();l++){

                                        Section section =new Section();
                                        ArrayList<Items> itemlist =new ArrayList<>();

                                        JSONObject jo3 = ja2.getJSONObject(l);
                                        section.setSectionid(jo3.getString("SectionId"));
                                        section.setSectionname(jo3.getString("SectionName"));
                                        section.setSectionsubcat(jo3.getString("SectionSubCategory"));

                                        JSONArray ja3 = jo3.getJSONArray("Items");

                                        for (int m=0;m<ja3.length();m++){

                                            Items item =new Items();
                                            ArrayList<Price> priceList = new ArrayList<>();
                                            ArrayList<SubItems> subitemsList =new ArrayList<>();

                                            JSONObject jo4 = ja3.getJSONObject(m);
                                            item.setItemId(jo4.getString("ItemId"));
                                            item.setSubCatId(jo4.getString("SubCategoryId"));
                                            item.setSectionid(jo4.getString("SectionId"));
                                            item.setItemName(jo4.getString("ItemName"));
                                            item.setItemNameAr(jo4.getString("ItemName_Ar"));
                                            item.setDescription(jo4.getString("ItemDesc"));
                                            item.setDescriptionAr(jo4.getString("ItemDesc_Ar"));
                                            item.setImage(jo4.getString("ItemImage"));
                                            item.setItemseq(jo4.getString("ItemSequence"));
                                            item.setBuildingType(jo4.getString("BindingType"));
                                            item.setIsdeliver(jo4.getString("IsDeliver"));
                                            item.setNo_of_items(jo4.getString("NoOfItems"));
                                            item.setDisplayname(jo4.getString("DisplayName"));

                                            JSONArray ja4 = jo4.getJSONArray("Price");

                                            for (int n=0;n<ja4.length();n++){
                                                Price price = new Price();

                                                JSONObject jo5 = ja4.getJSONObject(n);
                                                price.setPrice(jo5.getString("price"));
                                                price.setSizeid(jo5.getString("sizeId"));
                                                price.setSize(jo5.getString("size"));
                                                price.setPriceId(jo5.getString("priceId"));

                                                priceList.add(price);
                                            }

                                            JSONArray ja5 = jo4.getJSONArray("SubItems");

                                            try {
                                                for (int o=0;o<ja5.length();o++){
                                                    SubItems subItems =new SubItems();

                                                    JSONObject jo6 = ja5.getJSONObject(o);
                                                    subItems.setItemid(jo6.getString("ItemId"));
                                                    subItems.setSubitemid(jo6.getString("SubItemId"));
                                                    subItems.setSubitemName(jo6.getString("SubItemName"));
                                                    subItems.setSubItemName_Ar(jo6.getString("SubItemName_Ar"));
                                                    subItems.setSubItemimage(jo6.getString("SubItemImage"));
                                                    subItems.setSubItemseq(jo6.getString("SubItemSequence"));
                                                    subItems.setSubItemIsdeliver(jo6.getString("SubItemIsDeliver"));

                                                    subitemsList.add(subItems);

                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            item.setSubItems(subitemsList);
                                            item.setPriceList(priceList);
                                            itemlist.add(item);
                                        }
                                        section.setChildItems(itemlist);
                                        sectionList.add(section);
                                    }
                                    subcat.setSections(sectionList);
                                    subcatList.add(subcat);

                                }
                                maincat.setSubCategories(subcatList);
                                mainCatList.add(maincat);
                                Log.e("TAG" , "maincat " +mainCatList.size());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else {
                Toast.makeText(OrderFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
//            mAdapter.notifyDataSetChanged();

//            if(subCatList.size()>0) {
//                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, subCatList, language);
//                mGridView.setAdapter(mSubMenuAdapter);
//                mSubMenuAdapter.notifyDataSetChanged();
//            }
//            int count =  mAdapter.getGroupCount();
//            for (int i = 0; i <count ; i++) {
//                mExpListView.collapseGroup(i);
//            }
            super.onPostExecute(result);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pastry_layout:
                Intent pastryIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
                pastryIntent.putExtra("catId", 1);
                pastryIntent.putExtra("mainList", mainCatList);
                Log.e("TAG","size "+mainCatList.size());
                startActivity(pastryIntent);
                break;
            case R.id.cake_layout:
                Intent cakeIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
                cakeIntent.putExtra("catId", 2);
                cakeIntent.putExtra("mainList", mainCatList);
                startActivity(cakeIntent);
                break;
            case R.id.bread_layout:
                Intent breadIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
                breadIntent.putExtra("catId", 6);
                breadIntent.putExtra("mainList", mainCatList);
                startActivity(breadIntent);
                break;
            case R.id.sandwich_layout:
                Intent sandwichIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
                sandwichIntent.putExtra("catId", 3);
                sandwichIntent.putExtra("mainList", mainCatList);
                startActivity(sandwichIntent);
                break;
//            case R.id.salads_layout:
//                Intent saladsIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
//                saladsIntent.putExtra("catId", 14);
//                saladsIntent.putExtra("mainList", mainCatList);
//                startActivity(saladsIntent);
//                break;
            case R.id.macarons_layout:
                Intent macaronsIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
                macaronsIntent.putExtra("catId", 4);
                macaronsIntent.putExtra("mainList", mainCatList);
                startActivity(macaronsIntent);
                break;
            case R.id.hotmeals_layout:
                Intent hotmealsIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
                hotmealsIntent.putExtra("catId", 13);
                hotmealsIntent.putExtra("mainList", mainCatList);
                startActivity(hotmealsIntent);
                break;
//            case R.id.collections_layout:
//                Intent collectionsIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
//                collectionsIntent.putExtra("catId", 10);
//                startActivity(collectionsIntent);
//                break;
//            case R.id.catering_layout:
//                Intent cateringIntent = new Intent(OrderFragment.this, CategoriesListActivity.class);
//                cateringIntent.putExtra("catId", 8);
//                cateringIntent.putExtra("mainList", mainCatList);
//                startActivity(cateringIntent);
//                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
        orderPrice.setText("" +decimalFormat.format(myDbHelper.getTotalOrderPrice())+" SR");
        orderQuantity.setText(""+myDbHelper.getTotalOrderQty());


        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderFragment.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
//                     set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

//                     set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderFragment.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have "+ myDbHelper.getTotalOrderQty() +" item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent a=new Intent(OrderFragment.this,CheckoutActivity.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        myDbHelper.deleteOrderTable();
//                                    CheckoutActivity.orderQuantity.setText("0");
//                                    CheckoutActivity.orderPrice.setText("0SR");
//                                    CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("0");
//                                        CategoriesListActivity.orderPrice.setText("0.00SR");
//                                        CategoriesListActivity.mcount_basket.setText("0");
                                        OrderFragment.orderQuantity.setText("0");
                                        OrderFragment.orderPrice.setText("0.00SR");
                                        OrderFragment.mcount_basket.setText("0");
                                        setQty();
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات"+myDbHelper.getTotalOrderQty()+ "لديك " )
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        myDbHelper.deleteOrderTable();
//                                    CheckoutActivity.orderQuantity.setText("0");
//                                    CheckoutActivity.orderPrice.setText("0SR");
//                                    CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("0");
//                                        CategoriesListActivity.orderPrice.setText("0.00SR");
//                                        CategoriesListActivity.mcount_basket.setText("0");
                                        OrderFragment.orderQuantity.setText("0");
                                        OrderFragment.orderPrice.setText("0.00SR");
                                        OrderFragment.mcount_basket.setText("0");
                                        setQty();
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent a=new Intent(OrderFragment.this,CheckoutActivity.class);
                                        startActivity(a);
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }
        });

        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

        setQty();
    }

    @Override
    public void onBackPressed() {
        if(myDbHelper.getTotalOrderQty()>0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderFragment.this, android.R.style.Theme_Material_Light_Dialog));
            String title= "", msg= "", posBtn = "", negBtn = "";
//            if(language.equalsIgnoreCase("En")){
//                posBtn = "Yes";
//                negBtn = "No";
//                title = "dr.CAFE";
                msg = "You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear. Do you want to continue?";
//            }else if(language.equalsIgnoreCase("Ar")){
//                posBtn = "نعم";
//                negBtn = "لا";
//                title = "د. كيف";
//                msg = "لديك " + myDbHelper.getTotalOrderQty()+ "  منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات . هل تود الاستمرار";
//            }
            // set title
            alertDialogBuilder.setTitle(title);

            // set dialog message
            alertDialogBuilder
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            onBackPressed();
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();



        }else {
            super.onBackPressed();
        }
    }
}
