package com.cs.bakeryco.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.AboutUs;
import com.cs.bakeryco.activities.AddressActivity;
import com.cs.bakeryco.activities.LoginActivity;
import com.cs.bakeryco.activities.MoreWebView;
import com.cs.bakeryco.activities.MyProfileActivity;

import java.util.List;

/**
 * Created by CS on 14-09-2016.
 */
public class MoreFragment extends Fragment implements View.OnClickListener {

    private static final int PROFILE_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;
    String mLoginStatus;
    CardView myProfile, manageAddress;
    TextView langEnglish, langArabic;
    CardView aboutUs, contactUs;
    LinearLayout moreFb, moreTwitter, moreYoutube, moreKeek, moreInstagram, moreRate;
    SharedPreferences userPrefs;
    SharedPreferences.Editor  userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.moe_fragment, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.moe_fragment_arabic, container,
                    false);
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        myProfile = (CardView) rootView.findViewById(R.id.more_profile);
        manageAddress = (CardView) rootView.findViewById(R.id.more_address);
        moreFb = (LinearLayout) rootView.findViewById(R.id.more_fb);
        moreTwitter = (LinearLayout) rootView.findViewById(R.id.more_twitter);
        moreYoutube = (LinearLayout) rootView.findViewById(R.id.more_youtube);
        moreInstagram = (LinearLayout) rootView.findViewById(R.id.more_instagram);
//        moreKeek = (LinearLayout) rootView.findViewById(R.id.more_keek);
        moreRate = (LinearLayout) rootView.findViewById(R.id.more_rate);
        aboutUs = (CardView) rootView.findViewById(R.id.more_aboutus);
        contactUs = (CardView) rootView.findViewById(R.id.more_contactus);
        langEnglish = (TextView) rootView.findViewById(R.id.lang_english);
        langArabic = (TextView) rootView.findViewById(R.id.lang_arabic);


        myProfile.setOnClickListener(this);
        manageAddress.setOnClickListener(this);
        moreFb.setOnClickListener(this);
        moreTwitter.setOnClickListener(this);
        moreYoutube.setOnClickListener(this);
        moreInstagram.setOnClickListener(this);
//        moreKeek.setOnClickListener(this);
        moreRate.setOnClickListener(this);
        aboutUs.setOnClickListener(this);
        contactUs.setOnClickListener(this);
        langEnglish.setOnClickListener(this);
        langArabic.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.more_profile:
                mLoginStatus = userPrefs.getString("login_status", "");
                if(mLoginStatus.equalsIgnoreCase("loggedin")){
                    Intent intent = new Intent(getActivity(), MyProfileActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, PROFILE_REQUEST);
                }
                break;

            case R.id.more_address:
                mLoginStatus = userPrefs.getString("login_status", "");
                if(mLoginStatus.equalsIgnoreCase("loggedin")){
                    Intent i = new Intent(getActivity(), AddressActivity.class);
                    startActivity(i);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, ADDRESS_REQUEST);
                }

                break;

            case R.id.more_fb:
                Intent fbIntent = new Intent(getActivity(), MoreWebView.class);
                fbIntent.putExtra("title", "Facebook");
                fbIntent.putExtra("url","https://www.facebook.com/bakeryand.coksa");
                startActivity(fbIntent);
                break;

            case R.id.more_twitter:
                Intent twitterIntent = new Intent(getActivity(), MoreWebView.class);
                twitterIntent.putExtra("title", "Twitter");
                twitterIntent.putExtra("url","https://twitter.com/bakeryandcoksa");
                startActivity(twitterIntent);
                break;

//            case R.id.more_keek:
//                Intent keekIntent = new Intent(getActivity(), MoreWebView.class);
//                keekIntent.putExtra("title", "KeeK");
//                keekIntent.putExtra("url","https://www.keek.com/profile/bakeryandcoksa");
//                startActivity(keekIntent);
//                break;

            case R.id.more_youtube:
                Intent youtubeIntent = new Intent(getActivity(), MoreWebView.class);
                youtubeIntent.putExtra("title", "Youtube");
                youtubeIntent.putExtra("url","http://www.youtube.com/user/bakeryandcoksa");
                startActivity(youtubeIntent);
                break;

            case R.id.more_instagram:
                Intent instagramIntent = new Intent(getActivity(), MoreWebView.class);
                instagramIntent.putExtra("title", "Instagram");
                instagramIntent.putExtra("url","https://www.instagram.com/bakery.coksa/");
                startActivity(instagramIntent);
                break;

            case R.id.more_rate:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.bakeryco")));
                break;

            case R.id.more_aboutus:
                Intent intent1 = new Intent(getActivity(), AboutUs.class);
                startActivity(intent1);
                break;
            case R.id.more_contactus:
//                String email = "info@bakery-co.com";
//                Intent intent = new Intent(Intent.ACTION_SEND);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.setType("plain/text");
//                intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
//                intent.putExtra(Intent.EXTRA_SUBJECT, "");
////                intent.putExtra(Intent.EXTRA_TITLE, "Bakery & Company Experiance");
//                intent.putExtra(Intent.EXTRA_EMAIL,new String[] {email});
//                getActivity().startActivity(intent);
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("plain/text");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@bakery-co.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Bakery & Company Experience");
                i.putExtra(Intent.EXTRA_TITLE  , "Bakery & Company Experience");
                final PackageManager pm = getActivity().getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                String className = null;
                for (final ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                        className = info.activityInfo.name;

                        if(className != null && !className.isEmpty()){
                            break;
                        }
                    }
                }
                i.setClassName("com.google.android.gm", className);
                try {
                    getActivity().startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lang_english:
                languagePrefsEditor.putString("language","En");
                languagePrefsEditor.commit();
//                langEnglish.setBackgroundColor(Color.parseColor("#40815F"));
//                langArabic.setBackgroundColor(Color.parseColor("#00000000"));
//                langEnglish.setTextColor(Color.parseColor("#FFFFFF"));
//                langArabic.setTextColor(Color.parseColor("#000000"));
                getActivity().recreate();
                break;
            case R.id.lang_arabic:
                languagePrefsEditor.putString("language","Ar");
                languagePrefsEditor.commit();
//                langArabic.setBackgroundColor(Color.parseColor("#40815F"));
//                langEnglish.setBackgroundColor(Color.parseColor("#00000000"));
//                langArabic.setTextColor(Color.parseColor("#FFFFFF"));
//                langEnglish.setTextColor(Color.parseColor("#000000"));
                getActivity().recreate();
                break;
        }
    }
}
