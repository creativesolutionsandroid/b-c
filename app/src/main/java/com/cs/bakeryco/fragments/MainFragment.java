package com.cs.bakeryco.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.AboutUs;
import com.cs.bakeryco.activities.CategoriesListActivity;
import com.cs.bakeryco.activities.CorporateOrder;
import com.cs.bakeryco.activities.LoginActivity;
import com.cs.bakeryco.activities.TrackOrderActivity;
import com.cs.bakeryco.model.Order;

import java.util.ArrayList;

/**
 * Created by CS on 08-09-2016.
 */
public class MainFragment extends Fragment {
    private static final int TRACK_ORDER_REQUEST = 1;
    LinearLayout orderLayout, trackOrderLayout, cateringLayout, aboutusLayout, corporateLayout;
    SharedPreferences userPrefs;
    String mLoginStatus;
    RelativeLayout morder_cnt;
    TextView morder_count;
    DataBaseHelper myDbHelper;
    ListView xyz;
    ArrayList<Order> orderList=new ArrayList<>();
    int position;
    String language;
    SharedPreferences languagePrefs;
    View rootView;
    ImageView mbread;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
             rootView = inflater.inflate(R.layout.main_fragment, container,
                    false);
        }else if (language.equalsIgnoreCase("Ar")){
             rootView = inflater.inflate(R.layout.main_fragment_arabic, container,
                    false);
        }
        orderLayout = (LinearLayout) rootView.findViewById(R.id.order_layout);
        trackOrderLayout = (LinearLayout) rootView.findViewById(R.id.track_order_layout);
        cateringLayout = (LinearLayout) rootView.findViewById(R.id.catering_layout);
        aboutusLayout = (LinearLayout) rootView.findViewById(R.id.aboutus_layout);
        corporateLayout = (LinearLayout) rootView.findViewById(R.id.corporate_layout);
        mbread= (ImageView) rootView.findViewById(R.id.bread);
//        xyz= (ListView) rootView.findViewById(R.id.xyz);

        morder_cnt= (RelativeLayout) rootView.findViewById(R.id.order_cnt);

        morder_count= (TextView) rootView.findViewById(R.id.order_count);

        myDbHelper=new DataBaseHelper(getActivity());

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        orderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myDbHelper.getTotalOrderQty()==0) {
                    ((MainActivity) getActivity()).selectItem(1);
                }else {
                    Intent intent=new Intent(getActivity(),OrderFragment.class);
                    startActivity(intent);
                }
            }
        });

        mbread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cateringIntent = new Intent(getActivity(), CategoriesListActivity.class);
                cateringIntent.putExtra("catId", 6);
                startActivity(cateringIntent);
            }
        });

        trackOrderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {

                    Intent i = new Intent(getActivity(), TrackOrderActivity.class);
                    i.putExtra("orderId","-1");
                    i.putExtra("total_amt", "0");
                    i.putExtra("total_items", "0");
                    i.putExtra("expected_time", "0");
                    i.putExtra("payment_mode", "0");
                    i.putExtra("order_type", "0");
                    i.putExtra("order_number", "0");
                    startActivity(i);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, TRACK_ORDER_REQUEST);
                }

            }
        });

        cateringLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cateringIntent = new Intent(getActivity(), CategoriesListActivity.class);
                cateringIntent.putExtra("catId", 8);
                startActivity(cateringIntent);
            }
        });

        aboutusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cateringIntent = new Intent(getActivity(), AboutUs.class);
                startActivity(cateringIntent);
            }
        });

        corporateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent corporateIntent = new Intent(getActivity(), CorporateOrder.class);
                startActivity(corporateIntent);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLoginStatus = userPrefs.getString("login_status", "");
        if(requestCode == TRACK_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent i = new Intent(getActivity(), TrackOrderActivity.class);
            i.putExtra("orderId","-1");
            startActivity(i);
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myDbHelper.getTotalOrderQty()==0){
            morder_cnt.setVisibility(View.GONE);
        }else {
            morder_cnt.setVisibility(View.VISIBLE);
            morder_count.setText(""+myDbHelper.getTotalOrderQty());
        }
    }
}
