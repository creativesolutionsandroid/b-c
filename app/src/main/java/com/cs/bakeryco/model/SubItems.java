package com.cs.bakeryco.model;

import java.io.Serializable;

/**
 * Created by CS on 1/18/2018.
 */

public class SubItems implements Serializable {

    String itemid, subitemid, subitemName, subItemName_Ar, subItemimage, subItemseq, subItemIsdeliver;

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getSubitemid() {
        return subitemid;
    }

    public void setSubitemid(String subitemid) {
        this.subitemid = subitemid;
    }

    public String getSubitemName() {
        return subitemName;
    }

    public void setSubitemName(String subitemName) {
        this.subitemName = subitemName;
    }

    public String getSubItemName_Ar() {
        return subItemName_Ar;
    }

    public void setSubItemName_Ar(String subItemName_Ar) {
        this.subItemName_Ar = subItemName_Ar;
    }

    public String getSubItemimage() {
        return subItemimage;
    }

    public void setSubItemimage(String subItemimage) {
        this.subItemimage = subItemimage;
    }

    public String getSubItemseq() {
        return subItemseq;
    }

    public void setSubItemseq(String subItemseq) {
        this.subItemseq = subItemseq;
    }

    public String getSubItemIsdeliver() {
        return subItemIsdeliver;
    }

    public void setSubItemIsdeliver(String subItemIsdeliver) {
        this.subItemIsdeliver = subItemIsdeliver;
    }
}
