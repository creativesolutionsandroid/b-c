package com.cs.bakeryco.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class RatingDetails implements Serializable {

    String givenrating;
    ArrayList<Rating> arrayList;

    public String getGivenrating() {
        return givenrating;
    }

    public void setGivenrating(String givenrating) {
        this.givenrating = givenrating;
    }

    public ArrayList<Rating> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Rating> arrayList) {
        this.arrayList = arrayList;
    }
}
