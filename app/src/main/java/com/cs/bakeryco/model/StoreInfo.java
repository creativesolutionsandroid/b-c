package com.cs.bakeryco.model;

import java.util.Comparator;

/**
 * Created by CS on 7/13/2017.
 */

public class StoreInfo {

    String storeId, onlineOrderStatus, starttime, endtime, storeName, storeAddress, countryName, cityName, imageURL, familySection, wifi,
            patioSitting, driveThru, meetingSpace, hospital, university, office, shoppingMall, airport, dineIn, ladies, neighborhood, is24x7,
            status, ogCountry, message, message_ar, storeNumber, ogCity, storeName_ar, storeAddress_ar;
    Double latitude, longitude;
    int openFlag;
    float distance;


    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getOnlineOrderStatus() {
        return onlineOrderStatus;
    }

    public void setOnlineOrderStatus(String onlineOrderStatus) {
        this.onlineOrderStatus = onlineOrderStatus;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getFamilySection() {
        return familySection;
    }

    public void setFamilySection(String familySection) {
        this.familySection = familySection;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public String getPatioSitting() {
        return patioSitting;
    }

    public void setPatioSitting(String patioSitting) {
        this.patioSitting = patioSitting;
    }

    public String getDriveThru() {
        return driveThru;
    }

    public void setDriveThru(String driveThru) {
        this.driveThru = driveThru;
    }

    public String getMeetingSpace() {
        return meetingSpace;
    }

    public void setMeetingSpace(String meetingSpace) {
        this.meetingSpace = meetingSpace;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getShoppingMall() {
        return shoppingMall;
    }

    public void setShoppingMall(String shoppingMall) {
        this.shoppingMall = shoppingMall;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getDineIn() {
        return dineIn;
    }

    public void setDineIn(String dineIn) {
        this.dineIn = dineIn;
    }

    public String getLadies() {
        return ladies;
    }

    public void setLadies(String ladies) {
        this.ladies = ladies;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getIs24x7() {
        return is24x7;
    }

    public void setIs24x7(String is24x7) {
        this.is24x7 = is24x7;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOgCountry() {
        return ogCountry;
    }

    public void setOgCountry(String ogCountry) {
        this.ogCountry = ogCountry;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage_ar() {
        return message_ar;
    }

    public void setMessage_ar(String message_ar) {
        this.message_ar = message_ar;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getOgCity() {
        return ogCity;
    }

    public void setOgCity(String ogCity) {
        this.ogCity = ogCity;
    }

    public String getStoreName_ar() {
        return storeName_ar;
    }

    public void setStoreName_ar(String storeName_ar) {
        this.storeName_ar = storeName_ar;
    }

    public String getStoreAddress_ar() {
        return storeAddress_ar;
    }

    public void setStoreAddress_ar(String storeAddress_ar) {
        this.storeAddress_ar = storeAddress_ar;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getOpenFlag() {
        return openFlag;
    }

    public void setOpenFlag(int openFlag) {
        this.openFlag = openFlag;
    }

    /*Comparator for sorting the list by roll no*/
    public static Comparator<StoreInfo> storeDistance = new Comparator<StoreInfo>() {

        public int compare(StoreInfo s1, StoreInfo s2) {

            float rollno1 = s1.getDistance();
            float rollno2 = s2.getDistance();

	   /*For ascending order*/
            return Float.compare(rollno1, rollno2);

	   /*For descending order*/
            //rollno2-rollno1;
        }
    };


    /*Comparator for sorting the list by roll no*/
    public static Comparator<StoreInfo> storeOpenSort = new Comparator<StoreInfo>() {

        public int compare(StoreInfo s1, StoreInfo s2) {

            int rollno1 = s1.getOpenFlag();
            int rollno2 = s2.getOpenFlag();

	   /*For ascending order*/
            return rollno2 - rollno1;

	   /*For descending order*/
            //rollno2-rollno1;
        }
    };

}
