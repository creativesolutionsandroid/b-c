package com.cs.bakeryco.model;

/**
 * Created by SKT on 19-02-2016.
 */
public class OrderHistory {

    String orderId, storeId, orderDate, storeName, storeName_ar, userAddress, totalPrice, status, orderStatus, invoiceNo, orderType, itemDetails, itemDetails_ar;
    boolean isFavorite;


    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreName_ar() {
        return storeName_ar;
    }

    public void setStoreName_ar(String storeName_ar) {
        this.storeName_ar = storeName_ar;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String total_Price) {
        this.totalPrice = total_Price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(String itemDetails) {
        this.itemDetails = itemDetails;
    }

    public String getItemDetails_ar() {
        return itemDetails_ar;
    }

    public void setItemDetails_ar(String itemDetails_ar) {
        this.itemDetails_ar = itemDetails_ar;
    }
}
