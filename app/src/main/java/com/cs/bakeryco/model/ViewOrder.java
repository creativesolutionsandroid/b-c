package com.cs.bakeryco.model;

/**
 * Created by CS on 7/10/2017.
 */

public class ViewOrder {

    String itemId, itemName, itemNameAr, itemImage, itemDesc, itemDescAr, itemType, quantity, price, categoryId, ids, additionalsStr, additionalsStrAr, additionalsPrice;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNameAr() {
        return itemNameAr;
    }

    public void setItemNameAr(String itemNameAr) {
        this.itemNameAr = itemNameAr;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemDescAr() {
        return itemDescAr;
    }

    public void setItemDescAr(String itemDescAr) {
        this.itemDescAr = itemDescAr;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getAdditionalsStr() {
        return additionalsStr;
    }

    public void setAdditionalsStr(String additionalsStr) {
        this.additionalsStr = additionalsStr;
    }

    public String getAdditionalsStrAr() {
        return additionalsStrAr;
    }

    public void setAdditionalsStrAr(String additionalsStrAr) {
        this.additionalsStrAr = additionalsStrAr;
    }

    public String getAdditionalsPrice() {
        return additionalsPrice;
    }

    public void setAdditionalsPrice(String additionalsPrice) {
        this.additionalsPrice = additionalsPrice;
    }
}

