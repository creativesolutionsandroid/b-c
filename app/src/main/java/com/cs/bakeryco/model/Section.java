package com.cs.bakeryco.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by CS on 1/18/2018.
 */

public class Section implements Serializable {

    String sectionid, sectionname, sectionsubcat;
    ArrayList<Items> childItems;

    public String getSectionid() {
        return sectionid;
    }

    public void setSectionid(String sectionid) {
        this.sectionid = sectionid;
    }

    public String getSectionname() {
        return sectionname;
    }

    public void setSectionname(String sectionname) {
        this.sectionname = sectionname;
    }

    public String getSectionsubcat() {
        return sectionsubcat;
    }

    public void setSectionsubcat(String sectionsubcat) {
        this.sectionsubcat = sectionsubcat;
    }

    public ArrayList<Items> getChildItems() {
        return childItems;
    }

    public void setChildItems(ArrayList<Items> childItems) {
        this.childItems = childItems;
    }
}
