package com.cs.bakeryco.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by CS on 1/18/2018.
 */

public class MainCategories implements Serializable {

    String maincatid,maincatname,maincatname_ar,maincatdesc,maincatdesc_ar,mainimg;
    ArrayList<SubCategories> subCategories;

    public String getMaincatid() {
        return maincatid;
    }

    public void setMaincatid(String maincatid) {
        this.maincatid = maincatid;
    }

    public String getMaincatname() {
        return maincatname;
    }

    public void setMaincatname(String maincatname) {
        this.maincatname = maincatname;
    }

    public String getMaincatname_ar() {
        return maincatname_ar;
    }

    public void setMaincatname_ar(String maincatname_ar) {
        this.maincatname_ar = maincatname_ar;
    }

    public String getMaincatdesc() {
        return maincatdesc;
    }

    public void setMaincatdesc(String maincatdesc) {
        this.maincatdesc = maincatdesc;
    }

    public String getMaincatdesc_ar() {
        return maincatdesc_ar;
    }

    public void setMaincatdesc_ar(String maincatdesc_ar) {
        this.maincatdesc_ar = maincatdesc_ar;
    }

    public String getMainimg() {
        return mainimg;
    }

    public void setMainimg(String mainimg) {
        this.mainimg = mainimg;
    }

    public ArrayList<SubCategories> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(ArrayList<SubCategories> subCategories) {
        this.subCategories = subCategories;
    }
}
