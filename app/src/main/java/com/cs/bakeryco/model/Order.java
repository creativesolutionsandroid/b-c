package com.cs.bakeryco.model;

/**
 * Created by SKT on 14-02-2016.
 */
public class Order {
    String orderId, itemId, itemTypeId, subCatId, additionals, qty, price, additionalsPrice, additionalsTypeId, totalAmount, comment, status, creationDate, modifiedDate, categoryId, itemName, itemNameAr, image, item_desc, item_desc_Ar, sub_itemName, sub_itemName_Ar, sub_itemImage ,sub_itemId, sub_itemcount;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(String itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getAdditionals() {
        return additionals;
    }

    public void setAdditionals(String additionals) {
        this.additionals = additionals;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdditionalsPrice() {
        return additionalsPrice;
    }

    public void setAdditionalsPrice(String additionalsPrice) {
        this.additionalsPrice = additionalsPrice;
    }

    public String getAdditionalsTypeId() {
        return additionalsTypeId;
    }

    public void setAdditionalsTypeId(String additionalsTypeId) {
        this.additionalsTypeId = additionalsTypeId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNameAr() {
        return itemNameAr;
    }

    public void setItemNameAr(String itemNameAr) {
        this.itemNameAr = itemNameAr;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getItem_desc() {
        return item_desc;
    }

    public void setItem_desc(String item_desc) {
        this.item_desc = item_desc;
    }

    public String getItem_desc_Ar() {
        return item_desc_Ar;
    }

    public void setItem_desc_Ar(String item_desc_Ar) {
        this.item_desc_Ar = item_desc_Ar;
    }

    public String getSub_itemName() {
        return sub_itemName;
    }

    public void setSub_itemName(String sub_itemName) {
        this.sub_itemName = sub_itemName;
    }

    public String getSub_itemName_Ar() {
        return sub_itemName_Ar;
    }

    public void setSub_itemName_Ar(String sub_itemName_Ar) {
        this.sub_itemName_Ar = sub_itemName_Ar;
    }

    public String getSub_itemImage() {
        return sub_itemImage;
    }

    public void setSub_itemImage(String sub_itemImage) {
        this.sub_itemImage = sub_itemImage;
    }

    public String getSub_itemId() {
        return sub_itemId;
    }

    public void setSub_itemId(String sub_itemId) {
        this.sub_itemId = sub_itemId;
    }

    public String getSub_itemcount() {
        return sub_itemcount;
    }

    public void setSub_itemcount(String sub_itemcount) {
        this.sub_itemcount = sub_itemcount;
    }
}
